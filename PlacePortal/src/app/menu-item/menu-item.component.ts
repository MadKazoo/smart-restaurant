import { Component, OnInit, Input } from '@angular/core';
import { MenuItem } from '../model/menuItem';
import { MenuItemService } from '../services/menuItem.service';

@Component({
  selector: 'app-menu-item',
  templateUrl: './menu-item.component.html',
  styleUrls: ['./menu-item.component.css']
})
export class MenuItemComponent implements OnInit {
  @Input() placeId;
  @Input() menuItems: MenuItem[];

  menuItem: MenuItem;
  constructor(private menuItemService: MenuItemService) {
    this.menuItem = new MenuItem();
   }

  ngOnInit() {
  }

  removeItem(menuItem: MenuItem){
    this.menuItemService.deleteMenuItem(menuItem, this.placeId);
  }
  onSubmit(){
    this.menuItemService.CreateMenuItem(this.menuItem, this.placeId).subscribe(x=>{
      console.log(x);
    window.location.reload();
    });
  }
}

import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { LoginService } from '../services/login.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private loginService: LoginService, private router: Router) {
  }

  canLogIn: boolean;

  canActivate() {
    const token = localStorage.getItem('jwt');
    const login = localStorage.getItem('login');
    const password = localStorage.getItem('password');
    console.log(login + ' ' + password);

    const isLoginValid = this.loginService.isLoginValid;

    console.log('isloginValid');

    console.log(isLoginValid);

    console.log('token:');

    console.log(token);

    if (token && isLoginValid) {
      return true;
    }

    if (login && password) {
      this.loginService.login(login, password);
    }

    if (token && isLoginValid) {
      return true;
    }

    this.router.navigate(['login']);
    return false;
  }
}
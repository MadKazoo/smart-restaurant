import { Component, OnInit } from '@angular/core';
import { PlaceService } from '../services/place.service';
import { PlaceDto } from '../model/order';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { MenuItemService } from '../services/menuItem.service';
import { PlaceContextService } from '../services/place-context.service';

@Component({
  selector: 'app-places',
  templateUrl: './places.component.html',
  styleUrls: ['./places.component.css']
})
export class PlacesComponent implements OnInit {

  closeResult: string;
  isCollapsed = true;
  places: PlaceDto[];
  place: PlaceDto;
  constructor(private placeService: PlaceService, private modalService: NgbModal,
              private menuitemsService: MenuItemService, public placesContext: PlaceContextService) {
    placeService.GetPlaces().subscribe(places => this.placesContext.places = places);
  }

  onPlaceClick(content, place: PlaceDto) {
    this.menuitemsService.GetPlaceMenuItems(place.id).subscribe(x => {
      place.menuItems = x;
      this.place = place;
      this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
    });
  }

  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  removeItem(place) {

  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  ngOnInit() {
  }

}

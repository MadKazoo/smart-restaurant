import { Component, OnInit, Input } from '@angular/core';
import { PlaceOrderHub } from '../services/Orders/place-order-hub';
import { OrdersContextService } from '../services/Orders/orders-context';
import { ActivatedRoute } from '@angular/router';
import { CheckInService } from '../services/Orders/check-in.service';
import { CheckInContextService } from '../services/Orders/check-in-context.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {

  placeId: number;
  constructor(private placesOrderHub: PlaceOrderHub, private checkInService: CheckInService
    , private ordersContext: OrdersContextService, private activatedroute: ActivatedRoute,
    public checkInsContext: CheckInContextService) {

    this.placeId = Number.parseInt(activatedroute.snapshot.paramMap.get('placeId'), 0);

    console.log(this.placeId);

    this.checkInService.getCurrentlyProcessedCheckIns(this.placeId);
    if (this.placeId !== undefined && this.placeId !== 0) {
      placesOrderHub.listenForOrders(this.placeId);
    }
  }

  ngOnInit() {
  }

}

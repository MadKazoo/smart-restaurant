import { Component, OnInit, Input } from '@angular/core';
import { PlaceDto, PlaceTag } from '../model/order';
import { GeoCoordinates } from '../model/location';

import { } from 'googlemaps';
import { MouseEvent as AGMMouseEvent } from '@agm/core';
import { PlaceService } from '../services/place.service';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { GeoLocationService } from '../services/geo-location.service';
import { TagsService } from '../services/tags.service';
import { PlaceContextService } from '../services/place-context.service';


@Component({
  selector: 'app-add-place-form',
  templateUrl: './add-place-form.component.html',
  styleUrls: ['./add-place-form.component.css']
})
export class AddPlaceFormComponent implements OnInit {
  // google maps zoom level
  @Input() place: PlaceDto;
  zoom = 8;
  description = '';
  placeTags: PlaceTag[];
  newPlace: PlaceDto = new PlaceDto();

  isMapHidden = true;
  // initial center position for the map
  lat = 1;
  lng = 1;
  marker: Marker;
  markers: Marker[] = [];
  form: FormGroup;
  closeResult: string;
  newTag: string;

  constructor(private placeService: PlaceService,
    private modalService: NgbModal,
    private geoLocationService: GeoLocationService, private placesContext: PlaceContextService,
    private tagsService: TagsService) {
    placeService.GetPlaceTags().subscribe(x => {
      console.log('tags');
      console.log(x);
      this.placeTags = x;
    });
    if (this.place !== undefined && this.place !== null) {
      console.log('place');
      console.log(this.place);
      this.newPlace = Object.assign({}, this.place);
    }
  }
  ngOnInit(): void {
    this.lat = 52.14133764;
    this.lng = 21.131152;
    console.log(location);
    this.newPlace.description = '';
    this.newPlace.name = '';

    if (this.place !== undefined && this.place !== null) {
      console.log('place');
      console.log(this.place);
      this.lat = this.place.coordinates.latitude;
      this.lng = this.place.coordinates.longitude;

      this.marker = ({
        lat: this.lat,
        lng: this.lng,
        draggable: true
      });

      this.newPlace = Object.assign({}, this.place);
    }
  }
  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`);
  }

  onSubmit() {
    const location = new GeoCoordinates();
    location.latitude = this.lat;
    location.longitude = this.lng;
    if (this.placeTags !== undefined && this.placeTags.length > 0) {
      this.newPlace.placeTags = this.placeTags.filter(x => x.checked === true);
    }
    this.newPlace.coordinates = location;

    if (this.place !== undefined && this.place !== null) {
      console.log('updating props');
      this.placeService.updatePlace(this.newPlace).subscribe(place => {
        this.placesContext.place = place;
        this.updatePlacesArray(place);
      });
      return;
    }

    this.placeService.CreatePlace(this.newPlace).subscribe(x =>
      place => {
        this.placesContext.place = place;
        this.updatePlacesArray(place);
      }
    );
  }

  private updatePlacesArray(place: PlaceDto) {
    const index = this.placesContext.places.findIndex(x => x.id === place.id);
    if (index === -1) {
      this.placesContext.places.push(place);
    } else {
      this.placesContext.places[this.placesContext.places.findIndex(x => x.id === place.id)] = place;
    }
  }

  public isChecked(placeTagId): boolean {
    const isChecked = this.place.placeTags.find(x => x.id === placeTagId);
    return (isChecked !== undefined && isChecked !== null) ? true : false;
  }

  private updateProperties() {
    console.log(this.newPlace);

    console.log(this.place);
    const propNames = Object.keys(this.place);
    propNames.forEach(propName => {
      console.log(propName);
      const originalProperty = this.place[propName];
      const changedProperty = this.newPlace[propName];
      console.log(originalProperty);
      console.log(changedProperty);
      if (originalProperty !== changedProperty) {
        console.log('id');
        console.log(this.place.id);
        this.placeService.updateProperty(this.place.id, propName, changedProperty);
      }
    });
  }

  onCheckBoxChanged(tag: PlaceTag) {
    tag.checked = !tag.checked;
    console.log(tag);
  }

  mapClicked($event: AGMMouseEvent) {
    this.marker = ({
      lat: $event.coords.lat,
      lng: $event.coords.lng,
      draggable: true
    });
    this.lat = this.marker.lat;
    this.lng = this.marker.lng;
  }

  markerDragEnd(m: Marker, $event: MouseEvent) {
    console.log('dragEnd', m, $event);
  }

  toggleMap() {
    this.isMapHidden = !this.isMapHidden;
  }

  addTag() {
    if (this.newTag !== undefined && this.newTag !== null) {
      this.tagsService.addTag(this.newTag);
    }
  }
}

interface Marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}


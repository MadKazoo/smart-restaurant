import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { PlacesComponent } from './places/places.component';
import { AuthGuard } from './login/authGuard.service';
import { OrdersComponent } from './orders/orders.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: '', component: PlacesComponent, canActivate: [AuthGuard] },
  { path: 'Orders/:placeId', component: OrdersComponent, canActivate: [AuthGuard] },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

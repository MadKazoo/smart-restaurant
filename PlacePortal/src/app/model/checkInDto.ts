import { PartialOrder } from './order';

export class CheckIn {

    public id: string;
    public checkedInUsers: ClientDto[];
    public tableId: number;

    public placeId: number;
    public partialOrders: PartialOrder[];

}

export class ClientDto {

    public id: number;
    public name: string;
    public isReadyForOrder: boolean;
}

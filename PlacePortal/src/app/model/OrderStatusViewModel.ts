import { OrderStatus, Order } from "./order";

export class OrderStatusViewModel{
    orderId: Number;
    orderStatus: OrderStatus;
}
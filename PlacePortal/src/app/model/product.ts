import { CurrencyPipe } from '@angular/common';

export class Product {
    constructor(public id: string, public name: string,
                public price: CurrencyPipe,
                public imgUrl: string, public available: boolean,
                public quantity: number, public description: string, public category: string
    ) { }
}

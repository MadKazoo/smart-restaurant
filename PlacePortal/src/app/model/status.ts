export enum status {
    New,
    ToPayment,
    Paid,
    ToReceive,
    Receiving,
    Received,
    LackOfProduct,
    Rejected
}

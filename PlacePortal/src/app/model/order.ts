import { MenuItem } from './menuItem';
import { GeoCoordinates } from './location';


export class Order {
        public id: number;
        public groupId: string;
        public partialOrders: PartialOrder[];
        public placeId: number;
        public tableNumber: number;
        public price: number;
        public orderStatus: OrderStatus;
        public dateCreated: Date;
        public placeName: string;
}

export class PartialOrder {
        id: number;
        public menuItem: MenuItem;
        public userPartialOrders: UserPartialOrder[];
}

export class UserPartialOrder {
        public name: string;
        public toPayInMoney: number;
        public clientPercentShare: number;
        public appUserId: number;
        public isClientReady: boolean;
        public clientDto: ClientDto;
}
export class ClientDto {
        public id: number;
        public name: string;
        public imageIcon: string;
        public isReadyForOrder: boolean;
}

export class PlaceDto {
        public id: number;
        public name: string;
        public coordinates: GeoCoordinates;
        public numberOfTables: number;
        public imageUrl: string;
        public description: string;
        public avarageRating: number;
        public placeTags: PlaceTag[];
        public menuItems: MenuItem[];

}
export enum OrderStatus {
        NEW = 0,
        ACCEPTED = 1,
        PAID = 2,
        FINISHED = 3
}

export class PlaceTag {
      public id: number;
      public name: string;

      public checked = false;
}

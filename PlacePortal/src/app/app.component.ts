import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { } from 'googlemaps';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { LoginService } from './services/login.service';


// declare var ol: any;
@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

    closeResult: string;

    constructor(private modalService: NgbModal, private login: LoginService) {
    }

    // open(content) {
    //     this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
    //         this.closeResult = `Closed with: ${result}`;
    //     }, (reason) => {
    //         this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    //     });
    // }

    // private getDismissReason(reason: any): string {
    //     if (reason === ModalDismissReasons.ESC) {
    //         return 'by pressing ESC';
    //     } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
    //         return 'by clicking on a backdrop';
    //     } else {
    //         return `with: ${reason}`;
    //     }
    // }

    ngOnInit(): void {
    }

    // google maps zoom level
    // zoom = 8;

    // // initial center position for the map
    // lat = 51.673858;
    // lng = 7.815982;
    // marker: Marker;
    // markers: Marker[] = [];

    // clickedMarker(label: string, index: number) {
    //     console.log(`clicked the marker: ${label || index}`);
    // }

    // mapClicked($event: AGMMouseEvent) {
    //     this.marker = ({
    //         lat: $event.coords.lat,
    //         lng: $event.coords.lng,
    //         draggable: true
    //     });
    // }

    // markerDragEnd(m: Marker, $event: MouseEvent) {
    //     console.log('dragEnd', m, $event);
    // }
}

// just an interface for type safety.
// interface Marker {
//     lat: number;
//     lng: number;
//     label?: string;
//     draggable: boolean;
// }

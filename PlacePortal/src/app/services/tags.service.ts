import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { PlaceTag } from '../model/order';

@Injectable({
  providedIn: 'root'
})
export class TagsService {

  webServiceUrl: string;

  placesPath = "Place";

  menuPath = "Menu";


  placeTypePath = 'PlaceType';

  constructor(private http: HttpClient) { }

  public addTag(tag: string) {
    var headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Accept', 'application/json');

    return this.http.post<PlaceTag>(this.webServiceUrl + '/' + this.placeTypePath, tag, { headers });
  }
}

import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { MenuItem } from '../model/menuItem';
import { HeaderHelper } from './Helpers/headerHelper.service';


@Injectable({
    providedIn: 'root'
})

export class MenuItemService {

    webServiceUrl: string;

    placesPath = 'Place';

    menuPath = 'Menu';

    placeTypesPath = 'PlaceType';

    constructor(private http: HttpClient, private headersHelper: HeaderHelper) {
        this.webServiceUrl = environment.webServiceUrl;
    }

    public GetPlaceMenuItems(placeId) {
        const headers = this.headersHelper.createHeaderWithJWT();
        return this.http.get<MenuItem[]>(this.webServiceUrl + '/' + this.placesPath + '/' + placeId + '/' + this.menuPath, { headers });
    }

    public CreateMenuItem(menuItem: MenuItem, placeId) {
        const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers.append('Access-Control-Allow-Origin', '*');
        headers.append('Accept', 'application/json');

        const menuItemJson = JSON.stringify(menuItem);

        return this.http.post<MenuItem>(this.webServiceUrl + '/' +
            this.placesPath + '/' + placeId + '/' + this.menuPath, menuItemJson, { headers });

    }

    public deleteMenuItem(menuItem: MenuItem, placeId) {
        const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers.append('Access-Control-Allow-Origin', '*');
        headers.append('Accept', 'application/json');
        const placeObservable = this.http.delete(this.webServiceUrl + '/'
            + this.placesPath + '/' + placeId + '/' + this.menuPath + '/' + menuItem.id, { headers });
        placeObservable.subscribe(x => {
            console.log(x);
        },
            err => console.log(err));
    }

    public DeleteMenuItem(menuItem: MenuItem, placeId) {
        const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers.append('Access-Control-Allow-Origin', '*');
        headers.append('Accept', 'application/json');

        const menuItemJson = JSON.stringify(menuItem);

        const placeObservable = this.http.delete(this.webServiceUrl + '/' + placeId + '/' + this.menuPath, { headers });
        placeObservable.subscribe(x => {
            console.log(x);
        },
            err => console.log(err));
    }

    public UpdateMenuItem(menuItem: MenuItem, placeId) {
        const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers.append('Access-Control-Allow-Origin', '*');
        headers.append('Accept', 'application/json');

        const menuItemJson = JSON.stringify(menuItem);

        const placeObservable = this.http.put(this.webServiceUrl + '/'
            + placeId + '/' + this.menuPath + '/' + menuItem.id, menuItemJson, { headers });
        placeObservable.subscribe(x => {
            console.log(x);
        },
            err => console.log(err));
    }
}

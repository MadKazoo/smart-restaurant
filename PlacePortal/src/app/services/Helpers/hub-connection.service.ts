import { Injectable } from '@angular/core';
import { Subscription, Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import * as signalR from '@aspnet/signalr';



@Injectable({
    providedIn: 'root'
})

export class HubConnectionService {

    public connection: signalR.HubConnection;
    clientsHub = 'RestaurantHub';
    webServiceUrl: string;
    connectionSub: Subject<boolean>;
    isConnectionCreated: boolean;
    constructor() {
        this.connectionSub = new Subject();
        this.connectionSub.next(false);
        this.isConnectionCreated = false;
    }

    public createConnectionWithOrderHub(): Subject<boolean> {
        this.webServiceUrl = environment.webServiceUrl;
        console.log('starting connection with signalR web service');

        if (this.isConnectionCreated === false) {
            this.connection = new signalR.HubConnectionBuilder()
                .withUrl(this.webServiceUrl + '/' + this.clientsHub) // ,{ accessTokenFactory: () => this.userService.getToken() })
                .build();
            console.log('started connection with checkInHub');
            this.connection.start().then(x => {
                this.connectionSub.next(true);
                this.isConnectionCreated = true;
            },
                error => {
                    this.connectionSub.next(false);
                    this.isConnectionCreated = false;
                });
        }
        return this.connectionSub;
    }
}
export enum ConnectionStatusEnum {
    NOTCONNECTED = 0,
    CONNECTED = 1,
    CLOSED = 2
}

import { Injectable } from '@angular/core';
import { PlaceDto } from '../model/order';

@Injectable({
  providedIn: 'root'
})
export class PlaceContextService {
  places: PlaceDto[];
  place: PlaceDto;

  constructor() { }
}

import { Injectable } from '@angular/core';
import { PlaceTag, PlaceDto } from '../model/order';
import { HttpHeaders } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { UserService } from './user.service';
import { HeaderHelper } from './Helpers/headerHelper.service';
import { PlaceContextService } from './place-context.service';

@Injectable({
  providedIn: 'root'
})

export class PlaceService {
  webServiceUrl: string;
  placesPath = 'Place';
  placeTypesPath = 'PlaceType';

  constructor(private http: HttpClient,
    private userService: UserService, private headerHelper: HeaderHelper, private placesContext: PlaceContextService) {
    this.webServiceUrl = environment.webServiceUrl;
  }

  public GetPlaceTags() {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Accept', 'application/json');

    return this.http.get<PlaceTag[]>(this.webServiceUrl + '/' + this.placeTypesPath, { headers });

  }

  public GetPlaces() {
    const headers = this.headerHelper.createHeaderWithJWT();

    console.log('getting places');
    console.log(headers);
    // manage/user  manage
    const managePath = '/manage/user/';

    return this.http.get<PlaceDto[]>(this.webServiceUrl + '/' + this.placesPath + managePath + this.userService.getUserId(), { headers });
  }

  public CreatePlace(newPlace: PlaceDto) {
    const headers = this.headerHelper.createHeaderWithJWT();

    const place = JSON.stringify(newPlace);

    return this.http.post<PlaceDto>(this.webServiceUrl + '/' + this.placesPath, place, { headers });

  }


  public updatePlace(newPlace: PlaceDto) {
    const headers = this.headerHelper.createHeaderWithJWT();

    const newValue = JSON.stringify(newPlace);

    return this.http.put<PlaceDto>(
      this.webServiceUrl + '/' + this.placesPath + '/' + newPlace.id, newValue, { headers });

  }
  public updateProperty(placeId, propertyName, value) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Accept', 'application/json');

    const newValue = JSON.stringify(value);

    const placeObservable = this.http.put<PlaceDto>(
      this.webServiceUrl + '/' + this.placesPath + '/' + placeId + '/' + propertyName, newValue, { headers });
    placeObservable.subscribe(x => {
      console.log(x);
    },
      err => console.log(err));
  }
}

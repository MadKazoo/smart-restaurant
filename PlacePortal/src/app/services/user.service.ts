import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private userId: number;
  private token: string;
  constructor() {
  }

  getUserId() {
    return this.userId;
  }

  setToken(token: string){
    this.token = token;
  }
  getToken(): string{
    return this.token;
  }

  setUserId(userId: number) {
    this.userId = userId;
  }
}

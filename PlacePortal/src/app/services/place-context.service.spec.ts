import { TestBed } from '@angular/core/testing';

import { PlaceContextService } from './place-context.service';

describe('PlaceContextService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PlaceContextService = TestBed.get(PlaceContextService);
    expect(service).toBeTruthy();
  });
});

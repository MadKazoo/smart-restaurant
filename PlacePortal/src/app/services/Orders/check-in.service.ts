import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HeaderHelper } from '../Helpers/headerHelper.service';
import { HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { CheckIn } from '../../model/checkInDto';
import { CheckInContextService } from './check-in-context.service';
import { HubConnectionService } from '../Helpers/hub-connection.service';


@Injectable({
  providedIn: 'root'
})
export class CheckInService {
  webServiceUrl: string;

  placesPath = 'Place';

  placeCheckInsPath = 'CheckIn';

  constructor(private http: HttpClient, private serverConnectionService: HubConnectionService,
     private headerHelper: HeaderHelper, private checkInContext: CheckInContextService) {
    this.webServiceUrl = environment.webServiceUrl;
  }

  public getCurrentlyProcessedCheckIns(placeId) {
    const headers = this.headerHelper.createHeaderWithJWT();
    return this.http.get<CheckIn[]>(this.webServiceUrl + '/' + this.placesPath + '/' + placeId + '/' + this.placeCheckInsPath
      , { headers }).subscribe(checkIns => {

        console.log('got checkins');
        console.log(checkIns);
        this.checkInContext.setCheckIns(checkIns);
      });
  }
  public listenForOrders(placeId) {
    this.serverConnectionService.createConnectionWithOrderHub().subscribe(conn => {
      if (conn === true) {
        this.invokeAddToPlaceGroup(placeId).then(x => {
          this.subscribeToCheckIns();
        });
      }
    });
  }
  invokeAddToPlaceGroup(placeId) {
    console.log('joining resteurant orders');
    return this.serverConnectionService.connection.invoke('joinToRestaurantCheckIns', placeId);
}

invokeResignFromSubscription(placeId) {
    const resignFromOrdersSubscription = 'resignFromOrdersSubscription';
    return this.serverConnectionService.connection.invoke(resignFromOrdersSubscription, placeId);
}

public subscribeToCheckIns() {
    const subscribeToPlaceCheckIns = 'subscribeToPlaceCheckIns';
    console.log('susbscribing to checkin. finding user');
    this.serverConnectionService.connection.on(subscribeToPlaceCheckIns, data => {
        console.log(data);
        this.checkInContext.setCheckIns(data);
    });
}

}

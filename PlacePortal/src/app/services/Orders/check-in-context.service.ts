import { Injectable } from '@angular/core';
import { CheckIn } from 'src/app/model/checkInDto';

@Injectable({
  providedIn: 'root'
})
export class CheckInContextService {

  checkIns: CheckIn[];
  constructor() {
    this.checkIns = [];
  }

  setCheckIns(checkIns) {
    this.checkIns = checkIns;
  }
}

import { TestBed } from '@angular/core/testing';

import { CheckInContextService } from './check-in-context.service';

describe('CheckInContextService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CheckInContextService = TestBed.get(CheckInContextService);
    expect(service).toBeTruthy();
  });
});

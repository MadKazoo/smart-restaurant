import { Injectable } from '@angular/core';
import * as signalR from '@aspnet/signalr';
import { HubConnectionService } from '../Helpers/hub-connection.service';
import { OrdersContextService } from './orders-context';


@Injectable({
    providedIn: 'root'
})

export class PlaceOrderHub {
    constructor(private serverConnectionService: HubConnectionService,
        private orderContext: OrdersContextService) {
    }

    public listenForOrders(placeId) {
        this.serverConnectionService.createConnectionWithOrderHub().subscribe(conn => {
            if (conn === true) {
                this.invokeAddToPlaceGroup(placeId).then(x => {
                    this.subscribeToOrders();
                });
            }
        });
    }

    invokeAddToPlaceGroup(placeId) {
        console.log('joining resteurant orders');
        return this.serverConnectionService.connection.invoke('joinToRestaurantOrders', placeId);
    }

    invokeResignFromSubscription(placeId) {
        const resignFromOrdersSubscription = 'resignFromOrdersSubscription';
        return this.serverConnectionService.connection.invoke(resignFromOrdersSubscription, placeId);
    }

    public subscribeToOrders() {
        const subscribeToPlaceOrders = 'subscribeToPlaceOrders';
        console.log('susbscribing to checkin. finding user');
        this.serverConnectionService.connection.on(subscribeToPlaceOrders, data => {
            console.log(data);
            this.orderContext.setOrders(data);
        });
    }
}

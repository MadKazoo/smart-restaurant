import { Injectable } from '@angular/core';
import { Order } from '../../model/order';

@Injectable({
    providedIn: 'root'
})

export class OrdersContextService {
    /**
     *
     */

    orders: Order[];
    constructor() {
        this.orders = [];
    }

    setOrders(orders) {
        this.orders = orders;
    }
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddPlaceFormComponent } from './add-place-form/add-place-form.component';
import { AgmCoreModule } from '@agm/core';
import { FormsModule } from '@angular/forms';
import { NgbActiveModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { PlaceService } from './services/place.service';
import { HttpClientModule } from '@angular/common/http';
import { ModalComponent } from './modal/modal.component';
import { MenuItemComponent } from './menu-item/menu-item.component';
import { GeoLocationService } from './services/geo-location.service';
import { PlacesComponent } from './places/places.component';
import { LoginService } from './services/login.service';
import { HeaderHelper } from './services/Helpers/headerHelper.service';
import { UserService } from './services/user.service';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './login/authGuard.service';
import { OrdersComponent } from './orders/orders.component';
import { OrdersContextService } from './services/Orders/orders-context';
import { HubConnectionService } from './services/Helpers/hub-connection.service';
import { PlaceOrderHub } from './services/Orders/place-order-hub';



@NgModule({
  declarations: [
    AppComponent,
    AddPlaceFormComponent,
    ModalComponent,
    MenuItemComponent,
    PlacesComponent,
    LoginComponent,
    OrdersComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    NgbModule.forRoot(),
    HttpClientModule,
    AgmCoreModule.forRoot({

      // please get your own API key here:
      // https://developers.google.com/maps/documentation/javascript/get-api-key?hl=en
      apiKey: 'AIzaSyAw8X3yTdID166E7i-JVG5JDeiPvx6gpbY'
    })
  ],
  providers: [PlaceService, GeoLocationService, LoginService, HeaderHelper,
    UserService, AuthGuard, OrdersContextService, HubConnectionService, PlaceOrderHub],
  bootstrap: [AppComponent]
})
export class AppModule { }

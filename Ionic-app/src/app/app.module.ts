import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { BasketService } from './places/basket/basket.service';
import { HeaderHelper } from './services/Helpers/headerHelper.service';
import { AuthGuard } from './services/Authorization/authGuard.service';
import { SignalRServerConnectionService } from './services/CheckInConnection/signal-rserver-connection.service';
import { CheckInConnectionAdapterService } from './services/CheckInConnection/check-in-connection-adapter.service';
import { CheckInContextService } from './services/Context/check-in-context.service';
import { CurrentOrderContextService } from './current-order/current-order-context.service';
import { OrdersAlertLogicService } from './services/Order/orders-alert.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, HttpClientModule],
  providers: [
    SignalRServerConnectionService,
    CheckInConnectionAdapterService,
    CheckInContextService,
    CurrentOrderContextService,
    OrdersAlertLogicService,
    StatusBar,
    Geolocation,
    SplashScreen,
    BasketService,
    HeaderHelper,
    AuthGuard,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}

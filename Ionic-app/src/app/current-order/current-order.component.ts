import { Component, OnInit } from '@angular/core';
import { CurrentOrderService } from '../services/Order/currentOrder.service';
import { Subscription } from 'rxjs';
import { CurrentOrderContextService } from './current-order-context.service';
import { OrderDto, CheckInStatus, OrderStatus } from '../model/orderDto';
import { Router } from '@angular/router';

@Component({
  selector: 'app-current-order',
  templateUrl: './current-order.component.html',
  styleUrls: ['./current-order.component.scss']
})
export class CurrentOrderComponent implements OnInit {

  currentOrderText = 'Aktualne zamówienia';
  constructor(public orderContext: CurrentOrderContextService, private currentOrderService: CurrentOrderService, private router: Router) {
  }
  newOrderSubscription: Subscription;

  hasPaid(order: OrderDto): boolean {
    return this.currentOrderService.isPayButtonVisible(order);
  }

  getEnumString(orderStatus: CheckInStatus): string {
    return OrderStatus[orderStatus];
  }

  getUserPaymentStatus(order: OrderDto): string {
    const hasPaid = this.currentOrderService.hasUserPaid(order);
    if (order.orderStatus === OrderStatus.COMPLETED) {
      return 'Completed';
    } else if (hasPaid && order.orderStatus === OrderStatus.PAID) {
      return 'Paid';
    } else if (!hasPaid && order.orderStatus === OrderStatus.ACCEPTED) {
      return 'Waiting for payment';
    } else if (order.orderStatus === OrderStatus.ACCEPTED && hasPaid) {
      return 'Waiting for all payments';
    } else {
      return 'In progress';
    }
  }
  getUserPaymentShare(order: OrderDto) {
    const paymentShare = this.currentOrderService.getUserPaymentShare(order);
    return paymentShare;
  }

  payForOrder(orderId: Number) {
    this.router.navigate(['tabs/payment/' + orderId]);
  }
  ngOnInit() {
  }


}

import { Injectable } from '@angular/core';
import { OrderDto } from '../model/orderDto';

@Injectable({
  providedIn: 'root'
})
export class CurrentOrderContextService {
  public orders: OrderDto[];
  public currentlyProceedOrders: OrderDto[];

  addOrder(order: OrderDto) {
    this.orders.push(order);
  }

  public getOrders() {
    return this.orders.reverse();
  }

  constructor() { }
}

import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { TabsPageRoutingModule } from './tabs.router.module';

import { TabsPage } from './tabs.page';
import { HttpClientModule } from '@angular/common/http';
import { OrderAlertComponent } from '../order-alert/order-alert.component';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    HttpClientModule,
    FormsModule,
    TabsPageRoutingModule
    ],
  declarations: [TabsPage, OrderAlertComponent]
})
export class TabsPageModule {}

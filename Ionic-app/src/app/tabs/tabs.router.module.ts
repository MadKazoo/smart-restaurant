import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      // {
      //   path: 'home',
      //   children: [
      //     {
      //       path: '',
      //       loadChildren: '../home/home.module#HomePageModule'
      //     }
      //   ]
      // },
      {
        path: 'check-in-scanner',
        children: [
          {
            path: '',
            loadChildren: '../check-in-scanner/check-in-scanner.module#CheckInScannerPageModule'
          }
        ]
      },
      {
        path: 'browse',
        children: [
          {
            path: '',
            loadChildren: '../browse/browse.module#BrowsePageModule',

          },
          {
            path: 'place-menu',
            children: [
              {
                path: '',
                loadChildren: '../places/place-menu/place-menu.module#PlaceMenuPageModule'
              },
            ]
          },
        ]
      },
      {
        path: 'orders',
        children: [
          {
            path: '',
            loadChildren: '../orders/orders.module#OrdersPageModule'
          }
        ]
      },
      {
        path: 'payment',
        children: [
          {
            path: '',
            loadChildren: '../payment/payment.module#PaymentPageModule'
          }
        ]
      }
      // { path: 'basket', loadChildren: '../places/basket/basket.module#BasketPageModule' },
      // {
      //   path: '',
      //   redirectTo: '/tabs/home',
      //   pathMatch: 'full'
      // }
      ,
      {
        path: '',
        redirectTo: '/tabs/browse',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule { }

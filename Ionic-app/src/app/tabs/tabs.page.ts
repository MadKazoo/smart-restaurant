import { Component } from '@angular/core';
import { PlacesContextService } from '../places/services/places-context.service';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  constructor(private placesContext: PlacesContextService) {
  }

  getSelectionTab() {
    if (this.placesContext.lastCheckedPlace !== undefined && this.placesContext.lastCheckedPlace != null) {
      return 'browse/place-menu/' + this.placesContext.lastCheckedPlace.id;
    }
    return 'browse';
  }
}

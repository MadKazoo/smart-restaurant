import { MenuItem } from "./menuItem";

export class PlaceMenu {
    public PlaceId: String;
    public placeName: String;
    public menuItems: MenuItem[];
}
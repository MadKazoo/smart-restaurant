import { MenuItem } from './menuItem';
import { PartialOrder } from './partialOrder';
import { UserOrderStatus } from './userOrderStatus';

export class OrderDto {
        id: number;
        userId: number;
        groupId: string;
        menuItems: MenuItem[];
        orderStatus: OrderStatus;
        userOrderStatuses: UserOrderStatus[];
        placeId: number;
        placeName: string;
        dateCreated: Date;
        partialOrders: PartialOrder[];
        price: number;
}

export enum OrderStatus {
        NEW = 0,
        ACCEPTED = 1,
        PAID = 2,
        COMPLETED = 3
}

export enum CheckInStatus {
        NEW = 0,
        FINISHED = 1,
        CANCELED = 2
}
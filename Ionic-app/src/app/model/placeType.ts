import { PlaceTag } from "./place";

export class PlaceType{
    public constructor(name: string, icon: string){
        this.name = name;
        this.imageSrc = icon;
    }
    public name:string;
    public imageSrc:string;
}
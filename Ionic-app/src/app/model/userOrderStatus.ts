export class UserOrderStatus {
    userId: number;
    hasPaid: boolean;
    toPay: number;
}

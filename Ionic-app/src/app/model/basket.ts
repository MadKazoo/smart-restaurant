import { MenuItem } from './menuItem';
import { Place } from './place';

export class Basket {
    public itemsAmountMap = new Map<MenuItem, number>();
    items: MenuItem[] = [];
    public userId: number;
    public place: Place;
    public price: number;

    removeFromItems(menuItem: MenuItem) {
        const index = this.items.indexOf(menuItem);
        this.items.splice(index, 1);
    }
    addToItems(menuItem: MenuItem) {
        this.items.push(menuItem);
    }
    // addToItemsMap(menuItem: MenuItem){
    //     if(this.itemsAmountMap.get(menuItem) === null)
    //     {
    //         this.itemsAmountMap.set(menuItem, 1);
    //         return;
    //     }

    //     var test = this.itemsAmountMap.get(menuItem);
    //         let amount = this.itemsAmountMap.get(menuItem);

    //         amount = 1;
    //         this.itemsAmountMap.set(menuItem, amount);
    // }

    // removeFromItemsMap(menuItem: MenuItem){
    //     let amount = this.itemsAmountMap.get(menuItem);
    //     amount = amount - 1;
    //     if(amount<=0){
    //         this.itemsAmountMap.delete(menuItem);
    //     }
    //     else{
    //         this.itemsAmountMap.set(menuItem, amount);
    //     }
    // }
    // getMenuItemsMap(): Map<MenuItem, number>{
    //     return this.itemsAmountMap;
    // }
    getMenuItems(): MenuItem[] {
        return this.items;
    }
}

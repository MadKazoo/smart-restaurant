export class Location {
    public name: string;
    public latitude: number;
    public longitude: number;
}

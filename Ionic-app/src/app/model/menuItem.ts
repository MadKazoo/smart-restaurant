export class MenuItem {
    id: Number;
    name: string;
    description: String;
    public price: number;
    imageUrl: String;
    rate: Number = 609;
    amount: Number = 0;
}
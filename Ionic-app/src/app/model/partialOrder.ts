import { MenuItem } from './menuItem';
import { ClientDto } from './checkInDto';

export class PartialOrder {
    id: number;
    public menuItem: MenuItem;
    public userPartialOrders: UserPartialOrder[];
}

export class UserPartialOrder {
    public name = '';
    public toPayInMoney: number;
    public clientPercentShare: number;
    public appUserId: number;

    public isClientReady: boolean;
    public clientDto: ClientDto;
    public hasPaid: boolean;
}

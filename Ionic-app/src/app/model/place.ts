import { Location } from './location';


export class Place {
    public id: number;
    public name: string;
    public location: Location;
    public imageUrl: string;
    public description: string;
    public avarageRating: number;
    public placeTags: PlaceTag[];

}
export enum PlaceTag {
    PIZZA = 1,
    SUSHI = 2,
    CHICKEN = 3,
    SPANISH = 4,
}
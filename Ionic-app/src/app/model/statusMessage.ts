export class StatusMessage {

    message: String;
    status: Status;

}

export enum Status{
    NEW = 0,
    ACCEPTED = 1,
    READY = 2,
    FINISHED = 3
}
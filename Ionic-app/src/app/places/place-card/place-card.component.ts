import { Component, OnInit, Input } from '@angular/core';
import { Place } from '../../model/place';
import { PlacesService } from 'src/app/places/services/places.service';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { BasketService } from '../basket/basket.service';
import { PlacesContextService } from 'src/app/places/services/places-context.service';

@Component({
  selector: 'app-place-card',
  templateUrl: './place-card.component.html',
  styleUrls: ['./place-card.component.scss']
})
export class PlaceCardComponent implements OnInit {

  @Input()
  place: Place;
  counter: number;
  ngOnInit() {
  }

  constructor(private placesContext: PlacesContextService, private navCtrl: NavController, private basketService: BasketService) {
    this.counter = 0;
  }

  setPlaceAsLastChecked() {
    this.placesContext.lastCheckedPlace = this.place;
    this.basketService.setPlace(this.place);
    this.navCtrl.navigateForward(['/tabs/browse/place-menu/' + this.place.id]);
  }
}

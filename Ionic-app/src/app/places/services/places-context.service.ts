import { Injectable } from '@angular/core';
import { Place } from '../../model/place';

@Injectable({
  providedIn: 'root'
})
export class PlacesContextService {

  constructor() { }

  places: Place[];
  lastCheckedPlace: Place;

  setLastCheckedPlace(place: Place) {
    this.lastCheckedPlace = place;
  }
  setPlaces(places: Place[]) {
    this.places = places;
  }
  addPlaces(newPlaces) {
    console.log(this.places);
    console.log(newPlaces);
    this.places = this.places.concat(newPlaces);
  }

  getPlaceByIdFromContext(id: number): Place {
    if (this.places != null) {
      const place = this.places.filter(x => x.id === id)[0];
      return place;
    }
    return null;
  }
}


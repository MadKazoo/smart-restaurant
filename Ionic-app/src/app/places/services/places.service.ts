import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { Place } from '../../model/place';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { PlaceType } from 'src/app/model/placeType';
import { ApiEndpointsService } from 'src/app/services/API/api-endpoints.service';

@Injectable({
  providedIn: 'root'
})
export class PlacesService {

  webServiceUrl: string;

  constructor(private http: HttpClient, private apiEndpointService: ApiEndpointsService) {
    this.webServiceUrl = environment.webServiceUrl;
  }

  public getPlace(id: string): Observable<Place> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });

    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Accepst', 'application/json');
    const placeObservable = this.http.get<Place>(this.apiEndpointService.getPlaceUrl(id), { headers: headers, params: null });
    return placeObservable;
  }

  public getNearbyPlaces(lat, lng, distance): Observable<Place[]>  {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });

    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Accepst', 'application/json');
    const placeObservable = this.http.get<Place[]>
      (this.apiEndpointService.getNearbyPlacesUrl(lat, lng, distance), { headers: headers, params: null });
    return placeObservable;
  }

  public getNearest(lat, lng, amountToSkip): Observable<Place[]>  {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });

    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Accepst', 'application/json');
    const placeObservable = this.http.get<Place[]>
      (this.apiEndpointService.getNearestPlacesUrl(lat, lng, amountToSkip), { headers: headers, params: null });
    return placeObservable;
  }

  public getAllPlaces(): Observable<Place[]> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });

    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Accept', 'application/json');
    const placeObservable = this.http.get<Place[]>(this.apiEndpointService.getPlaceBaseUrl(), { headers: headers, params: null });
    return placeObservable;
  }


  public getPlaceTypes(): Observable<PlaceType[]> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });

    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Accept', 'application/json');
    const placeTypesObservable = this.http.get<PlaceType[]>(this.apiEndpointService.getPlaceTypesUrl(), { headers: headers, params: null });
    return placeTypesObservable;
  }
}

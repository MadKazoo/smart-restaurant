import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PlaceMenu } from '../../model/placeMenu';
import { ApiEndpointsService } from 'src/app/services/API/api-endpoints.service';
import { MenuItem } from 'src/app/model/menuItem';

@Injectable({
  providedIn: 'root'
})
export class PlaceMenuService {

  placesPath = '/places/';
  webServiceUrl: string;

  constructor(private http: HttpClient, private apiEndpointService : ApiEndpointsService) {
    this.webServiceUrl = environment.webServiceUrl;
  }

  getPlaceMenu(id: string): Observable<MenuItem[]> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });

    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Accept', 'application/json');
    const placeObservable = this.http.get<MenuItem[]>(this.apiEndpointService.getPlaceMenuUrl(id), { headers: headers, params: null });
    return placeObservable;
  }
}

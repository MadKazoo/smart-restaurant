import { TestBed } from '@angular/core/testing';

import { PlacesContextService } from './places-context.service';

describe('PlacesContextService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PlacesContextService = TestBed.get(PlacesContextService);
    expect(service).toBeTruthy();
  });
});

import { Component, Input, OnDestroy, OnInit, ChangeDetectorRef } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { ClientDto, CheckInDto } from 'src/app/model/checkInDto';
import { PartialOrder, UserPartialOrder } from 'src/app/model/partialOrder';
import { CheckInConnectionAdapterService } from 'src/app/services/CheckInConnection/check-in-connection-adapter.service';
import { CheckInContextService } from 'src/app/services/Context/check-in-context.service';
import { UserService } from 'src/app/services/Context/user.service';
import { BasketService } from '../basket/basket.service';
import { AlertFactory } from './alertFactory.service';
import { MenuItem } from 'src/app/model/menuItem';
import { OrderConnectionAdapterService } from 'src/app/services/Order/order-connection-adapter.service';
import { CurrentOrderContextService } from 'src/app/current-order/current-order-context.service';

@Component({
  selector: 'app-check-in',
  templateUrl: './check-in.component.html',
  styleUrls: ['./check-in.component.scss']
})
export class CheckInComponent implements OnInit, OnDestroy {

  @Input()
  placeId: number;
  public isReady: boolean;
  newOrderSubscription: Subscription;

  nextToTableString = 'Eat together';
  joinText = 'join';
  changeText = 'change';
  resignText = 'resign';
  yourOrdersString = 'Your orders';

  orderButtonText = 'Order';

  cancelText = 'Cancel';

  priceCurrency = 'zł';


  constructor(private checkInConnectionAdapter: CheckInConnectionAdapterService,
    public checkInContext: CheckInContextService, public basket: BasketService, private orderContext: CurrentOrderContextService,
    private basketService: BasketService, private orderConnectionService: OrderConnectionAdapterService,
    private userService: UserService, private alertFactory: AlertFactory, public alertController: AlertController) {

    this.checkInConnectionAdapter.checkIn(null, null);
  }

  ngOnInit() {
  }

  checkOut() {
    this.checkInConnectionAdapter.checkOut();
  }

  resignFromPartialOrder(partialOrder: PartialOrder) {
    this.checkInConnectionAdapter.resignFromPartialOrder(partialOrder);
  }

  checkIn() {
    this.presentAlertWithChooseTable();
  }

  public removeItemFromBasket(menuItem: MenuItem) {
    this.basket.removeItemFromBasket(menuItem);
  }

  subscribeToCheckIn() {
    this.checkInConnectionAdapter.checkIn(null, null);
  }
  isUserParticipatingInPartialOrder(partialOrder: PartialOrder): boolean {
    const userId = this.userService.getUserId();
    if (partialOrder.userPartialOrders.find(x => x.appUserId === userId)) {
      return true;
    }
    return false;
  }

  isUserParticipatingInCheckIn() {
    if (this.checkInContext.checkInDto != null) {
      return true;
    }
    return false;
  }

  async presentAlertWithChooseTable() {
    const alert = await this.alertFactory.presentDialogToCheckIn(this.placeId, this.basket.basket.getMenuItems());
    await alert.present();
  }

  async joinToPartialOrder(partialOrder: PartialOrder) {
    const alert = await this.alertFactory.createJoinToPartialOrderAlert(partialOrder);
    await alert.present();
  }
  getIsReady() {
    if (this.checkInContext === undefined || this.checkInContext === null) {
      return false;
    }
    return this.checkInContext.isReadyForOrder;
  }
  setUserAsNotReadyForOrder() {
    this.checkInConnectionAdapter.setUserStatusToCheckIn(false);
    this.isReady = false;
  }

  private SetReadyForOrder() {
    this.checkInConnectionAdapter.setUserStatusToCheckIn(true);

    this.newOrderSubscription = this.checkInConnectionAdapter.subscribeToNewOrder()
      .subscribe(order => {
        this.orderConnectionService.createNewSubscription(order.groupId);
        this.orderContext.addOrder(order);
        this.checkInContext.resetCheckIn();
      });

    this.isReady = true;
  }

  private findItemsOnlyForAppUser(): Array<{ name: string, price: number }> {
    const userId = this.userService.getUserId();
    const menuItemMap: Array<{ name: string, price: number }> = [];
    this.checkInContext.checkInDto.partialOrders.forEach(element => {
      const userIndex: UserPartialOrder = element.userPartialOrders.find(z => z.appUserId === userId);
      if (userIndex !== undefined && userIndex != null) {
        const toPay = userIndex.toPayInMoney;
        menuItemMap.push({ name: element.menuItem.name, price: toPay });
      }
    });
    return menuItemMap;
  }

  isReadyForOrder(user: ClientDto): boolean {
    return user.isReadyForOrder;
  }

  async setUserAsReadyForOrder() {
    console.log('setting user status');
    if (this.checkInConnectionAdapter.isUserCheckedIn()) {
      const itemsPrices = this.findItemsOnlyForAppUser();
      const alert = await this.alertFactory.presentInformationAboutOrder(itemsPrices, this.SetReadyForOrder());
      await alert.present();

    } else {
      // TODO - take it from checkIn
      const items = this.basketService.getMenuItems();
      const alert = await this.alertFactory.presentDialogToCheckIn(this.placeId, items);
      await alert.present();
    }
  }

  ngOnDestroy(): void {
  }

}

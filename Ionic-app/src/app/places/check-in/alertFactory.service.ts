import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { MenuItem } from 'src/app/model/menuItem';
import { OrderDto } from 'src/app/model/orderDto';
import { PartialOrder } from 'src/app/model/partialOrder';
import { CheckInConnectionAdapterService } from 'src/app/services/CheckInConnection/check-in-connection-adapter.service';


@Injectable({
  providedIn: 'root'
})

export class AlertFactory {

  constructor(private alertController: AlertController, private router: Router,
    private hubConnectionService: CheckInConnectionAdapterService) { }

  private multipleOrdersToBePaidString = 'Orders';
  private ordersNeededToBePaidString = 'There are multiple orders waiting to be paid';
  priceCurrency = 'zł';
  ordersHeaderText = 'Zamówienia';
  choseTableText = 'Wybierz numer stolika';
  okButtonText = 'Ok';
  cancelButtonText = 'Anuluj';
  priceText = 'Cena';
  youWillPayText = 'Zapłacisz';
  cantFindGivenTableText = 'Nie mogę znaleźć stolika';
  youNeedToCheckInHeader = 'You need to specify table number first';
  checkInUsingTablenumber = 'Provide table number';
  readyToPayOrderDialogHeader = 'Pay';
  readyToPayOrderDialogSubHeader = 'Sum:';

  cancelText = 'Cancel';


  async createJoinToPartialOrderAlert(partialOrder: PartialOrder) {
    const alert = await this.alertController.create({
      header: partialOrder.menuItem.name,
      subHeader: this.priceText + ' ' + partialOrder.menuItem.price,
      message: this.youWillPayText,
      inputs: [
        {
          name: 'moneyToPay',
          type: 'number',
          max: partialOrder.menuItem.price,
          min: 0
        }
      ],
      buttons: [
        {
          text: this.cancelButtonText,
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: this.okButtonText,
          handler: (data) => {
            this.hubConnectionService.joinToPartialOrder(partialOrder, data.moneyToPay);
          }
        }
      ]
    });

    return alert;
  }

  async presentAlertWithInformation(headerText: string) {
    const alert = await this.alertController.create({
      header: headerText,
      buttons: [
        {
          text: this.cancelButtonText,
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: this.okButtonText,
          handler: (data) => {
          }
        }
      ]
    });

    return alert;
  }

  async presentAlertWithChooseTable(placeId: number, methodToCall: any) {
    const alert = await this.alertController.create({
      header: this.choseTableText,
      inputs: [
        {
          name: 'tablenumber',
          type: 'number'
        }
      ],
      buttons: [
        {
          text: this.cancelButtonText,
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: this.okButtonText,
          handler: (data) => {
            this.hubConnectionService.checkIn(placeId, data.tablenumber);
            methodToCall();
          }
        }
      ]
    });

    return alert;
  }

  // stuff to move out
  async presentDialogToCheckIn(placeId: number, items: MenuItem[]) {
    // const items = this.basketService.getMenuItems();
    // console.log(items);
    let sum = 0;

    items.forEach(element => {
      sum += element.price.valueOf();
    });

    const alert = await this.alertController.create({
      header: this.youNeedToCheckInHeader,
      subHeader: this.readyToPayOrderDialogSubHeader + sum + this.priceCurrency,
      inputs: [
        {
          name: 'tablenumber',
          type: 'number'
        }
      ],
      buttons: [
        {
          text: 'cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Check in',
          handler: (data) => {
            if (data === null || data === undefined || data === '') {
              return false;
            }
            this.hubConnectionService.checkInAddingItmes(placeId, data.tablenumber, items);
          }
        }
      ]
    });

    return alert;
  }

  private delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  async presentInformationAboutOrder(itemsPrices: Array<{ name: string, price: number }>, methodToCall: any) {
    // TODO exact info should be taken from the server
    let sum = 0;
    itemsPrices.forEach(element => {
      sum += element.price.valueOf();
    });
    const prices = this.itemsToPayToString(itemsPrices);

    const alert = await this.alertController.create({
      header: this.readyToPayOrderDialogHeader,
      subHeader: this.readyToPayOrderDialogSubHeader + sum + this.priceCurrency,
      message: prices,

      buttons: [
        {
          text: 'cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'ok',
          handler: () => {
            methodToCall();
          }
        }
      ]
    });

    return alert;
  }

  itemsToPayToString(items: Array<{ name: string, price: number }>): string {
    let itemsString = '';

    items.forEach(entry => {
      itemsString = itemsString + entry.name.toString() + ': ' + entry.price + this.priceCurrency + ', ';
    }
    );

    return itemsString;
  }

  async presentAlertyWithMultipleOrdersToBePaidNotification() {
    const alert = await this.alertController.create({
      header: this.multipleOrdersToBePaidString,
      subHeader: this.ordersNeededToBePaidString,
      message: this.youWillPayText,
      buttons: [
        {
          text: this.okButtonText,
          handler: (data) => {
            this.router.navigate(['tabs/orders']);
          }
        }
      ]
    });
    return alert;
  }


  async presentAlertyWithSingleOrderToBePaid(order: OrderDto) {
    const alert = await this.alertController.create({
      header: this.ordersHeaderText,
      subHeader: order.placeName,
      message: order.dateCreated.toString(),
      buttons: [
        {
          text: this.okButtonText,
          handler: (data) => {
            this.router.navigate(['tabs/payment/' + order.id]);
          }
        }
      ]
    });
    return alert;
  }
}

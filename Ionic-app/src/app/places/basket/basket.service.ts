import { Injectable } from '@angular/core';
import { MenuItem } from '../../model/menuItem';
import { Place } from 'src/app/model/place';
import { Basket } from 'src/app/model/basket';

@Injectable({
  providedIn: 'root'
})
export class BasketService {

  public basket: Basket = new Basket();

  constructor() {
  }

  addToMenuItems(menuItem: MenuItem) {

    this.basket.addToItems(menuItem);
  }

  getMenuItemPrice() {
    let price = 0;
    this.basket.items.forEach(x => price += x.price);
  }

  clearBasket() {
    this.basket.itemsAmountMap.clear();
  }

  removeItemFromBasket(menuItem: MenuItem) {
    this.basket.removeFromItems(menuItem);
  }

  setPlace(place: Place) {
    this.basket.place = place;
  }

  getMenuItems(): MenuItem[] {
    return this.basket.getMenuItems();
  }
}

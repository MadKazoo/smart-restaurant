import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'src/app/model/menuItem';
import { BasketService } from './basket.service';
import { OrderDto } from 'src/app/model/orderDto';


@Component({
  selector: 'app-basket',
  templateUrl: './basket.page.html',
  styleUrls: ['./basket.page.scss'],
})
export class BasketPage implements OnInit {
  menuItems: MenuItem[] = [];

  constructor(private basketService: BasketService) {
    this.updateBasket();
  }

  ngOnInit() {
  }

  submitOrder() {
    const order = new OrderDto();
    order.menuItems = Array.from(this.menuItems);
    order.orderStatus = null;

  }

  handleError(error) {
    console.log(error);
  }

  removeItemFromBasket(menuItem: MenuItem) {
    this.basketService.removeItemFromBasket(menuItem);
    this.updateBasket();
  }

  private updateBasket() {
    // this.basketService.basket.menuItems.subscribe(items=>{
    //   console.log('dodano item');
    //   this.menuItems = items;
    // });
    // var items = this.basketService.getMenuItems();

    this.menuItems = this.basketService.getMenuItems();
  }
}

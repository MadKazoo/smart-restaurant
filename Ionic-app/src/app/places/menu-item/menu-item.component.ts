import { Component, Input, OnInit } from '@angular/core';
import { MenuItem } from 'src/app/model/menuItem';
import { CheckInConnectionAdapterService } from 'src/app/services/CheckInConnection/check-in-connection-adapter.service';
import { BasketService } from '../basket/basket.service';

@Component({
  selector: 'app-menu-item',
  templateUrl: './menu-item.component.html',
  styleUrls: ['./menu-item.component.scss']
})
export class MenuItemComponent implements OnInit {

  @Input() menuItem: MenuItem;

  constructor(private basketService: BasketService, private hubConnectionService: CheckInConnectionAdapterService) { }
  addButtonText = 'Add';
  ngOnInit() {
  }

  addToBasket() {
    if (this.hubConnectionService.isUserCheckedIn()) {
      this.hubConnectionService.addItemToCheckInOrder(this.menuItem);
    } else {
      this.basketService.addToMenuItems(this.menuItem);
    }
  }
}

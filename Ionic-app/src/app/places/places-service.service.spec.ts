import { TestBed } from '@angular/core/testing';

import { PlacesService } from './services/places.service';

describe('PlacesServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PlacesService = TestBed.get(PlacesService);
    expect(service).toBeTruthy();
  });
});

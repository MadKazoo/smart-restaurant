import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PlaceMenuPage } from './place-menu.page';
import { MenuItemComponent } from '../menu-item/menu-item.component';
import { CheckInConnectionService } from 'src/app/services/CheckInConnection/check-in.service';
import { CheckInComponent } from '../check-in/check-in.component';
import { AlertFactory } from '../check-in/alertFactory.service';

const routes: Routes = [
  {
    path: ':id',
    component: PlaceMenuPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
  ],
  providers: [CheckInConnectionService, AlertFactory],

  declarations: [PlaceMenuPage, MenuItemComponent, CheckInComponent]
})
export class PlaceMenuPageModule { }

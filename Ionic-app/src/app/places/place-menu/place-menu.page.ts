import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Observable, Subscription } from 'rxjs';
import { CurrentOrderContextService } from 'src/app/current-order/current-order-context.service';
import { Place } from 'src/app/model/place';
import { PlaceMenu } from 'src/app/model/placeMenu';
import { PlacesContextService } from 'src/app/places/services/places-context.service';
import { PlacesService } from 'src/app/places/services/places.service';
import { CheckInContextService } from 'src/app/services/Context/check-in-context.service';
import { CheckInConnectionAdapterService } from 'src/app/services/CheckInConnection/check-in-connection-adapter.service';
import { PlaceMenuService } from 'src/app/places/services/place-menu.service';
import { UserService } from 'src/app/services/Context/user.service';
import { BasketService } from '../basket/basket.service';
import { AlertFactory } from '../check-in/alertFactory.service';
import { MenuItem } from 'src/app/model/menuItem';
import { OrderConnectionAdapterService } from 'src/app/services/Order/order-connection-adapter.service';

@Component({
  selector: 'app-place-menu',
  templateUrl: './place-menu.page.html',
  styleUrls: ['./place-menu.page.scss'],
})
//// toooooo refactor
export class PlaceMenuPage implements OnInit {
  menuString = 'Menu';
  orderString = 'Order';
  readyToPayOrderDialogHeader = 'Pay';
  readyToPayOrderDialogSubHeader = 'Sum:';
  priceCurrency = 'zł';
  youNeedToCheckInHeader = 'You need to specify table number first';
  checkInUsingTableNumber = 'Provide table number';
  isTableSegmentHidden = true;
  category: any;
  placeId: string;
  public placeMenu: MenuItem[];
  place: Place;
  public isReady: boolean;
  placeMenuSubscribtion: Observable<MenuItem[]>;
  newOrderSubscription: Subscription;

  constructor(private route: ActivatedRoute, private placeMenuService: PlaceMenuService,
    private placesContext: PlacesContextService, private placeService: PlacesService,
   private checkInConnectionAdapter: CheckInConnectionAdapterService, private checkInContext: CheckInContextService) {
    this.isReady = false;
    this.placeMenu = [];

    this.placeId = this.route.snapshot.paramMap.get('id');
    const decimalRadix = 10;
    const idNumber = Number.parseInt(this.placeId, decimalRadix);

    this.place = this.placesContext.getPlaceByIdFromContext(idNumber);
    if (this.place == null) {
      this.placeService.getPlace(this.placeId).subscribe(place => {
        this.place = place;
      });
    }
    this.placeMenuSubscribtion = this.placeMenuService.getPlaceMenu(this.placeId);
    this.placeMenuSubscribtion.subscribe(menu => {
      this.placeMenu = menu;
    },
      error => {
        this.handleError(error);
      }
    );
  }

  ngOnInit() {
  }
  changeUserReadyToOrder() {
    this.checkInConnectionAdapter.setUserStatusToCheckIn(true);
  }


  itemsToPayToString(items: Array<{ name: string, price: number }>): string {
    let itemsString = '';

    items.forEach(entry => {
      itemsString = itemsString + entry.name.toString() + ': ' + entry.price + this.priceCurrency + ', ';
    }
    );

    return itemsString;
  }

  getIsReady() {
    return this.checkInContext.isReadyForOrder;
  }

  handleError(error) {
    console.log(error);
  }

  segmentChanged(ev: any) {
    this.isTableSegmentHidden = !this.isTableSegmentHidden;
  }

  doRefresh(event) {
    // Todo - should update check in
    this.checkInConnectionAdapter.getCheckIn().subscribe(x => {
      this.checkInContext.setCheckIn(x);
      event.target.complete();
    });
  }
}

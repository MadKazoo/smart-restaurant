import { Component, OnInit } from '@angular/core';
import { Place, PlaceTag } from '../model/place';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { PlacesService } from './services/places.service';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { PlacesContextService } from './services/places-context.service';
import { PlaceType } from '../model/placeType';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@Component({
  selector: 'app-places',
  templateUrl: './places.component.html',
  styleUrls: ['./places.component.scss']
})
export class PlacesComponent implements OnInit {
  visiblePlaces: Place[] = [];

  placeTypes: PlaceType[];
  searchTerm = '';
  distanceText = 'Distance';
  currentPlaceTag: PlaceTag;
  hardcodedLocationLon = 20.996254;
  hardcodedLocationLat = 52.205601;

  distanceValue = 5000;

  constructor(private plcesService: PlacesService, private placesContext: PlacesContextService, private geolocation: Geolocation) {
    this.setPlaceTypes();
    const hardcodedLocationLon = 20.996254;
    const hardcodedLocationLat = 52.205601;

    this.geolocation.getCurrentPosition().then((resp) => {
      const lat = resp.coords.latitude;
      const lng = resp.coords.longitude;
      let amountToSkip = 0;
      if (this.placesContext.places !== undefined && this.placesContext.places !== null) {
        amountToSkip = this.placesContext.places.length;
      }
      this.plcesService.getNearest(lat, lng, amountToSkip).subscribe(places => {
        placesContext.setPlaces(places);
        this.visiblePlaces = places;
      });
      // resp.coords.latitude
      // resp.coords.longitude
    }).catch((error) => {
      console.log('Error getting location', error);
      let amountToSkip = 0;
      if (this.placesContext.places !== undefined && this.placesContext.places !== null) {
        amountToSkip = this.placesContext.places.length;
      }
      this.plcesService.getNearest(hardcodedLocationLat, hardcodedLocationLon, amountToSkip).subscribe(places => {
        placesContext.setPlaces(places);
        this.visiblePlaces = places;
      });
    });
  }
  ngOnInit() {
  }

  loadMoreData() {
    console.log('loading more places');
    let amountToSkip = 0;
    if (this.placesContext.places !== undefined && this.placesContext.places !== null) {
      amountToSkip = this.placesContext.places.length;
    }
    this.plcesService.getNearest(this.hardcodedLocationLat, this.hardcodedLocationLon, amountToSkip).subscribe(places => {
      this.placesContext.addPlaces(places);
      this.visiblePlaces = this.placesContext.places;
    });
  }
  getNearestPlaces() {
    this.plcesService.getNearbyPlaces(this.hardcodedLocationLat, this.hardcodedLocationLon, this.distanceValue).subscribe(places => {
      this.placesContext.setPlaces(places);
      this.visiblePlaces = this.placesContext.places;
    });
  }

  logData() {
    console.log(this.distanceValue);
  }
  selectOnly(placeTag: PlaceTag) {
    this.visiblePlaces = [];
    if (this.currentPlaceTag !== undefined && placeTag === this.currentPlaceTag) {
      this.visiblePlaces = this.placesContext.places;
      return;
    }
    this.currentPlaceTag = placeTag;
    this.placesContext.places.forEach(element => {
      const placeTypes = element.placeTags;
      console.log(placeTypes);
      if (placeTypes.find(x => x === placeTag)) {
        console.log('place tag is true');

        this.visiblePlaces.push(element);
      }
    });
  }

  setFilteredItems() {
    this.visiblePlaces = this.filterItems(this.searchTerm);
  }

  filterItems(searchTerm) {

    return this.placesContext.places.filter((item) => {
      return item.name.toLowerCase().includes(searchTerm.toLowerCase());
    });
  }
  onChangeDistanceValue(value) {
    console.log('value changed');
  }
  setPlaceTypes() {
    this.plcesService.getPlaceTypes().subscribe(x => this.placeTypes = x);
  }
}

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { PlacesService } from '../places/services/places.service';
import { PopupCardPage } from '../popup-card/popup-card.page';
import { CheckInConnectionService } from '../services/CheckInConnection/check-in.service';
import { HomePage } from './home.page';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      }
    ])
  ],
  declarations: [HomePage],
  providers: [PlacesService, CheckInConnectionService],
  entryComponents: []

})
export class HomePageModule { }

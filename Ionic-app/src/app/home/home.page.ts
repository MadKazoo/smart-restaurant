import { Component, OnInit } from '@angular/core';
import { Environment, GoogleMap, GoogleMapOptions, GoogleMaps, GoogleMapsEvent, Marker } from '@ionic-native/google-maps/ngx';
import { NFC } from '@ionic-native/nfc/ngx';
import { AlertController, LoadingController, ModalController, NavController, Platform } from '@ionic/angular';
import { PlacesService } from '../places/services/places.service';
import { PopupCardPage } from '../popup-card/popup-card.page';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  private map: GoogleMap;
  private loading: HTMLIonLoadingElement;

  constructor(private platform: Platform, public alertController: AlertController,
    public loadingController: LoadingController,
    private placesService: PlacesService, private nfc: NFC, private modalController: ModalController, private navCtrl: NavController) {
  }

  async ngOnInit() {
  }
  
}

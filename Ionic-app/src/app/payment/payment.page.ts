import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CurrentOrderService } from '../services/Order/currentOrder.service';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.page.html',
  styleUrls: ['./payment.page.scss'],
})
export class PaymentPage implements OnInit {
  payForOrderString = "Pay for order";

  orderId: Number;
  constructor(private route: ActivatedRoute, private router: Router, private orderService: CurrentOrderService) { 
    this.orderId = Number.parseInt(this.route.snapshot.paramMap.get('id'));
  }
          
  ngOnInit() {
  }

  payForOrder(){
    console.log('paying for order');
    if(this.orderId!==undefined && this.orderId!==0){
    this.orderService.getOrderPaymentUrl(this.orderId).subscribe(x=>this.handleOrderPaymentUrl(x.toString()));
    }
  }

  private handleOrderPaymentUrl(url: string){
    this.router.navigate(['/tabs']);
  }
}

import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiEndpointsService {
  private webServiceUrl: string;

  constructor() {
    this.webServiceUrl = environment.webServiceUrl;
  }
  getPlaceMenuUrl(placeId) {
    return this.getPlaceBaseUrl() + '/' + placeId + '/Menu';
  }

  getMenuResourceUrl(placeId, menuItemId) {
    return this.getPlaceMenuUrl(placeId) + '/' + menuItemId;
  }

  getPlaceBaseUrl() {
    return this.webServiceUrl + '/Place';
  }

  getTablesUrl(placeId) {
    return this.getPlaceBaseUrl() + '/' + placeId + '/tables';
  }

  getPlaceTypesUrl() {
    return this.getPlaceBaseUrl() + '/types';
  }

  getPlaceUrl(placeId) {
    return this.getPlaceBaseUrl() + '/' + placeId;
  }

  getNearbyPlacesUrl(lat, lng, distance) {
    return this.getPlaceBaseUrl() + '/' + lat + '/' + lng + '/' + distance;
  }

  getNearestPlacesUrl(lat, lng, amountToSkip) {
    return this.getPlaceBaseUrl() + '/nearest/' + lat + '/' + lng + '/' + amountToSkip;
  }

  getPlacePropertyUrl(placeId, propertyName) {
    return this.getPlaceUrl(placeId) + '/' + propertyName;
  }
}

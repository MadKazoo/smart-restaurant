import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';

@Injectable()
export class HeaderHelper {
  constructor() {
  }
  canActivate() {
    const token = localStorage.getItem('jwt');

    if (token) {
      return true;
    }
    // this.router.navigate(['login']);
    return false;
  }

  createHeaderWithJWT(): HttpHeaders {
    const token = localStorage.getItem('jwt');
    const header = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    });

    return header;
  }

  createHeaderWithoutJWT(): HttpHeaders {
    const header = new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    });
    return header;
  }
}

import { TestBed, inject } from '@angular/core/testing';

import { ApiEndpointsService } from './api-endpoints.service';

describe('ApiEndpointsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApiEndpointsService]
    });
  });

  it('should be created', inject([ApiEndpointsService], (service: ApiEndpointsService) => {
    expect(service).toBeTruthy();
  }));
});

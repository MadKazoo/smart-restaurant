import { Injectable } from '@angular/core';
import { CheckInDto, ClientDto } from '../../model/checkInDto';
import { PartialOrder } from '../../model/partialOrder';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CheckInContextService {

  public checkInDto: CheckInDto;
  public checkedInUsers: ClientDto[] = [];
  public partialOrders: PartialOrder[] = [];
  public isReadyForOrder = false;

  constructor() {
  }
  setCheckIn(checkIn: CheckInDto) {
    if (checkIn === null) {
      return;
    }
    if (checkIn.checkedInUsers.length === 0) {
      this.checkedInUsers = null;
      this.checkInDto = null;
      this.partialOrders = null;
    }
    this.checkedInUsers = checkIn.checkedInUsers;
    this.checkInDto = checkIn;
    this.partialOrders = checkIn.partialOrders;
    if (this.containsAnyPartialOrders) {
      this.parseUsersOrders();
    }
  }

  private parseUsersOrders() {
    this.partialOrders.forEach(element => {
      element.userPartialOrders.forEach(userPartialOrder => {
        const user = this.checkedInUsers.find(x => x.id === userPartialOrder.appUserId);
        userPartialOrder.clientDto = user;
      });
    });
  }
  public containsAnyPartialOrders(): boolean {
    return (this.partialOrders !== null && this.partialOrders.length > 0);
  }

  public resetCheckIn() {
    this.checkInDto = null;
    this.checkedInUsers = [];
    this.partialOrders = [];
    this.isReadyForOrder = false;
  }
}

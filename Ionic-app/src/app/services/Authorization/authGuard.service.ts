import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { LoginService } from './login.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private loginService: LoginService, private router: Router) {
  }

  canLogIn: boolean;

  canActivate() {
    const token = localStorage.getItem('jwt');
    const login = localStorage.getItem('login');
    const password = localStorage.getItem('password');
    console.log(login + ' ' + password);

    if (login && password) {
      this.loginService.login(login, password);
    }
    const isLoginValid = this.loginService.isLoginValid;

    if (token && isLoginValid) {
      return true;
    }

    this.router.navigate(['login']);
    return false;
  }


}
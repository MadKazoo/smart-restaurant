import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HeaderHelper } from '../Helpers/headerHelper.service';
import { environment } from 'src/environments/environment';
import { Subscription, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { UserService } from '../Context/user.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  isLoginValid: boolean;
  loginPath = '/auth/login';
  registerPath = '/auth/register';

  constructor(private http: HttpClient, private httpHeaderHelper: HeaderHelper,
    private router: Router, private userService: UserService) { }

  login(login: string, password: string): Observable<any> {

    const headers = this.httpHeaderHelper.createHeaderWithoutJWT();
    const credentials = this.createCreditentials(login, password);
    const apiUrl = environment.webServiceUrl + this.loginPath;
    console.log('writing to ' +apiUrl);
    const sub = this.http.post(apiUrl, credentials, {
      headers: headers
    });

    sub.subscribe(response => {
      const token = (<any>response).token;
      console.log(response);
      const userId = (<any>response).id;
      console.log('id user: ' + userId);
      this.userService.setUserId(userId);
      localStorage.setItem('jwt', token);
      this.userService.setToken(token);

      // to remove if don't want to log in automatically
      localStorage.setItem('login', login);
      localStorage.setItem('password', password);

      this.isLoginValid = true;
      this.router.navigate(['tabs']);
    }, err => {
      console.log(err);
      this.isLoginValid = false;
    });

    return sub;
  }

  testJWT() {
    const headers = this.httpHeaderHelper.createHeaderWithJWT();
    const apiUrl = environment.webServiceUrl + '/auth/testJWT';
    this.http.get(apiUrl, {
      headers: headers
    }).subscribe(response => {
      console.log('JWT success - ' + response);
    }, err => {
      console.log('JWT fail = ' + err);
    });
  }
  register(login: string, password: string) {

    const headers = this.httpHeaderHelper.createHeaderWithJWT();
    const credentials = this.createCreditentials(login, password);
    const apiUrl = environment.webServiceUrl + this.registerPath;

    this.http.post(apiUrl, credentials, {
      headers: headers
    }).subscribe(response => {
      localStorage.setItem('login', login);
      localStorage.setItem('password', password);
    }, err => {
      this.isLoginValid = false;
    });
  }

  createCreditentials(login: string, password: string) {
    return {
      login: login,
      password: password
    };
  }
  logOut() {
    localStorage.removeItem('jwt');
  }
}

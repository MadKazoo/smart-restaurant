import { TestBed } from '@angular/core/testing';

import { CheckInConnectionAdapterService } from './check-in-connection-adapter.service';

describe('CheckInConnectionAdapterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CheckInConnectionAdapterService = TestBed.get(CheckInConnectionAdapterService);
    expect(service).toBeTruthy();
  });
});

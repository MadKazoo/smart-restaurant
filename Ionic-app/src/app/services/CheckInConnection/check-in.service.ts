import { Injectable } from '@angular/core';
import * as signalR from '@aspnet/signalr';
import { environment } from '../../../environments/environment';
import { CheckInViewModel } from '../../model/checkInViewModel';
import { Observable, Subscribable, Subject } from 'rxjs';
import { CheckInDto } from '../../model/checkInDto';
import { UserService } from '../Context/user.service';
import { MenuItem } from '../../model/menuItem';
import { PartialOrder } from '../../model/partialOrder';
import { SignalRServerConnectionService } from './signal-rserver-connection.service';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { OrderDto } from '../../model/orderDto';
import { CheckInContextService } from '../Context/check-in-context.service';

@Injectable({
  providedIn: 'root'
})
export class CheckInConnectionService {

  checkInViewModel: CheckInViewModel;
  checkInSubscription;
  checkInHubURL = 'CheckIn';
  qrCodeUrl = 'qr-code';
  webServiceUrl: string;
  constructor(private userService: UserService, private serverConnectionService: SignalRServerConnectionService,
    public checkinContext: CheckInContextService, private http: HttpClient) {
    this.webServiceUrl = environment.webServiceUrl;
  }

  public isUserCheckedIn() {
    if (this.checkinContext.checkInDto === undefined || this.checkinContext.checkInDto === null) {
      return false;
    }
    return true;
  }
  public getInfoFromHash(hash: string) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Accept', 'application/json');
    const placeInfo = this.http.get<CheckInDto>(this.webServiceUrl + '/Place/' +
      this.qrCodeUrl + '/' + hash, { headers, params: null });
    return placeInfo;
  }

  public getCurrentlyProcessedCheckIn(): Observable<CheckInDto> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    console.log('sending message to ' + this.webServiceUrl +
      '/checkIn/user/' + this.userService.getUserId());
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Accept', 'application/json');
    const checkIn = this.http.get<CheckInDto>(this.webServiceUrl +
      '/checkIn/user/' + this.userService.getUserId(), { headers, params: null });
    return checkIn;
  }

  public addItemToCheckInOrder(menuItem: MenuItem) {
    console.log('adding itme To checkInOrder ');


    this.serverConnectionService.connection
      .invoke('addItemToOrder', this.checkinContext.checkInDto.id, this.userService.getUserId(), menuItem);
  }

  setUserStatusToCheckIn(status: boolean) {
    console.log('sending user status');
    console.log(status);
    const setUserStatusToCheckIn = 'setUserStatusToCheckIn';

    console.log('setting user status');

    this.serverConnectionService.connection
      .invoke(setUserStatusToCheckIn, this.checkinContext.checkInDto.id, this.userService.getUserId(), status);
  }

  public CheckIn(placeId: number, TableNumber: number) {
    this.createCheckInViewModel(placeId, TableNumber);

    console.log(this.checkInViewModel);
    console.log('checking in');

    return this.serverConnectionService.connection.invoke('checkIn', this.checkInViewModel);
  }

  public resignFromPartialOrder(partialOrder: PartialOrder) {
    const resignFromPartialOrder = 'resignFromPartialOrder';
    this.serverConnectionService.connection.invoke(resignFromPartialOrder,
      this.checkinContext.checkInDto.id, partialOrder.id, this.userService.getUserId())
      .then(element => {
        console.log('success');
      },
        error => {
          this.serverConnectionService.createConnectionWithCheckIn();
        });

  }

  public subscribeToCheckIn() {
    const subscribeCheckIn = 'subscribeCheckIn';
    console.log('susbscribing to checkin. finding user');
    this.serverConnectionService.connection.on(subscribeCheckIn, data => {
      this.checkinContext.setCheckIn(data);
      console.log('got data from subscription to check in');
      console.log(data);
      if (this.isContainingCurrentUser()) {
        this.setUsersReadyForOrder();
      } else {
        this.checkinContext.resetCheckIn();
      }
    });
  }

  private isContainingCurrentUser() {
    if (this.checkinContext.checkInDto.checkedInUsers !== undefined &&
      this.checkinContext.checkInDto.checkedInUsers !== null &&
      this.checkinContext.checkInDto.checkedInUsers.length > 0 &&
      this.checkinContext.checkInDto.checkedInUsers.find(x => x.id === this.userService.getUserId())) {
      return true;
    }
    return false;
  }

  private setUsersReadyForOrder() {
    this.checkinContext.isReadyForOrder = this.checkinContext.checkInDto.checkedInUsers
      .find(x => x.id === this.userService.getUserId()).isReadyForOrder;
  }
  subscribeToNewOrder(): Observable<OrderDto> {
    const subscribeToNewOrder = 'subscribeToNewOrder';
    return new Observable<OrderDto>(observer =>
      this.serverConnectionService.connection.on(subscribeToNewOrder, data => {
        observer.next(data);
      }));
  }
  joinToPartialOrder(partialOrder: PartialOrder, moneyToPay: number) {
    console.log('joining partial order');
    this.serverConnectionService.connection.invoke('joinToPartialOrder',
      this.checkinContext.checkInDto.id, partialOrder.id, this.userService.getUserId(), moneyToPay)
      .then(element => {
        console.log('success');
      },
        error => { console.log(error); });
  }

  checkOut() {
    const checkOutMethod = 'checkOut';
    this.serverConnectionService.connection.invoke(checkOutMethod, this.checkinContext.checkInDto.id, this.userService.getUserId());
    this.resetConnection();
  }
  resetConnection() {
    this.checkinContext.checkInDto = null;
    this.checkinContext.partialOrders = null;
    this.checkinContext.checkedInUsers = null;
  }
  joinToTableByHash(hash: string) {

  }
  createCheckInViewModel(placeId: number, TableNumber: number) {
    this.checkInViewModel = new CheckInViewModel();
    this.checkInViewModel.ClientId = this.userService.getUserId();
    this.checkInViewModel.TableNumber = TableNumber;
    this.checkInViewModel.placeId = placeId;

  }

  public isUserReadyForOrder() {
    return this.checkinContext.isReadyForOrder;
  }
}

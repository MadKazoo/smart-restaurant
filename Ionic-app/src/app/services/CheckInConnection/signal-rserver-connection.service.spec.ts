import { TestBed } from '@angular/core/testing';

import { SignalRServerConnectionService } from './signal-rserver-connection.service';

describe('SignalRServerConnectionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SignalRServerConnectionService = TestBed.get(SignalRServerConnectionService);
    expect(service).toBeTruthy();
  });
});

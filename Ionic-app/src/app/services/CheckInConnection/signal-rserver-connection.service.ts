import { Injectable } from '@angular/core';
import * as signalR from '@aspnet/signalr';
import { Observable, Subscription } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UserService } from '../Context/user.service';


@Injectable({
  providedIn: 'root'
})
export class SignalRServerConnectionService {

  public connection: signalR.HubConnection;
  clientsHub = 'ClientsHub';
  webServiceUrl: string;
  startedConnection: any;
  isConnected: boolean;

  connectionSub: Subscription;
  connectionStatus: Observable<ConnectionStatusEnum>;
  isConnectionCreated: boolean;
  hasConnectionWithCHeckInStarted: boolean;
  constructor(private userService: UserService) {
    this.connectionSub = new Subscription();
    this.connectionStatus = new Observable(x => x.next(ConnectionStatusEnum.CONNECTED));
    this.hasConnectionWithCHeckInStarted = false;
    this.isConnected = false;
    this.isConnectionCreated = false;
  }

  public async createConnectionWithCheckIn(): Promise<void> {
    this.webServiceUrl = environment.webServiceUrl;
    console.log('starting connection with signalR web service');
    this.connection = new signalR.HubConnectionBuilder()
      .withUrl(this.webServiceUrl + '/' + this.clientsHub, { accessTokenFactory: () => this.userService.getToken() })
      .build();
    console.log('started connection with checkInHub');
    this.isConnectionCreated = true;
    this.connection.start();
  }

}
export enum ConnectionStatusEnum {
  NOTCONNECTED = 0,
  CONNECTED = 1,
  CLOSED = 2
}

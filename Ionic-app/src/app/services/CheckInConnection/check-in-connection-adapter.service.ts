import { Injectable } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { CheckInDto } from '../../model/checkInDto';
import { SignalRServerConnectionService } from './signal-rserver-connection.service';
import { CheckInConnectionService } from './check-in.service';
import { CheckInContextService } from '../Context/check-in-context.service';

@Injectable({
  providedIn: 'root'
})
export class CheckInConnectionAdapterService {
  public connection: signalR.HubConnection;
  checkInSubscription: Subscription;

  constructor(private checkInConnection: CheckInConnectionService,
    private checkInContext: CheckInContextService,
    private signalRConnectionService: SignalRServerConnectionService) {
  }

  public createSignalrConnection() {
    return this.signalRConnectionService.createConnectionWithCheckIn();
  }


  public checkIn(placeId: number, tableId: number) {
    if (placeId === null || tableId === null) {
      this.getAndSubscribeToCheckIn();
    } else {
      this.createNewCheckIn(placeId, tableId);
      return this.subscribeToCheckIn();
    }
  }

  public checkInAddingItmes(placeId, tableNumber, items) {
    this.createNewCheckIn(placeId, tableNumber)
      .then(async () => {
        await this.delay(300);
        items.forEach(item => this.addItemToCheckInOrder(item));
      });
  }

  private delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  public subscribeToCheckIn() {
    this.checkInConnection.subscribeToCheckIn();
  }

  public isUserCheckedIn(): boolean {
    if (this.checkInConnection.isUserCheckedIn()) {
      return true;
    }

    return false;
  }
  public getCheckIn() {
    return this.checkInConnection.getCurrentlyProcessedCheckIn();
  }

  public getAndSubscribeToCheckIn() {
    return this.checkInConnection.getCurrentlyProcessedCheckIn().subscribe(checkIn => {
      this.subscribeToCheckIn();
      if (checkIn !== undefined && checkIn !== null) {
        this.checkInContext.setCheckIn(checkIn);
        this.createNewCheckIn(checkIn.placeId, checkIn.tableId);
      }
    });
  }

  getInfoFromHash(hash: string) {
    return this.checkInConnection.getInfoFromHash(hash);
  }
  public checkOut() {
    return this.checkInConnection.checkOut();
  }

  public resignFromPartialOrder(partialOrder) {
    this.checkInConnection.resignFromPartialOrder(partialOrder);
  }
  public addItemToCheckInOrder(menuitem) {
    this.checkInConnection.addItemToCheckInOrder(menuitem);
  }

  public subscribeToNewOrder() {
    return this.checkInConnection.subscribeToNewOrder();
  }

  public setUserStatusToCheckIn(status: boolean) {
    this.checkInConnection.setUserStatusToCheckIn(status);
  }

  public joinToPartialOrder(partialOrder, moneyToPay) {
    this.checkInConnection.joinToPartialOrder(partialOrder, moneyToPay);
  }

  private createNewCheckIn(placeId: number, tableId: number) {
    return this.checkInConnection.CheckIn(placeId, tableId);
  }
}

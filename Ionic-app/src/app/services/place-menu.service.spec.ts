import { TestBed } from '@angular/core/testing';

import { PlaceMenuService } from './place-menu.service';

describe('PlaceMenuService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PlaceMenuService = TestBed.get(PlaceMenuService);
    expect(service).toBeTruthy();
  });
});

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { OrderDto, OrderStatus } from '../../model/orderDto';
import { UserService } from '../Context/user.service';
import { CurrentOrderContextService } from '../../current-order/current-order-context.service';
import { SignalRServerConnectionService } from '../CheckInConnection/signal-rserver-connection.service';



@Injectable({
  providedIn: 'root'
})
export class CurrentOrderService {
  invokeClientOrderSubscriptionMethod = 'invokeClientOrderSubscription';
  subscribeToOrderMethod = 'subscribeToOrder';
  webServiceUrl: string;
  hasOrdersChanged: Subject<boolean>;

  constructor(private userService: UserService, private serverConnectionService: SignalRServerConnectionService,
    public currentOrderContext: CurrentOrderContextService, private http: HttpClient) {
    this.currentOrderContext.orders = [];
    this.currentOrderContext.currentlyProceedOrders = [];
    this.webServiceUrl = environment.webServiceUrl;
    this.hasOrdersChanged = new Subject();
  }

  public getOrderPaymentUrl(orderId: Number) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    console.log('sending message to ' + this.webServiceUrl +
      '/Orders/user/' + this.userService.getUserId());
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Accept', 'application/json');
    const getOrderPaymentUrl = this.http.get(this.webServiceUrl +
      '/Orders/' + orderId + '/payment/' + this.userService.getUserId(), { headers: headers, params: null });
    return getOrderPaymentUrl;
  }

  public createNewSubscription(groupId: string) {
    this.invokeOrderSubscription(groupId);
    this.hasOrdersChanged.next(true);
  }

  public invokeOrderSubscription(orderGroupId: string) {
    return this.serverConnectionService
      .connection.invoke(this.invokeClientOrderSubscriptionMethod, orderGroupId);
  }
  public subscribeToOrders(): Observable<OrderDto> {
    {
      return new Observable<OrderDto>(observer =>
        this.serverConnectionService.connection.on(this.subscribeToOrderMethod, data => {
          this.shubscribeToOrder(data);
          observer.next(data);
        }));
    }
  }

  public invokeOrdersSubscriptions(orders: OrderDto[]) {
    this.currentOrderContext.orders = orders;
    orders.forEach(order => {
      if (order.orderStatus === OrderStatus.ACCEPTED || order.orderStatus === OrderStatus.NEW) {
        console.log('invoking subscription on ' + order.groupId);
        this.currentOrderContext.currentlyProceedOrders.push(order);
        this.invokeOrderSubscription(order.groupId);
      }
    });
  }

  public shubscribeToOrder(order: OrderDto) {
    const index = this.currentOrderContext.orders.findIndex(x => x.groupId === order.groupId);
    if (index !== -1) {
      this.currentOrderContext.orders[index] = (order);
    }
    this.currentOrderContext.currentlyProceedOrders.push(order);
  }

  public getUserOrders() {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    console.log('sending message to ' + this.webServiceUrl +
      '/Orders/user/' + this.userService.getUserId());

    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Accept', 'application/json');
    const placeObservable = this.http.get<OrderDto[]>(this.webServiceUrl +
      '/Orders/user/' + this.userService.getUserId(), { headers: headers, params: null });
    return placeObservable;
  }

  public isPayButtonVisible(order: OrderDto): boolean {
    if (order.orderStatus !== OrderStatus.ACCEPTED || order.userOrderStatuses === undefined || order.userOrderStatuses === null) {
      return false;
    }
    const hasPaid = order.userOrderStatuses.find(x => x.userId === this.userService.getUserId()).hasPaid;

    if (hasPaid === true) {
      return false;
    }

    return true;
  }

  public hasUserPaid(order: OrderDto): boolean {
    const hasPaid = order.userOrderStatuses.find(x => x.userId === this.userService.getUserId()).hasPaid;
    return hasPaid;
  }

  public getUserPaymentShare(order: OrderDto) {
    const paymentShare = order.userOrderStatuses.find(x => x.userId === this.userService.getUserId()).toPay;
    return paymentShare;
  }

  public getHasOrdersChangedSubject(): Subject<boolean> {
    return this.hasOrdersChanged;
  }
}

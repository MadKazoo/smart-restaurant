import { Injectable } from '@angular/core';
import { CurrentOrderService } from './currentOrder.service';

@Injectable({
  providedIn: 'root'
})
export class OrderConnectionAdapterService {

  constructor(private currentOrderService: CurrentOrderService) { }

  public createNewSubscription(groupId: string) {
    this.currentOrderService.createNewSubscription(groupId);
  } 

}

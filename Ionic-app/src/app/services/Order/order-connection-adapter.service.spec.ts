import { TestBed } from '@angular/core/testing';

import { OrderConnectionAdapterService } from './order-connection-adapter.service';

describe('OrderConnectionAdapterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OrderConnectionAdapterService = TestBed.get(OrderConnectionAdapterService);
    expect(service).toBeTruthy();
  });
});

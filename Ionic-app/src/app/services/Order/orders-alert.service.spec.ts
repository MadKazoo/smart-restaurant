import { TestBed } from '@angular/core/testing';

import { OrdersAlertLogicService } from './orders-alert.service';

describe('OrdersAlertService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OrdersAlertLogicService = TestBed.get(OrdersAlertLogicService);
    expect(service).toBeTruthy();
  });
});

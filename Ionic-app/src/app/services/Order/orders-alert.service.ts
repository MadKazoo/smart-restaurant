import { Injectable } from '@angular/core';
import { CurrentOrderContextService } from '../../current-order/current-order-context.service';
import { OrderDto, OrderStatus } from '../../model/orderDto';
import { UserService } from '../Context/user.service';

@Injectable({
  providedIn: 'root'
})
export class OrdersAlertLogicService {

  constructor(public orderContext: CurrentOrderContextService, private appUserService: UserService) {
  }

  getUnpaidOrders(orders: OrderDto[]) {
    const unpaidOrders: OrderDto[] = [];
    orders.forEach(order => order.userOrderStatuses.forEach(userOrderStatus => {
      if (userOrderStatus.userId === this.appUserService.getUserId()
        && userOrderStatus.hasPaid === false && order.orderStatus === OrderStatus.ACCEPTED) {
        unpaidOrders.push(order);
      }
    }));
    console.log('unpaid orders ' + unpaidOrders);
    return unpaidOrders;
  }

  getUnpaidOrdersFromOrder(order: OrderDto) {
    const unpaidOrders: OrderDto[] = [];
    order.userOrderStatuses.forEach(userOrderStatus => {
      if (userOrderStatus.userId === this.appUserService.getUserId() && userOrderStatus.hasPaid === false) {
        unpaidOrders.push(order);
      }
    });
    console.log('unpaid orders ' + unpaidOrders);
    return unpaidOrders;
  }
}

import { Component, OnInit, Input } from '@angular/core';
import { Place } from '../model/place';

@Component({
  selector: 'app-popup-card',
  templateUrl: './popup-card.page.html',
  styleUrls: ['./popup-card.page.scss'],
})
export class PopupCardPage implements OnInit {
  @Input() place: Place;

  constructor() { }

  ngOnInit() {
  }

}

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupCardPage } from './popup-card.page';

describe('PopupCardPage', () => {
  let component: PopupCardPage;
  let fixture: ComponentFixture<PopupCardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupCardPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupCardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

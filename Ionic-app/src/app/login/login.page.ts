import { Component, OnInit } from '@angular/core';
import { LoginService } from '../services/Authorization/login.service';
import { AuthGuard } from '../services/Authorization/authGuard.service';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  login: string;
  password: string;
  wrongLoginOrPasswordText = 'Wrong login or password';
  public hasLoginFailed = false;
  private webServiceUrl: string;

  isLoggingIn: boolean;
  constructor(private loginService: LoginService, private authGuardService: AuthGuard, private http: HttpClient) {
    this.hasLoginFailed = false;
    this.isLoggingIn = false;
    this.webServiceUrl = "https://smartrestaurant-webapi.azurewebsites.net";
    this.getAllPlaces().subscribe(x=>{console.log(x), error => console.log(error)});

    // this.authGuardService
  }

  public getAllPlaces(): Observable<any>{
    var headers = new HttpHeaders({'Content-Type': 'application/json'});
    
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Accept', 'application/json');
    var placeObservable = this.http.get<any>(this.webServiceUrl + '/Place', { headers: headers, params: null });
    return placeObservable;
  }
  ngOnInit() {
    this.hasLoginFailed = false;
  }
  logIn() {
    const sub = this.loginService.login(this.login, this.password);
    this.isLoggingIn = true;
    sub.subscribe(x => {
      this.isLoggingIn = false;
     },
      error => {
        console.log(error);
        this.isLoggingIn = false;
        this.hasLoginFailed = true;
      });
    // if(saveLogin){
    //  localStorage.setItem("login");
    //   localStorage.setItem("password");
    // }
  }
}

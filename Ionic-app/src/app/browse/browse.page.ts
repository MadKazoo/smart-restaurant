import { Component, OnInit } from '@angular/core';
import { LoginService } from '../services/Authorization/login.service';

@Component({
  selector: 'app-browse',
  templateUrl: './browse.page.html',
  styleUrls: ['./browse.page.scss'],
})
export class BrowsePage implements OnInit {
  constructor(private loginService: LoginService) {
  }
  ngOnInit() {
    console.log('browser on inint');
  }
  logIn() {
  }

  TestJWT() {
    this.loginService.testJWT();
  }

  removeJWT() {
    this.loginService.logOut();
  }
}

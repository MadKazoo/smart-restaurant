import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { BrowsePage } from './browse.page';
import { PlacesComponent } from '../places/places.component';
import { PlaceCardComponent } from '../places/place-card/place-card.component';
import { StatusBar } from '@ionic-native/status-bar/ngx';

const routes: Routes = [
  {
    path: '',
    component: BrowsePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  providers: [
    StatusBar
  ],
  declarations: [BrowsePage, PlaceCardComponent, PlacesComponent]
})
export class BrowsePageModule { }

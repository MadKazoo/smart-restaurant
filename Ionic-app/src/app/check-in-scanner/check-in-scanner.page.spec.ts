import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckInScannerPage } from './check-in-scanner.page';

describe('CheckInScannerPage', () => {
  let component: CheckInScannerPage;
  let fixture: ComponentFixture<CheckInScannerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckInScannerPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckInScannerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

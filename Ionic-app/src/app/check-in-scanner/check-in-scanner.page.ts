import { Component, OnInit } from '@angular/core';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { QrCodeScannerPage } from '../qr-code-scanner/qr-code-scanner.page';

@Component({
  selector: 'app-check-in-scanner',
  templateUrl: './check-in-scanner.page.html',
  styleUrls: ['./check-in-scanner.page.scss'],
})  
export class CheckInScannerPage implements OnInit {

  scanText = 'Scan';
  
  constructor(private qrScanner: QRScanner, public navCtrl: NavController, private router: Router) {
    
   }

   push() {
    this.navCtrl.navigateForward(['qr-code-scanner']);
  }
  qrscanner() {
    
    // Optionally request the permission early
    this.qrScanner.prepare()
      .then((status: QRScannerStatus) => {
        if (status.authorized) {
          // camera permission was grante

          this.qrScanner.resumePreview();

          // show camera preview
          this.qrScanner.show()
          .then((data : QRScannerStatus)=> { 
            alert(data.showing);
          },err => {
            alert(err);

          });

          // wait for user to scan something, then the observable callback will be called

        } else if (status.denied) {
          alert('Please allow use of camera');
          // camera permission was permanently denied
          // you must use QRScanner.openSettings() method to guide the user to the settings page
          // then they can grant the permission from there
        } else {
          // permission was denied, but not permanently. You can ask for permission again at a later time.
          alert('else');
        }
      })
      .catch((e: any) => {
        alert('Error is' + e);
      });

  }
    ngOnInit() {
  }


}

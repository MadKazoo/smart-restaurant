import { Component, OnInit } from '@angular/core';
import { CurrentOrderContextService } from '../current-order/current-order-context.service';
import { CurrentOrderService } from '../services/Order/currentOrder.service';
import { OrderDto } from '../model/orderDto';
import { AlertFactory } from '../places/check-in/alertFactory.service';
import { CheckInConnectionAdapterService } from '../services/CheckInConnection/check-in-connection-adapter.service';
import { OrdersAlertLogicService } from '../services/Order/orders-alert.service';

@Component({
  selector: 'app-order-alert',
  templateUrl: './order-alert.component.html',
  styleUrls: ['./order-alert.component.scss']
})
export class OrderAlertComponent implements OnInit {

  constructor(private orderAlertLogic: OrdersAlertLogicService, private ordersContext: CurrentOrderContextService,
    private connectionService: CheckInConnectionAdapterService,
     private currentOrderService: CurrentOrderService, private alertFactory: AlertFactory) {
    this.currentOrderService.getHasOrdersChangedSubject().subscribe(x => {
      const unpaidOrders = this.orderAlertLogic.getUnpaidOrders(ordersContext.orders);
      this.presentAlertWithUnpaidOrders(unpaidOrders);
    });

    connectionService.createSignalrConnection().then(x => {
      this.currentOrderService.subscribeToOrders();

      currentOrderService.getUserOrders().subscribe(orders => {
        this.currentOrderService.invokeOrdersSubscriptions(orders);
        const unpaidOrders = this.orderAlertLogic.getUnpaidOrders(orders);
        this.presentAlertWithUnpaidOrders(unpaidOrders);
      });
    });


  }

  async getOrderAlert(amountOfOrders) {
    if (amountOfOrders > 1) {
      const alert = await this.alertFactory.presentAlertyWithMultipleOrdersToBePaidNotification();
      await alert.present();
    } else if (amountOfOrders === 1) {
      const alert = null;
    }
  }

  async presentAlertWithUnpaidOrders(orders: OrderDto[]) {
    if (orders.length > 1) {
      const alert = await this.alertFactory.presentAlertyWithMultipleOrdersToBePaidNotification();
      await alert.present();
    } else if (orders.length === 1) {
      const alert = await this.alertFactory.presentAlertyWithSingleOrderToBePaid(orders[0]);
      await alert.present();
    }
  }
  ngOnInit() {
  }

}

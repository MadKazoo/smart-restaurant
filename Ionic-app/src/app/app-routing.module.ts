import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './services/Authorization/authGuard.service';

const routes: Routes = [
  { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' , canActivate: [AuthGuard] },
   { path: 'login', loadChildren: './login/login.module#LoginPageModule'},
  { path: 'check-in-scanner', loadChildren: './check-in-scanner/check-in-scanner.module#CheckInScannerPageModule' },
  { path: 'qr-code-scanner', loadChildren: './qr-code-scanner/qr-code-scanner.module#QrCodeScannerPageModule' },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}

import { Component, OnInit } from '@angular/core';
import { NavController, Platform } from '@ionic/angular';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';
import { CheckInConnectionAdapterService } from '../services/CheckInConnection/check-in-connection-adapter.service';

@Component({
  selector: 'app-qr-code-scanner',
  templateUrl: './qr-code-scanner.page.html',
  styleUrls: ['./qr-code-scanner.page.scss'],
})
export class QrCodeScannerPage implements OnInit {

  constructor(public navCtrl: NavController,
    public platform: Platform,
    public qrScanner: QRScanner, private checkInConnectionAdapter: CheckInConnectionAdapterService) {

    platform.ready().then(() => {
      this.qrscanner();
    });

  }

  ngOnInit() {
  }

  qrscanner() {
    // Optionally request the permission early
    this.qrScanner.prepare()
      .then((status: QRScannerStatus) => {
        if (status.authorized) {
          // camera permission was granted

          // start scanning
          let scanSub = this.qrScanner.scan().subscribe((text: string) => {

            console.log('Scanned something', text);
            // this.checkInConnectionAdapter.checkIn()
            this.checkInConnectionAdapter.getInfoFromHash(text).subscribe(placeInfo => {
              this.navCtrl.navigateForward('')

              this.navCtrl.navigateForward(['/tabs/browse/place-menu/' + placeInfo.placeId]).then(x=>
                this.checkInConnectionAdapter.checkIn(placeInfo.placeId, placeInfo.tableId));
              });

            this.qrScanner.hide(); // hide camera preview
            scanSub.unsubscribe(); // stop scanning
            this.navCtrl.pop();
          });

          this.qrScanner.resumePreview();

          // show camera preview
          this.qrScanner.show()
            .then((data: QRScannerStatus) => {
              alert(data.showing);
            }, err => {
              alert(err);

            });

          // wait for user to scan something, then the observable callback will be called

        } else if (status.denied) {
          alert('denied');
          // camera permission was permanently denied
          // you must use QRScanner.openSettings() method to guide the user to the settings page
          // then they can grant the permission from there
        } else {
          // permission was denied, but not permanently. You can ask for permission again at a later time.
          alert('else');
        }
      })
      .catch((e: any) => {
        alert('Error is' + e);
      });
  }


  private validateText(text: string): boolean {
    // TODO - validate 
    return true;
  }
}

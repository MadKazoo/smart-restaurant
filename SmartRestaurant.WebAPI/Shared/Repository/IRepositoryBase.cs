using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Shared.Repository
{
    public interface IRepositoryBase<T> where T : class
    {
        void SaveChanges();
        void Add(T entity);
        void Delete(Expression<Func<T, bool>> where);
        void Delete(T entity);
        T Get(Expression<Func<T, bool>> where);
        IEnumerable<T> GetAll();
        IQueryable<T> GetAllAsQueryable();
        T GetById(byte id);
        T GetById(Guid id);
        T GetById(int id);
        IEnumerable<T> GetMany(Expression<Func<T, bool>> where);
        IQueryable<T> GetManyAsQueryable(Expression<Func<T, bool>> where);
        void Update(T entity);
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using Places.Model.Entity;
using Shared.Helpers;
using Shared.Interfaces;

namespace Shared.PartialOrder
{
    public class PartialOrderLogic : IPartialOrderLogic
    {
        private readonly IBillSplittingLogic _billSplittingLogic;

        public PartialOrderLogic(IBillSplittingLogic billSplittingLogic)
        {
            _billSplittingLogic = billSplittingLogic;
        }

        public void RemoveUserFromPartialOrder(Places.Model.Entity.PartialOrder partialOrder,
            CheckInEntity checkInEntity, long userId)
        {
            if (partialOrder.UserPartialOrders.Count() == 1)
                DeletePartialOrder(checkInEntity, partialOrder);
            else
                RemoveUserFromPartialOrder(partialOrder, userId);
        }

        public void AddItemToPartialOrder(CheckInEntity checkIn, Places.Model.Entity.PartialOrder partialOrder)
        {
            if (checkIn.PartialOrders == null)
            {
                checkIn.PartialOrders = new List<Places.Model.Entity.PartialOrder> {partialOrder};
            }
            else
            {
                var checkInOrderList = checkIn.PartialOrders.ToList();
                checkInOrderList.Add(partialOrder);
                checkIn.PartialOrders = checkInOrderList;
            }
        }

        public void AddUserToPartialOrder(Places.Model.Entity.PartialOrder partialOrder, long userId,
            decimal moneyToPay)
        {
            List<UserPartialOrder> list = partialOrder.UserPartialOrders.ToList();

            var userPartialOrder = new UserPartialOrder
            {
                AppUserId = userId
            };

            list.Add(userPartialOrder);
            
            var user = list.FirstOrDefault(x => x.AppUserId == userId);


            _billSplittingLogic.SetUserPercentShareInOrder(list, user, moneyToPay, partialOrder.MenuItem.Price);

            partialOrder.UserPartialOrders = list;
        }

        public void SetCheckInPartialOrderStatus(CheckInEntity checkIn, long userId, bool isClientReady)
        {
            var status = isClientReady ? UserCheckInStatusEnum.READY : UserCheckInStatusEnum.NEW;

            var userOrderStatus = checkIn.UsersReadyForOrder.FirstOrDefault(x => x.AppUserEntity.Id == userId);

            if (userOrderStatus == null)
                throw new ArgumentNullException("Can't set User Order Status, it does not exist");

            userOrderStatus.Status = status;
        }

        public void DeletePartialOrder(CheckInEntity checkIn, Places.Model.Entity.PartialOrder partialOrder)
        {
            var partialOrders = checkIn.PartialOrders.ToList();
            partialOrders.Remove(partialOrder);
            checkIn.PartialOrders = partialOrders;
        }

        public void RemoveUserFromPartialOrder(Places.Model.Entity.PartialOrder partialOrder, long userId)
        {
            var list = partialOrder.UserPartialOrders.ToList();
            var userPartialOrder = list.FirstOrDefault(x => x.AppUserId == userId);


            if (userPartialOrder == null)
                throw new ArgumentNullException("Can't remove user partial order, it does not exist");

            var userNegativeShare = userPartialOrder.ClientPercentShare * -1;
            list.Remove(list.FirstOrDefault(x => x.AppUserId == userId));


            _billSplittingLogic.SetUsersPercentShareInOrderAfterUserLeave(list, userNegativeShare, partialOrder.MenuItem.Price);

            partialOrder.UserPartialOrders = list;
        }

        public void SetUsersPercentShareInOrder(Places.Model.Entity.PartialOrder partialOrder, long userId,
            decimal clientMoneyShare)
        {
            var list = partialOrder.UserPartialOrders.ToList();
            
            var userToSetPercentShare = list.First(x => x.AppUserId == userId);
            _billSplittingLogic.SetUserPercentShareInOrder(list, userToSetPercentShare, clientMoneyShare, partialOrder.MenuItem.Price);

            partialOrder.UserPartialOrders = list;
        }
    }
}
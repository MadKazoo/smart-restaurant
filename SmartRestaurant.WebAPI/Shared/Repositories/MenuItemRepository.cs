using Persistence;
using Places.Model.Entity;
using Shared.Interfaces;
using Shared.Repository;

namespace Shared.Repositories
{
    public class MenuItemRepository : RepositoryBase<MenuItemEntity>, IMenuItemRepository
    {
        public MenuItemRepository(SmartRestaurantDbContext databaseContext) : base(databaseContext)
        {
        }
    }
}
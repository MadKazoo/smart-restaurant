using System;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using Persistence;
using Places.Model.Entity;
using Shared.Interfaces;
using Shared.Repository;

namespace Shared.Repositories
{
    public class OrderRepository : RepositoryBase<OrderEntity>, IOrderRepository
    {
        public OrderRepository(SmartRestaurantDbContext databaseContext) : base(databaseContext)
        {
        }

        public void AddOrder(OrderEntity orderEntity)
        {
            base.Add(orderEntity);
        }

        public OrderEntity GetOrderWithUserStatuses(int orderId, long userId)
        {
            var order = base.GetManyAsQueryable(x => x.Id == orderId)
                .Include(x => x.Place)
                .Include(x => x.UserOrderStatuses).FirstOrDefault();
            return order;
        }

        public IQueryable<OrderEntity> GetOrdersWithFilter(Expression<Func<OrderEntity, bool>> filter)
        {
            return base.GetManyAsQueryable(filter)
                .Include(x => x.Table)
                .Include(x => x.Place)
                .Include(x => x.PartialOrders)
                .ThenInclude(y => y.MenuItem)
                .Include(x => x.PartialOrders)
                .ThenInclude(x => x.UserPartialOrders)
                .Include(x => x.UserOrderStatuses);
        }
    }
}
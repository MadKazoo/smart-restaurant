using System.Linq;
using Microsoft.EntityFrameworkCore;
using Persistence;
using Places.Model.Entity;
using Shared.Interfaces;
using Shared.Repository;

namespace Shared.Repositories
{
    public class PlaceMenuRepository : RepositoryBase<PlaceMenuEntity>, IPlaceMenuRepository
    {
        public PlaceMenuRepository(SmartRestaurantDbContext databaseContext) : base(databaseContext)
        {
        }

        public PlaceMenuEntity GetPlaceMenuByPlaceId(int placeId)
        {
            return base.GetManyAsQueryable(x => x.PlaceId == placeId)
                .Include(menu => menu.MenuItems).First();
        }
    }
}
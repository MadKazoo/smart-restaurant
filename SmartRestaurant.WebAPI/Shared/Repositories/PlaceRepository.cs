using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using Persistence;
using Places.Model.Entity;
using Shared.Interfaces;
using Shared.Repository;

namespace Shared.Repositories
{
    public sealed class PlaceRepository : RepositoryBase<PlaceEntity>, IPlaceRepository
    {
        public PlaceRepository()
        {
        }
        public PlaceRepository(SmartRestaurantDbContext databaseContext) : base(databaseContext)
        {
        }

        public PlaceEntity GetWithTables(int id)
        {
            return _dbSet.Where(x => x.Id == id)
                .Include(z => z.Tables)
                .Include(y => y.PlaceTypes)
                .FirstOrDefault();
        }

        public PlaceEntity GetWithMenu(Expression<Func<PlaceEntity, bool>> where)
        {
            return _dbSet.Where(where).Include(x => x.PlaceMenuEntity)
                .ThenInclude(x => x.MenuItems).FirstOrDefault();
        }

        public PlaceEntity Get(Expression<Func<PlaceEntity, bool>> where)
        {
            return _dbSet.Where(where).Include(x => x.PlaceTypes).FirstOrDefault();
        }

        public IQueryable<PlaceEntity> GetIncludingEverything(List<int> ids)
        {
            return base.GetManyAsQueryable(x=>ids.Contains(x.Id))
                .Include(x=>x.Location)
                .Include(x => x.Tables)
                .Include(x => x.PlaceTypes)
                    .ThenInclude(y=>y.CategoryType)
                .Include(x => x.PlaceMenuEntity)
                    .ThenInclude(y => y.MenuItems);
        }

        public IEnumerable<PlaceEntity> GetAllIncludingEverything()
        {
            return base._dbSet
                .Include(x=>x.Location)
                .Include(x => x.Tables)
                .Include(x => x.PlaceTypes)
                    .ThenInclude(y=>y.CategoryType)
                .Include(x => x.PlaceMenuEntity)
                .ThenInclude(y => y.MenuItems);
        }
        public PlaceEntity GetPlaceWithMenu(long placeId)
        {
            return base.Get(x => x.Id == placeId);
        }
    }
}
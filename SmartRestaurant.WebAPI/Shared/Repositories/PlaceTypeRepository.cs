using Persistence;
using Places.Model.Entity;
using Shared.Interfaces;
using Shared.Repository;

namespace Shared.Repositories
{
    public class PlaceTypeRepository : RepositoryBase<CategoryType>, IPlaceTypeRepository
    {
        public PlaceTypeRepository(SmartRestaurantDbContext databaseContext) : base(databaseContext)
        {
        }
    }
}
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Shared.Interfaces;
using Shared.PartialOrder;
using Shared.Repositories;
using Shared.Repository;

namespace Shared.Helpers
{
    public static class SharedModuleExtensions
    {
        public static void RegisterSharedModuleDependencies(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddTransient<IBillSplittingLogic, BillSplittingLogic>();
            services.AddTransient<IMd5Hashing, Md5Hashing>();
            services.AddTransient<IMenuItemRepository, MenuItemRepository>();
            services.AddTransient<IOrderRepository, OrderRepository>();
            services.AddTransient<IPartialOrderLogic, PartialOrderLogic>();
            services.AddTransient<IPlaceMenuRepository, PlaceMenuRepository>();
            services.AddTransient<IPlaceRepository, PlaceRepository>();
            services.AddTransient<IPlaceTypeRepository, PlaceTypeRepository>();
        }
    }
}
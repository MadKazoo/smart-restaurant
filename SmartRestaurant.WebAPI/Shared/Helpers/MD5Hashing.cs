using System;
using System.Security.Cryptography;
using System.Text;
using Shared.Interfaces;

namespace Shared.Helpers
{
    public class Md5Hashing : IMd5Hashing
    {
        public string CreateHash(string input)
        {
            var sBuilder = new StringBuilder();

            using (var md5Hash = MD5.Create())
            {
                var saltLength = 8;

                // Convert the input string to a byte array and compute the hash.
                var data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input + GetSalt(saltLength)));

                // Create a new Stringbuilder to collect the bytes
                // and create a string.


                // Loop through each byte of the hashed data 
                // and format each one as a hexadecimal string.
                for (var i = 0; i < data.Length; i++) sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        public bool VerifyHash(MD5 md5Hash, string input, string hash)
        {
            // Hash the input.
            var hashOfInput = CreateHash(input);

            // Create a StringComparer an compare the hashes.
            var comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash)) return true;

            return false;
        }

        private static byte[] GetSalt(int maximumSaltLength)
        {
            var salt = new byte[maximumSaltLength];
            using (var random = new RNGCryptoServiceProvider())
            {
                random.GetNonZeroBytes(salt);
            }

            return salt;
        }
    }
}
using System;

namespace Shared.Helpers
{
    public class MoneySplittingHelper
    {
        private decimal _Money;

        private decimal _PercentShare;
        public decimal ItemPrice { get; set; }

        public decimal Money
        {
            get => _Money;

            set
            {
                _Money = Math.Round(value, 2);
                if (ItemPrice != 0) _PercentShare = CalculateMoneyFromPercentShare(_Money);
            }
        }

        public decimal PercentShare
        {
            get => _PercentShare;

            set
            {
                _PercentShare = Math.Round(value, 2);
                if (ItemPrice != 0) _Money = CalculateMoneyFromPercentShare(_PercentShare);
            }
        }

        public decimal CalculateMoneyFromPercentShare(decimal percentShare)
        {
            return Math.Round(percentShare / ItemPrice * 100, 2);
        }
    }
}
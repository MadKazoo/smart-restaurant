using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Places.Model.Entity;
using Remotion.Linq.Parsing;
using Shared.Interfaces;

namespace Shared.Helpers
{
    public class BillSplittingLogic : IBillSplittingLogic
    {
        public void SetUsersPercentShareInOrderAfterUserLeave(List<UserPartialOrder> userPartialOrders, decimal leavingUserPercentShare,
            decimal menuItemPrice)
        {
            var amountOfClients = userPartialOrders.Count;  // Count on List has O(1)
            ValidateAmountOfClients(amountOfClients);
            
            if (amountOfClients == 1)
            {
                var fullPercentShare = 100;
                userPartialOrders.First().SetClientMenuItemPercentShare(menuItemPrice, fullPercentShare);
                return;
            }
            
            var amountOfClientsWithoutLeavingOne = amountOfClients - 1;
            
            var percentToSubtractFromExisting = leavingUserPercentShare / amountOfClientsWithoutLeavingOne;
            userPartialOrders.ForEach(x =>
            {
                var toPay = x.ClientPercentShare - percentToSubtractFromExisting;
                toPay = toPay < 0 ? 0 : toPay;
                x.SetToPayInMoney(menuItemPrice, toPay);
            });
        }
        public void SetUserPercentShareInOrder(List<UserPartialOrder> userPartialOrders, UserPartialOrder userPartialOrder,
            decimal moneyToPay, decimal itemPrice)
        {
            if (userPartialOrder.ToPayInMoney == moneyToPay) return;
            if (userPartialOrder == null) throw new NullReferenceException();

            var previousToPayInMoney = userPartialOrder.ToPayInMoney;
            userPartialOrder.SetToPayInMoney(itemPrice, moneyToPay);

            var amountOfClients = userPartialOrders.Count - 1;

            if (IsPennyMissing(itemPrice, amountOfClients))
            { 
                AddPennyToFirstUser(userPartialOrders, itemPrice);
            }

            var delta = moneyToPay;
            if (moneyToPay != previousToPayInMoney) delta = moneyToPay - previousToPayInMoney;

            var moneyToSubtract = delta / amountOfClients;

            userPartialOrders.ForEach(x =>
            {
                if (x.AppUserId != userPartialOrder.AppUserId)
                {
                    var toPay = x.ToPayInMoney - moneyToSubtract;

                    var moneySplittingHelp = new MoneySplittingHelper
                    {
                        ItemPrice = itemPrice, Money = toPay
                    };

                    toPay = toPay < 0 ? 0 : moneySplittingHelp.Money;
                    x.SetToPayInMoney(itemPrice, toPay);
                }
            });

            ValidatePercentage(userPartialOrders);
            ValidateCost(userPartialOrders, itemPrice);
        }

        private void AddPennyToFirstUser(List<UserPartialOrder> userPartialOrders, decimal itemPrice)
        {
            var sum = userPartialOrders.Sum(x => x.ToPayInMoney);

            if (sum + 0.01m == itemPrice)
            {
                var userToAddRest = userPartialOrders.First();
                userToAddRest.SetToPayInMoney(itemPrice, userToAddRest.ToPayInMoney + 0.01m);
            }
            else if (sum - 0.01m == itemPrice)
            {
                var userToAddRest = userPartialOrders.First();
                userToAddRest.SetToPayInMoney(itemPrice, userToAddRest.ToPayInMoney - 0.01m);
            }
        }
        
        private bool IsPennyMissing(decimal itemPrice, decimal amountOfClients) =>
            itemPrice / (amountOfClients + 1) % 2 != 0;

        private void ValidateAmountOfClients(int amountOfClients)
        {
            if (amountOfClients == 0)
            {
                throw new ArgumentException();
            }
        }
        private void ValidatePercentage(List<UserPartialOrder> userPartialOrders)
        {
            var sum = userPartialOrders.Sum(x => x.ClientPercentShare);
            if (sum != 100) throw new InvalidDataException("Sum of partial order has to be 100");
        }


        private void ValidateCost(List<UserPartialOrder> userPartialOrders, decimal menuItemCost)
        {
            var sum = userPartialOrders.Sum(x => x.ToPayInMoney);
            if (sum != menuItemCost)
            {
                if (sum + 0.01m == menuItemCost)
                {
                    var userToAddRest = userPartialOrders.First();
                    userToAddRest.SetToPayInMoney(menuItemCost, userToAddRest.ToPayInMoney + 0.01m);
                }
                else if (sum - 0.01m == menuItemCost)
                {
                    var userToAddRest = userPartialOrders.First();
                    userToAddRest.SetToPayInMoney(menuItemCost, userToAddRest.ToPayInMoney - 0.01m);
                }
                else
                {
                    throw new InvalidDataException("Sum of partial order has to be of menuItemCost");
                }
            }
        }
    }
}
using System.Collections.Generic;
using Places.Model.Entity;

namespace Shared.Interfaces
{
    public interface IBillSplittingLogic
    {
        void SetUsersPercentShareInOrderAfterUserLeave(List<UserPartialOrder> userPartialOrders, decimal leavingUserPercentShare,
            decimal menuItemPrice);

        void SetUserPercentShareInOrder(List<UserPartialOrder> userPartialOrders, UserPartialOrder userToSetPercentShare,
            decimal clientPercentShare, decimal itemPrice);
    }
}
using System;
using System.Linq;
using System.Linq.Expressions;
using Places.Model.Entity;
using Shared.Repository;

namespace Shared.Interfaces
{
    public interface IOrderRepository : IRepositoryBase<OrderEntity>
    {
        void AddOrder(OrderEntity orderEntity);
        OrderEntity GetOrderWithUserStatuses(int orderId, long userId);

        IQueryable<OrderEntity> GetOrdersWithFilter(Expression<Func<OrderEntity, bool>> filter);
    }
}
using Places.Model.Entity;
using Shared.Repository;

namespace Shared.Interfaces
{
    public interface IMenuItemRepository : IRepositoryBase<MenuItemEntity>
    {
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Places.Model.Entity;
using Shared.Repository;

namespace Shared.Interfaces
{
    public interface IPlaceRepository : IRepositoryBase<PlaceEntity>
    {
        PlaceEntity GetWithTables(int id);

        PlaceEntity GetWithMenu(Expression<Func<PlaceEntity, bool>> where);
        IQueryable<PlaceEntity> GetIncludingEverything(List<int> ids);
        IEnumerable<PlaceEntity> GetAllIncludingEverything();
    }
}
using Places.Model.Entity;

namespace Shared.Interfaces
{
    public interface IPartialOrderLogic
    {
        void AddItemToPartialOrder(CheckInEntity checkIn, Places.Model.Entity.PartialOrder partialOrder);

        void AddUserToPartialOrder(Places.Model.Entity.PartialOrder partialOrder, long userId,
            decimal clientPercentShare);

        void DeletePartialOrder(CheckInEntity checkIn, Places.Model.Entity.PartialOrder partialOrder);
        void RemoveUserFromPartialOrder(Places.Model.Entity.PartialOrder partialOrder, long userId);
        void SetCheckInPartialOrderStatus(CheckInEntity checkIn, long userId, bool isClientReady);

        void SetUsersPercentShareInOrder(Places.Model.Entity.PartialOrder partialOrder, long userId,
            decimal clientMoneyShare);

        void RemoveUserFromPartialOrder(Places.Model.Entity.PartialOrder partialOrder, CheckInEntity checkInEntity,
            long userId);
    }
}
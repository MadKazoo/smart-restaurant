using System.Security.Cryptography;

namespace Shared.Interfaces
{
    public interface IMd5Hashing
    {
        string CreateHash(string input);
        bool VerifyHash(MD5 md5Hash, string input, string hash);
    }
}
using Places.Model.Entity;
using Shared.Repository;

namespace Shared.Interfaces
{
    public interface IPlaceMenuRepository : IRepositoryBase<PlaceMenuEntity>
    {
        PlaceMenuEntity GetPlaceMenuByPlaceId(int placeId);
    }
}
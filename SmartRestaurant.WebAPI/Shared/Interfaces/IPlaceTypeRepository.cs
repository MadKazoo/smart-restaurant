using Places.Model.Entity;
using Shared.Repository;

namespace Shared.Interfaces
{
    public interface IPlaceTypeRepository : IRepositoryBase<CategoryType>
    {
    }
}
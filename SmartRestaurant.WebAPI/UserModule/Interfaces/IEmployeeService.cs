using Places.Model.Domain;
using Places.Model.Entity;

namespace UserModule.Interfaces
{
    public interface IEmployeeService
    {
        Employee Authorize(CredentialsViewModel userCreditentials);
    }
}
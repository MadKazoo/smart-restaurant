using Places.Model.Entity;
using Shared.Repository;

namespace UserModule.Interfaces
{
    public interface IEmployeeRepository : IRepositoryBase<Employee>
    {
    }
}
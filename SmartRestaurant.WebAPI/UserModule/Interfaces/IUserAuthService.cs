using Places.Model.Domain;
using Places.Model.Entity;

namespace UserModule.Interfaces
{
    public interface IUserAuthService
    {
        void RegisterUser(RegistrationViewModel registrationViewModel);

        AppUserEntity Authorize(CredentialsViewModel model);
    }
}
using System.Collections.Generic;
using Places.Model.Entity;
using Shared.Repository;

namespace UserModule.Interfaces
{
    public interface IPlaceEmployeeRepository : IRepositoryBase<PlaceEmployee>
    {
        IEnumerable<PlaceEntity> GetEmployeePlacesIncludingPlaceInfo(int employeeId);

        IEnumerable<int> GetEmployeePlaceIds(int employeeId);
    }
}
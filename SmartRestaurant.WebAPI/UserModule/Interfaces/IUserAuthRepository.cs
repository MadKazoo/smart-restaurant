using Places.Model.Entity;
using Shared.Repository;

namespace UserModule.Interfaces
{
    public interface IUserAuthRepository : IRepositoryBase<AppUserEntity>
    {
        AppUserEntity CheckPassword(string name, string password);
    }
}
using Places.Model.Domain;
using Places.Model.Entity;
using UserModule.Helpers;
using UserModule.Interfaces;
using IUserAuthRepository = UserModule.Interfaces.IUserAuthRepository;

namespace UserModule.Services
{
    public class UserAuthService : IUserAuthService
    {
        private readonly IUserAuthRepository _userAuthRepository;

        public UserAuthService(IUserAuthRepository userAuthRepository)
        {
            _userAuthRepository = userAuthRepository;
        }

        public void RegisterUser(RegistrationViewModel registrationViewModel)
        {
            var user = UserFactory.CreateUser(registrationViewModel.Name, registrationViewModel.Password);

            _userAuthRepository.Add(user);
            _userAuthRepository.SaveChanges();
        }

        public AppUserEntity Authorize(CredentialsViewModel userCreditentials)
        {
            var user = _userAuthRepository.CheckPassword(userCreditentials.Login, userCreditentials.Password);
            return user;
        }
    }
}
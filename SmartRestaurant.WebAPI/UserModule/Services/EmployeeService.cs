using Places.Model.Domain;
using Places.Model.Entity;
using UserModule.Interfaces;

namespace UserModule.Services
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IEmployeeRepository _employeeRepository;

        public EmployeeService(IEmployeeRepository employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        public Employee Authorize(CredentialsViewModel userCreditentials)
        {
            var user = _employeeRepository.Get(x =>
                x.Login.Equals(userCreditentials.Login) && x.Password.Equals(userCreditentials.Password));
            return user;
        }
    }
}
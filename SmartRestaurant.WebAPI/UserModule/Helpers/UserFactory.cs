using Places.Model.Entity;

namespace UserModule.Helpers
{
    public static class UserFactory
    {
        public static AppUserEntity CreateUser(string name, string password)
        {
            var user = new AppUserEntity
            {
                Login = name,
                Password = password
            };
            return user;
        }
    }
}
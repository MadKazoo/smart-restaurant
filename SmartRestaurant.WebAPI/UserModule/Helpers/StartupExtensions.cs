using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using UserModule.Interfaces;
using UserModule.Repositories;
using UserModule.Services;

namespace UserModule.Helpers
{
    public static class StartupExtensions
    {
        public static void RegisterUserModuleDependencies(this IServiceCollection services,
                 IConfiguration configuration)
             {
                 services.AddTransient<IUserAuthRepository, UserAuthRepository>();
                 services.AddTransient<IUserAuthService, UserAuthService>();
                 services.AddTransient<IEmployeeService, EmployeeService>();
                 services.AddTransient<IEmployeeRepository, EmployeeRepository>();
                 services.AddTransient<IPlaceEmployeeRepository, PlaceEmployeeRepository>();
                 
             }
         }
}
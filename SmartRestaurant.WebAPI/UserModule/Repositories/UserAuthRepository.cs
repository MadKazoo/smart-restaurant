using Persistence;
using Places.Model.Entity;
using Shared.Repository;
using UserModule.Interfaces;

namespace UserModule.Repositories
{
    public class UserAuthRepository : RepositoryBase<AppUserEntity>, IUserAuthRepository
    {
        public UserAuthRepository(SmartRestaurantDbContext databaseContext) : base(databaseContext)
        {
        }

        public AppUserEntity CheckPassword(string name, string password)
        {
            // var users = this.GetAll();
            var user = Get(x => x.Login.Equals(name) && x.Password.Equals(password));
            return user;
        }

        public AppUserEntity FindByNameAsync(string userName)
        {
            return Get(x => x.Login.Equals(userName));
        }
    }
}
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Persistence;
using Places.Model.Entity;
using Shared.Repository;
using UserModule.Interfaces;

namespace UserModule.Repositories
{
    public class PlaceEmployeeRepository : RepositoryBase<PlaceEmployee>, IPlaceEmployeeRepository
    {
        public PlaceEmployeeRepository(SmartRestaurantDbContext databaseContext) : base(databaseContext)
        {
        }

        public IEnumerable<PlaceEntity> GetEmployeePlacesIncludingPlaceInfo(int employeeId)
        {

            return base.GetAllAsQueryable().Where(x => x.Employee.Id == employeeId)
                .Include(y => y.Employee)
                .Include(z => z.Place)
                .ThenInclude(place => place.Tables)
                .Include(z => z.Place)
                .ThenInclude(place => place.PlaceTypes)
                .Include(z => z.Place)
                .ThenInclude(place => place.PlaceMenuEntity)
                .ThenInclude(placeMenu => placeMenu.MenuItems)
                .Include(z => z.Place)
                .ThenInclude(place => place.Location)
                .Select(employee => employee.Place);

        }

        public IEnumerable<int> GetEmployeePlaceIds(int employeeId)
        {
            return base.GetManyAsQueryable(x => x.Employee.Id == employeeId).Include(x => x.Place)
                .Select(employee => employee.Place.Id);
        }
    }
}
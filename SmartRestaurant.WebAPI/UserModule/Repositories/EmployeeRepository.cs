using Persistence;
using Places.Model.Entity;
using Shared.Repository;
using UserModule.Interfaces;

namespace UserModule.Repositories
{
    public class EmployeeRepository : RepositoryBase<Employee>, IEmployeeRepository
    {
        public EmployeeRepository(SmartRestaurantDbContext databaseContext) : base(databaseContext)
        {
        }
    }
}
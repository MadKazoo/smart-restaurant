﻿using System;
using System.Collections.Generic;
using Places.Model;
using Places.Model.Entity;

namespace Persistence
{
    public class Mockup
    {
        public readonly PlaceEntity ExamplePlaceEntity = new PlaceEntity
        {
            Description = "",
            Key = "",
            Id = 1, Name = "place"
        };

        public List<CheckInEntity> CheckIns = new List<CheckInEntity>();
        public List<MenuItemEntity> MenuItems = new List<MenuItemEntity>();
        public List<OrderEntity> Orders = new List<OrderEntity>();
        public List<PlaceMenuEntity> PlaceMenus = new List<PlaceMenuEntity>();
        public List<PlaceEntity> Places = new List<PlaceEntity>();
        public List<PlaceTableEntity> PlaceTables = new List<PlaceTableEntity>();
        public List<PlaceCategoryType> PlaceTypes = new List<PlaceCategoryType>();
        public List<AppUserEntity> Users = new List<AppUserEntity>();

        public void GenerateMockupData(int amount)
        {
            Users.Add(new AppUserEntity
            {
                Id = 1,
                Login = "admin",
                Password = "admin",
                ImageIcon = "https://cdn1.iconfinder.com/data/icons/business-users/512/circle-512.png"
            });
            var testUserEntity = new AppUserEntity
            {
                Id = 2,
                Login = "test",
                Password = "test",
                ImageIcon =
                    "https://cdn1.vectorstock.com/i/1000x1000/25/70/user-icon-woman-profile-human-avatar-vector-10552570.jpg"
            };
            Users.Add(testUserEntity);

            for (var i = 1; i <= 2; i++)
            {
                var placeMenuEntity = new PlaceMenuEntity
                {
                    Id = i
                };
                placeMenuEntity.PlaceId = i;

                var place = new PlaceEntity(ExamplePlaceEntity)
                {
                    ImageUrl = i % 2 != 0
                        ? "https://ih1.redbubble.net/image.423538079.8089/ap,550x550,12x16,1,transparent,t.u1.png"
                        : "https://comparic.pl/wp-content/uploads/2018/06/pizzahut.jpg",
                    Key = i.ToString(),
                    Name = i % 2 == 0 ? "Pizza Hut" : "Los Pollos Hermanos",
                    Id = i, AvarageRating = new Random().Next(1, 5), PlaceMenuEntity = placeMenuEntity,
                    PlaceTypes = new List<PlaceCategoryType>()
                };

                Places.Add(place);

                var menuItems = new List<MenuItemEntity>
                {
                    new MenuItemEntity
                    {
                        Id = i, Name = "Prosciutto",
                        ImageUrl =
                            "https://ocs-pl.oktawave.com/v1/AUTH_876e5729-f8dd-45dd-908f-35d8bb716177/amrest-web-ordering/GRD4/GRD4590/W4/prosciuttoerucola_160-6.png",
                        Description =
                            "Szynka dojrzewająca, ser grana padano, pomidorki koktajlowe, rukola, mozzarella, ziołowy sos pomidorowy                        ",
                        Price = new decimal(34.99), PlaceMenuId = i, PlaceMenuEntity = placeMenuEntity
                    },
                    new MenuItemEntity
                    {
                        Id = amount + i, Name = "Classica",
                        ImageUrl =
                            "https://ocs-pl.oktawave.com/v1/AUTH_876e5729-f8dd-45dd-908f-35d8bb716177/amrest-web-ordering/GRD4/GRD4590/W4/classica_medium_160.png",
                        Description =
                            "Szynka dojrzewająca, ser grana padano, pomidorki koktajlowe, rukola, mozzarella, ziołowy sos pomidorowy                        ",
                        Price = new decimal(37.99), PlaceMenuId = i, PlaceMenuEntity = placeMenuEntity
                    }
                };

                placeMenuEntity.MenuItems = menuItems;
                PlaceMenus.Add(placeMenuEntity);
                MenuItems.AddRange(menuItems);
            }

            var placeTable = new PlaceTableEntity
            {
                TableNumber = 1,
                PlaceId = 1
            };

            /*  
              var pizzaPlaceType = new PlaceCategoryType() {Id = 1, PlaceTypeCountry = PlaceTypeCountry.FRANCE};
              var chickenPlaceType = new PlaceCategoryType() {Id = 2, PlaceTypeCountry= PlaceTypeCountry.FRANCE};
            
  
              PlaceTypes.Add(pizzaPlaceType);
              PlaceTypes.Add(chickenPlaceType);
  
              var types = Places[0].PlaceTypes.ToList();
              types.Add(pizzaPlaceType);
              Places[0].PlaceTypes = types;
              
              var types1 = Places[0].PlaceTypes.ToList();
              types1.Add(chickenPlaceType);
              Places[1].PlaceTypes = types1;
              
  //            var types2 = Places[0].PlaceTypes.ToList();
  //            types2.Add(pizzaPlaceType);
  //            Places[2].PlaceTypes = types2;
  //            
  //            var types3 = Places[0].PlaceTypes.ToList();
  //            types3.Add(pizzaPlaceType);
  //            Places[3].PlaceTypes = types3;
  
              Places.First().Tables = new List<PlaceTableEntity>(){ placeTable };
  
              PlaceTables.Add(placeTable);
          }
          
          */
        }
    }
}
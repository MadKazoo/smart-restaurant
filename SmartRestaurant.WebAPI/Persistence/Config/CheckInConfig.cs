using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Places.Model.Entity;

namespace Persistence.Config
{
    public class CheckInConfig : IEntityTypeConfiguration<CheckInEntity>
    {
        public void Configure(EntityTypeBuilder<CheckInEntity> builder)
        {
            builder.HasKey(CheckedIn => new {CheckedIn.Id});
            builder.HasMany(CheckedIn => CheckedIn.PartialOrders);
            builder.HasOne(CheckedIn => CheckedIn.Table);
            builder.HasMany(checkIn => checkIn.UsersReadyForOrder);
        }
    }
}
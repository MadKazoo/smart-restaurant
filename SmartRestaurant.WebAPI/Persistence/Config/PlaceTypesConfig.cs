using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Places.Model.Entity;

namespace Persistence.Config
{
    public class PlaceTypesConfig : IEntityTypeConfiguration<PlaceCategoryType>
    {
        public void Configure(EntityTypeBuilder<PlaceCategoryType> builder)
        {
            // builder.HasKey(MenuItem => new { MenuItem.Id, MenuItem. });
            builder.HasKey(place => place.Id);
        }
    }
}
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Places.Model.Entity;

namespace Persistence.Config
{
    public class PlaceMenuItemConfig : IEntityTypeConfiguration<MenuItemEntity>
    {
        public void Configure(EntityTypeBuilder<MenuItemEntity> builder)
        {
            // builder.HasKey(MenuItem => new { MenuItem.Id, MenuItem. });

            builder.HasKey(MenuItem => new {MenuItem.Id});
            // builder.HasOne(MenuItem => PlaceMenu);
        }
    }
}
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Places.Model.Entity;

namespace Persistence.Config
{
    public class PlaceConfig : IEntityTypeConfiguration<PlaceEntity>
    {
        public void Configure(EntityTypeBuilder<PlaceEntity> builder)
        {
            builder.HasKey(place => place.Id);

            builder.HasMany(place => place.Tables);
            
            builder.HasMany(z => z.PlaceTypes);
        }
    }
}
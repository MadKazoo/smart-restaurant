using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Places.Model.Entity;

namespace Persistence.Config
{
    public class CheckedInUsersConfig : IEntityTypeConfiguration<CheckedInUsers>
    {
        public void Configure(EntityTypeBuilder<CheckedInUsers> builder)
        {
            builder.HasKey(CheckedIn => new {CheckedIn.Id});
            builder.HasOne(checkIn => checkIn.User);
        }
    }
}
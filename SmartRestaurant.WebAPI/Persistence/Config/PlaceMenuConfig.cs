using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Places.Model.Entity;

namespace Persistence.Config
{
    public class PlaceMenuConfig : IEntityTypeConfiguration<PlaceMenuEntity>
    {
        public void Configure(EntityTypeBuilder<PlaceMenuEntity> builder)
        {
            builder.HasKey(PlaceMenu => new {PlaceMenu.Id});

            builder.HasMany(PlaceMenu => PlaceMenu.MenuItems);
        }
    }
}
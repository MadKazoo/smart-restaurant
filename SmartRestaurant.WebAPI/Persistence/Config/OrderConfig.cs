using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Places.Model.Entity;

namespace Persistence.Config
{
    public class OrderConfig : IEntityTypeConfiguration<OrderEntity>
    {
        public void Configure(EntityTypeBuilder<OrderEntity> builder)
        {
            builder.HasKey(order => new {order.Id});
            builder.HasMany(order => order.PartialOrders);
            builder.HasMany(order => order.UserOrderStatuses);
            builder.HasOne(order => order.Place);
            ;
        }
    }
}
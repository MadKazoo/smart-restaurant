using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Places.Model.Entity;

namespace Persistence.Config
{
    public class PlaceEmployeeConfig : IEntityTypeConfiguration<PlaceEmployee>
    {
        public void Configure(EntityTypeBuilder<PlaceEmployee> builder)
        {
            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.Employee);

            builder.HasOne(x => x.Place);
        }
    }
}
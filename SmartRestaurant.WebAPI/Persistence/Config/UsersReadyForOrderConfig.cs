using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Places.Model.Entity;

namespace Persistence.Config
{
    public class UsersReadyForOrderConfig : IEntityTypeConfiguration<UserCheckInStatus>
    {
        public void Configure(EntityTypeBuilder<UserCheckInStatus> builder)
        {
            builder.HasKey(u => u.Id);
            builder.HasOne(u => u.AppUserEntity);

//                builder.Ignore(u => u.AppUserEntity.Password);
        }
    }
}
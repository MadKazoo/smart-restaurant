using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Persistence.Config;
using Places.Model;
using Places.Model.Entity;

namespace Persistence
{
    public class SmartRestaurantDbContext : DbContext
    {
        public SmartRestaurantDbContext() : base()
        {
        }
        public SmartRestaurantDbContext(DbContextOptions<SmartRestaurantDbContext> options) : base(options)
        {
        }

        public DbSet<PlaceEntity> Places { get; set; }
        public DbSet<PlaceMenuEntity> PlaceMenus { get; set; }
        public DbSet<MenuItemEntity> MenuItems { get; set; }
        public DbSet<PlaceTableEntity> PlaceTables { get; set; }
        public DbSet<CheckInEntity> CheckIns { get; set; }
        public DbSet<PartialOrder> PartialOrders { get; set; }
        public DbSet<CategoryType> CategoryTypes { get; set; }
        public DbSet<UserPartialOrder> UserPartialOrders { get; set; }
        public DbSet<OrderEntity> Orders { get; set; }
        public DbSet<AppUserEntity> Users { get; set; }
        public DbSet<PlaceCategoryType> PlaceTypes { get; set; }
        public DbSet<UserOrderStatus> UserOrderStatus { get; set; }
        public DbSet<UserCheckInStatus> UserCheckInStatus { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<PlaceEmployee> PlaceEmployees { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyConfiguration(new PlaceMenuConfig());
            builder.ApplyConfiguration(new OrderConfig());
            builder.ApplyConfiguration(new PlaceConfig());
            builder.ApplyConfiguration(new PlaceTypesConfig());
            builder.ApplyConfiguration(new CheckInConfig());
            builder.ApplyConfiguration(new CheckedInUsersConfig());
            builder.ApplyConfiguration(new UserPartialOrderConfig());
            builder.ApplyConfiguration(new PlaceEmployeeConfig());
        }
    }
    
    
    public class RestaurantContextFacotry : IDesignTimeDbContextFactory<SmartRestaurantDbContext>
    {
        public SmartRestaurantDbContext CreateDbContext(string[] args)
        {
            string connectionString = "Server=127.0.0.1,1433;Database=master;User ID=sa;password=Password1!;";
            
            var optionsBuilder = new DbContextOptionsBuilder<SmartRestaurantDbContext>();


            optionsBuilder.UseSqlServer(connectionString, x => x.UseNetTopologySuite());
            return new SmartRestaurantDbContext(optionsBuilder.Options);
        }
    }
}
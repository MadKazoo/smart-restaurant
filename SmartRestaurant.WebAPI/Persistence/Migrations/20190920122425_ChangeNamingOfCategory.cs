﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations
{
    public partial class ChangeNamingOfCategory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PlaceTypes_PlaceTypeCountry_PlaceTypeCountryId",
                table: "PlaceTypes");

            migrationBuilder.RenameColumn(
                name: "PlaceTypeCountryId",
                table: "PlaceTypes",
                newName: "CategoryTypeId");

            migrationBuilder.RenameIndex(
                name: "IX_PlaceTypes_PlaceTypeCountryId",
                table: "PlaceTypes",
                newName: "IX_PlaceTypes_CategoryTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_PlaceTypes_PlaceTypeCountry_CategoryTypeId",
                table: "PlaceTypes",
                column: "CategoryTypeId",
                principalTable: "PlaceTypeCountry",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PlaceTypes_PlaceTypeCountry_CategoryTypeId",
                table: "PlaceTypes");

            migrationBuilder.RenameColumn(
                name: "CategoryTypeId",
                table: "PlaceTypes",
                newName: "PlaceTypeCountryId");

            migrationBuilder.RenameIndex(
                name: "IX_PlaceTypes_CategoryTypeId",
                table: "PlaceTypes",
                newName: "IX_PlaceTypes_PlaceTypeCountryId");

            migrationBuilder.AddForeignKey(
                name: "FK_PlaceTypes_PlaceTypeCountry_PlaceTypeCountryId",
                table: "PlaceTypes",
                column: "PlaceTypeCountryId",
                principalTable: "PlaceTypeCountry",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

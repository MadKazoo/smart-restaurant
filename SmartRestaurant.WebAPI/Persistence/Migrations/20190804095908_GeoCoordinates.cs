﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations
{
    public partial class GeoCoordinates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                "GeoCoordinateses",
                table => new
                {
                    Id = table.Column<int>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    AddressName = table.Column<string>(nullable: true),
                    Latitude = table.Column<double>(),
                    Longitude = table.Column<double>()
                },
                constraints: table => { table.PrimaryKey("PK_GeoCoordinateses", x => x.Id); });

            migrationBuilder.CreateTable(
                "PlaceMenus",
                table => new
                {
                    Id = table.Column<int>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    PlaceId = table.Column<int>()
                },
                constraints: table => { table.PrimaryKey("PK_PlaceMenus", x => x.Id); });

            migrationBuilder.CreateTable(
                "PlaceTypeCountry",
                table => new
                {
                    Id = table.Column<int>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    ImageSrc = table.Column<string>(nullable: true)
                },
                constraints: table => { table.PrimaryKey("PK_PlaceTypeCountry", x => x.Id); });

            migrationBuilder.CreateTable(
                "Users",
                table => new
                {
                    Id = table.Column<long>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    Login = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    ImageIcon = table.Column<string>(nullable: true)
                },
                constraints: table => { table.PrimaryKey("PK_Users", x => x.Id); });

            migrationBuilder.CreateTable(
                "MenuItems",
                table => new
                {
                    Id = table.Column<int>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    PlaceMenuId = table.Column<int>(),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Price = table.Column<decimal>(),
                    ImageUrl = table.Column<string>(nullable: true),
                    PlaceMenuEntityId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MenuItems", x => x.Id);
                    table.ForeignKey(
                        "FK_MenuItems_PlaceMenus_PlaceMenuEntityId",
                        x => x.PlaceMenuEntityId,
                        "PlaceMenus",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                "Places",
                table => new
                {
                    Id = table.Column<int>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    Key = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    ImageUrl = table.Column<string>(nullable: true),
                    PlaceMenuEntityId = table.Column<int>(nullable: true),
                    AvarageRating = table.Column<double>(),
                    coordinatesId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Places", x => x.Id);
                    table.ForeignKey(
                        "FK_Places_PlaceMenus_PlaceMenuEntityId",
                        x => x.PlaceMenuEntityId,
                        "PlaceMenus",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        "FK_Places_GeoCoordinateses_coordinatesId",
                        x => x.coordinatesId,
                        "GeoCoordinateses",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                "CheckedInUsers",
                table => new
                {
                    Id = table.Column<long>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    CheckInId = table.Column<Guid>(),
                    UserId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CheckedInUsers", x => x.Id);
                    table.ForeignKey(
                        "FK_CheckedInUsers_Users_UserId",
                        x => x.UserId,
                        "Users",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                "PlaceTables",
                table => new
                {
                    Id = table.Column<long>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    TableNumber = table.Column<int>(),
                    PlaceId = table.Column<int>(),
                    PlaceEntityId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlaceTables", x => x.Id);
                    table.ForeignKey(
                        "FK_PlaceTables_Places_PlaceEntityId",
                        x => x.PlaceEntityId,
                        "Places",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                "PlaceTypes",
                table => new
                {
                    Id = table.Column<int>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    PlaceTypeCountryId = table.Column<int>(nullable: true),
                    PlaceEntityId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlaceTypes", x => x.Id);
                    table.ForeignKey(
                        "FK_PlaceTypes_Places_PlaceEntityId",
                        x => x.PlaceEntityId,
                        "Places",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        "FK_PlaceTypes_PlaceTypeCountry_PlaceTypeCountryId",
                        x => x.PlaceTypeCountryId,
                        "PlaceTypeCountry",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                "CheckIns",
                table => new
                {
                    Id = table.Column<Guid>(),
                    TableId = table.Column<long>(nullable: true),
                    PlaceId = table.Column<int>(nullable: true),
                    Status = table.Column<int>(),
                    DateCreated = table.Column<DateTime>()
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CheckIns", x => x.Id);
                    table.ForeignKey(
                        "FK_CheckIns_Places_PlaceId",
                        x => x.PlaceId,
                        "Places",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        "FK_CheckIns_PlaceTables_TableId",
                        x => x.TableId,
                        "PlaceTables",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                "Orders",
                table => new
                {
                    Id = table.Column<int>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    GroupId = table.Column<Guid>(),
                    OrderStatus = table.Column<int>(),
                    PlaceId = table.Column<int>(nullable: true),
                    TableId = table.Column<long>(nullable: true),
                    DateCreated = table.Column<DateTime>(),
                    Price = table.Column<decimal>()
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.Id);
                    table.ForeignKey(
                        "FK_Orders_Places_PlaceId",
                        x => x.PlaceId,
                        "Places",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        "FK_Orders_PlaceTables_TableId",
                        x => x.TableId,
                        "PlaceTables",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                "UserCheckInStatus",
                table => new
                {
                    Id = table.Column<long>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    AppUserEntityId = table.Column<long>(nullable: true),
                    Status = table.Column<int>(),
                    ToPay = table.Column<decimal>(),
                    CheckInEntityId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserCheckInStatus", x => x.Id);
                    table.ForeignKey(
                        "FK_UserCheckInStatus_Users_AppUserEntityId",
                        x => x.AppUserEntityId,
                        "Users",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        "FK_UserCheckInStatus_CheckIns_CheckInEntityId",
                        x => x.CheckInEntityId,
                        "CheckIns",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                "PartialOrders",
                table => new
                {
                    Id = table.Column<long>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    MenuItemId = table.Column<int>(nullable: true),
                    CheckInEntityId = table.Column<Guid>(nullable: true),
                    OrderEntityId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PartialOrders", x => x.Id);
                    table.ForeignKey(
                        "FK_PartialOrders_CheckIns_CheckInEntityId",
                        x => x.CheckInEntityId,
                        "CheckIns",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        "FK_PartialOrders_MenuItems_MenuItemId",
                        x => x.MenuItemId,
                        "MenuItems",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        "FK_PartialOrders_Orders_OrderEntityId",
                        x => x.OrderEntityId,
                        "Orders",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                "UserOrderStatus",
                table => new
                {
                    Id = table.Column<long>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<long>(),
                    HasPaid = table.Column<bool>(),
                    ToPay = table.Column<decimal>(),
                    OrderEntityId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserOrderStatus", x => x.Id);
                    table.ForeignKey(
                        "FK_UserOrderStatus_Orders_OrderEntityId",
                        x => x.OrderEntityId,
                        "Orders",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                "UserPartialOrders",
                table => new
                {
                    Id = table.Column<long>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    ClientPercentShare = table.Column<decimal>(),
                    ToPayInMoney = table.Column<decimal>(),
                    AppUserId = table.Column<long>(),
                    PartialOrderId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserPartialOrders", x => x.Id);
                    table.ForeignKey(
                        "FK_UserPartialOrders_PartialOrders_PartialOrderId",
                        x => x.PartialOrderId,
                        "PartialOrders",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                "IX_CheckedInUsers_UserId",
                "CheckedInUsers",
                "UserId");

            migrationBuilder.CreateIndex(
                "IX_CheckIns_PlaceId",
                "CheckIns",
                "PlaceId");

            migrationBuilder.CreateIndex(
                "IX_CheckIns_TableId",
                "CheckIns",
                "TableId");

            migrationBuilder.CreateIndex(
                "IX_MenuItems_PlaceMenuEntityId",
                "MenuItems",
                "PlaceMenuEntityId");

            migrationBuilder.CreateIndex(
                "IX_Orders_PlaceId",
                "Orders",
                "PlaceId");

            migrationBuilder.CreateIndex(
                "IX_Orders_TableId",
                "Orders",
                "TableId");

            migrationBuilder.CreateIndex(
                "IX_PartialOrders_CheckInEntityId",
                "PartialOrders",
                "CheckInEntityId");

            migrationBuilder.CreateIndex(
                "IX_PartialOrders_MenuItemId",
                "PartialOrders",
                "MenuItemId");

            migrationBuilder.CreateIndex(
                "IX_PartialOrders_OrderEntityId",
                "PartialOrders",
                "OrderEntityId");

            migrationBuilder.CreateIndex(
                "IX_Places_PlaceMenuEntityId",
                "Places",
                "PlaceMenuEntityId");

            migrationBuilder.CreateIndex(
                "IX_Places_coordinatesId",
                "Places",
                "coordinatesId");

            migrationBuilder.CreateIndex(
                "IX_PlaceTables_PlaceEntityId",
                "PlaceTables",
                "PlaceEntityId");

            migrationBuilder.CreateIndex(
                "IX_PlaceTypes_PlaceEntityId",
                "PlaceTypes",
                "PlaceEntityId");

            migrationBuilder.CreateIndex(
                "IX_PlaceTypes_PlaceTypeCountryId",
                "PlaceTypes",
                "PlaceTypeCountryId");

            migrationBuilder.CreateIndex(
                "IX_UserCheckInStatus_AppUserEntityId",
                "UserCheckInStatus",
                "AppUserEntityId");

            migrationBuilder.CreateIndex(
                "IX_UserCheckInStatus_CheckInEntityId",
                "UserCheckInStatus",
                "CheckInEntityId");

            migrationBuilder.CreateIndex(
                "IX_UserOrderStatus_OrderEntityId",
                "UserOrderStatus",
                "OrderEntityId");

            migrationBuilder.CreateIndex(
                "IX_UserPartialOrders_PartialOrderId",
                "UserPartialOrders",
                "PartialOrderId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                "CheckedInUsers");

            migrationBuilder.DropTable(
                "PlaceTypes");

            migrationBuilder.DropTable(
                "UserCheckInStatus");

            migrationBuilder.DropTable(
                "UserOrderStatus");

            migrationBuilder.DropTable(
                "UserPartialOrders");

            migrationBuilder.DropTable(
                "PlaceTypeCountry");

            migrationBuilder.DropTable(
                "Users");

            migrationBuilder.DropTable(
                "PartialOrders");

            migrationBuilder.DropTable(
                "CheckIns");

            migrationBuilder.DropTable(
                "MenuItems");

            migrationBuilder.DropTable(
                "Orders");

            migrationBuilder.DropTable(
                "PlaceTables");

            migrationBuilder.DropTable(
                "Places");

            migrationBuilder.DropTable(
                "PlaceMenus");

            migrationBuilder.DropTable(
                "GeoCoordinateses");
        }
    }
}
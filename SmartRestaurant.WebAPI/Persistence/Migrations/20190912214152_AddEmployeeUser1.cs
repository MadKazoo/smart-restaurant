﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations
{
    public partial class AddEmployeeUser1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                "FK_PlaceEmployees_Users_EmployeeId",
                "PlaceEmployees");

            migrationBuilder.DropForeignKey(
                "FK_Places_GeoCoordinateses_coordinatesId",
                "Places");

            migrationBuilder.DropPrimaryKey(
                "PK_GeoCoordinateses",
                "GeoCoordinateses");

            migrationBuilder.DropColumn(
                "Discriminator",
                "Users");

            migrationBuilder.RenameTable(
                "GeoCoordinateses",
                newName: "GeoCoordinates");

            migrationBuilder.AddPrimaryKey(
                "PK_GeoCoordinates",
                "GeoCoordinates",
                "Id");

            migrationBuilder.CreateTable(
                "Employees",
                table => new
                {
                    Id = table.Column<long>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    Login = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    ImageIcon = table.Column<string>(nullable: true)
                },
                constraints: table => { table.PrimaryKey("PK_Employees", x => x.Id); });

            migrationBuilder.AddForeignKey(
                "FK_PlaceEmployees_Employees_EmployeeId",
                "PlaceEmployees",
                "EmployeeId",
                "Employees",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                "FK_Places_GeoCoordinates_coordinatesId",
                "Places",
                "coordinatesId",
                "GeoCoordinates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                "FK_PlaceEmployees_Employees_EmployeeId",
                "PlaceEmployees");

            migrationBuilder.DropForeignKey(
                "FK_Places_GeoCoordinates_coordinatesId",
                "Places");

            migrationBuilder.DropTable(
                "Employees");

            migrationBuilder.DropPrimaryKey(
                "PK_GeoCoordinates",
                "GeoCoordinates");

            migrationBuilder.RenameTable(
                "GeoCoordinates",
                newName: "GeoCoordinateses");

            migrationBuilder.AddColumn<string>(
                "Discriminator",
                "Users",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                "PK_GeoCoordinateses",
                "GeoCoordinateses",
                "Id");

            migrationBuilder.AddForeignKey(
                "FK_PlaceEmployees_Users_EmployeeId",
                "PlaceEmployees",
                "EmployeeId",
                "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                "FK_Places_GeoCoordinateses_coordinatesId",
                "Places",
                "coordinatesId",
                "GeoCoordinateses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
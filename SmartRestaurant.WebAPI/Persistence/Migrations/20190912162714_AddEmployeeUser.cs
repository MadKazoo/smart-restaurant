﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations
{
    public partial class AddEmployeeUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                "Discriminator",
                "Users",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateTable(
                "PlaceEmployees",
                table => new
                {
                    Id = table.Column<int>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    EmployeeId = table.Column<long>(nullable: true),
                    PlaceId = table.Column<int>(nullable: true),
                    EmployeePermission = table.Column<int>()
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlaceEmployees", x => x.Id);
                    table.ForeignKey(
                        "FK_PlaceEmployees_Users_EmployeeId",
                        x => x.EmployeeId,
                        "Users",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        "FK_PlaceEmployees_Places_PlaceId",
                        x => x.PlaceId,
                        "Places",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                "IX_PlaceEmployees_EmployeeId",
                "PlaceEmployees",
                "EmployeeId");

            migrationBuilder.CreateIndex(
                "IX_PlaceEmployees_PlaceId",
                "PlaceEmployees",
                "PlaceId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                "PlaceEmployees");

            migrationBuilder.DropColumn(
                "Discriminator",
                "Users");
        }
    }
}
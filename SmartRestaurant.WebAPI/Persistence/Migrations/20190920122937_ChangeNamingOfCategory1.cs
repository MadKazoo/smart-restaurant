﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations
{
    public partial class ChangeNamingOfCategory1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PlaceTypes_PlaceTypeCountry_CategoryTypeId",
                table: "PlaceTypes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PlaceTypeCountry",
                table: "PlaceTypeCountry");

            migrationBuilder.RenameTable(
                name: "PlaceTypeCountry",
                newName: "CategoryTypes");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CategoryTypes",
                table: "CategoryTypes",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_PlaceTypes_CategoryTypes_CategoryTypeId",
                table: "PlaceTypes",
                column: "CategoryTypeId",
                principalTable: "CategoryTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PlaceTypes_CategoryTypes_CategoryTypeId",
                table: "PlaceTypes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CategoryTypes",
                table: "CategoryTypes");

            migrationBuilder.RenameTable(
                name: "CategoryTypes",
                newName: "PlaceTypeCountry");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PlaceTypeCountry",
                table: "PlaceTypeCountry",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_PlaceTypes_PlaceTypeCountry_CategoryTypeId",
                table: "PlaceTypes",
                column: "CategoryTypeId",
                principalTable: "PlaceTypeCountry",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

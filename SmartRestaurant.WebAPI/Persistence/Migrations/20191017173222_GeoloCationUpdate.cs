﻿using Microsoft.EntityFrameworkCore.Migrations;
using NetTopologySuite.Geometries;

namespace Persistence.Migrations
{
    public partial class GeoloCationUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Places_GeoCoordinates_coordinatesId",
                table: "Places");

            migrationBuilder.DropIndex(
                name: "IX_Places_coordinatesId",
                table: "Places");

            migrationBuilder.DropColumn(
                name: "Location",
                table: "Places");

            migrationBuilder.DropColumn(
                name: "coordinatesId",
                table: "Places");

            migrationBuilder.AddColumn<Point>(
                name: "GeoLocation",
                table: "Places",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GeoLocation",
                table: "Places");

            migrationBuilder.AddColumn<Geometry>(
                name: "Location",
                table: "Places",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "coordinatesId",
                table: "Places",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Places_coordinatesId",
                table: "Places",
                column: "coordinatesId");

            migrationBuilder.AddForeignKey(
                name: "FK_Places_GeoCoordinates_coordinatesId",
                table: "Places",
                column: "coordinatesId",
                principalTable: "GeoCoordinates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

using System;
using System.Linq.Expressions;
using NetTopologySuite.Geometries;
using Places.Model.Entity;

namespace PlaceModule.Services
{
    public interface IGeoLocationLogic
    {
        Expression<Func<PlaceEntity, bool>> GetNearbyPlaces(Point location, double distance);
    }
}
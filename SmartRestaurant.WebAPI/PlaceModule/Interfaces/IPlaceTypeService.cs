using System.Collections.Generic;
using Places.Model.Entity;

namespace PlaceModule.Interfaces
{
    public interface IPlaceTypeService
    {
        IEnumerable<CategoryType> GetPlaceTypes();

        void AddPlaceType(CategoryType categoryType);
    }
}
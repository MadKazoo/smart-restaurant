﻿using System;
using System.Collections.Generic;
using NetTopologySuite.Geometries;
using Places.Model.Domain;
using Places.Model.Entity;

namespace PlaceModule.Interfaces
{
    public interface IClientPlaceService
    {
        IEnumerable<PlaceDto> GetPlaces();
        IEnumerable<CategoryType> GetPlaceTypes();

        Tuple<int, int> GetPlaceAndTableId(string tableHash);

        PlaceEntity GetPlace(int id);
        
        IEnumerable<PlaceEntity> GetPlaces(int idFrom, int idTo);

        IEnumerable<PlaceDto> GetNearbyPlaces(double lat, double lon, double distance);

        PlaceMenuEntity GetPlaceMenu(int placeId);
        // Place GetPlace(int id);
        IEnumerable<PlaceDto> GetNearest(double lat, double lon, int amount);
    }
}
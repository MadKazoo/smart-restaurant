using System.Collections.Generic;
using NetTopologySuite.Geometries;
using Places.Model.Domain;

namespace PlaceModule.Interfaces
{
    public interface IPlaceService
    {
        void AddPlace(PlaceDto place, int userId);

        void AddTableToPlace(int placeId, int tableId, int amountOfSeats);

        PlaceDto UpdatePlace(PlaceDto placeDto);

        IEnumerable<CheckInDto> GetPlaceCheckIns(int placeId);
        
        void DeletePlace(int placeId);
        
        IEnumerable<PlaceDto> GetPlacesAccessibleToEmployee(int employee); 

    }
}
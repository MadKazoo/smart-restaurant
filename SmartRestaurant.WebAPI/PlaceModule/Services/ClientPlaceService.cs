﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using NetTopologySuite.Geometries;
using Persistence;
using PlaceModule.Interfaces;
using Places.Model.Domain;
using Places.Model.Entity;
using Places.Model.Mapping;

namespace PlaceModule.Services
{
    public class ClientPlaceService : IClientPlaceService
    {
        private readonly SmartRestaurantDbContext _context;

        public ClientPlaceService(SmartRestaurantDbContext context)
        {
            _context = context;
        }

        public IEnumerable<PlaceDto> GetPlaces()
        {
            var placesEntities = _context.Places
                .Include(place => place.Tables)
                .Include(place => place.PlaceTypes)
                .ThenInclude(placeTypes => placeTypes.CategoryType);
            var places = placesEntities.Select(x => x.EntityToDomain());
            return places;
        }

        public IEnumerable<CategoryType> GetPlaceTypes()
        {
            var placeTypes = _context.CategoryTypes.ToList();
            return placeTypes;
        }

        public IEnumerable<PlaceEntity> GetPlaces(int idFrom, int idTo)
        {
            throw new NotImplementedException();
        }

        public Tuple<int, int> GetPlaceAndTableId(string tableHash)
        {
            var table = _context.PlaceTables.FirstOrDefault(x => x.Hash == tableHash);
            return new Tuple<int, int>(table.PlaceId, table.TableNumber);
        }

        //TODO change it to DTO
        public PlaceMenuEntity GetPlaceMenu(int placeId)
        {
            return _context.Places.FirstOrDefault(x => x.Id == placeId).PlaceMenuEntity;
        }

        public PlaceEntity GetPlace(int id)
        {
            var place = _context.Places.FirstOrDefault(x=>x.Id==id);
            return place;
        }
        
        public IEnumerable<PlaceDto> GetNearbyPlaces(double lat, double lon, double distance)
        {
            Point location = new Point(lon, lat)
            {
                SRID = 4326
            };
            
            var places = _context.Places.Where(x => x.Location.ProjectTo(2855).Distance(location.ProjectTo(2855)) <= distance)
                .Select(x=>x.EntityToDomain()).ToList();
            return places;
        }
        
        public IEnumerable<PlaceDto> GetNearest(double lat, double lon, int amount)
        {
            Point location = new Point(lon, lat)
            {
                SRID = 4326
            };
            var places = _context.Places.Take(5)
                .Skip(amount).OrderByDescending(x=>x.Location.Distance(location))
                .Select(x=>x.EntityToDomain()).ToList();
            return places;        }

    }
}
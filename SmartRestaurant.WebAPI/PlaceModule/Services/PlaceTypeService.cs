using System.Collections.Generic;
using System.Linq;
using Persistence;
using PlaceModule.Interfaces;
using Places.Model.Entity;
using Shared.Interfaces;

namespace PlaceModule.Services
{
    public class PlaceTypeService : IPlaceTypeService
    {
        private SmartRestaurantDbContext _context;

        public PlaceTypeService(SmartRestaurantDbContext context)
        {
            _context = context;
        }

        public IEnumerable<CategoryType> GetPlaceTypes()
        {
            return _context.CategoryTypes.ToList();
        }

        public void AddPlaceType(CategoryType categoryType)
        {
            _context.CategoryTypes.Add(categoryType);
            _context.SaveChanges();
        }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using NetTopologySuite.Geometries;
using Persistence;
using PlaceModule.Interfaces;
using Places.Model.Domain;
using Places.Model.Entity;
using Places.Model.Mapping;

namespace PlaceModule.Services
{
    public class PlaceService : IPlaceService
    {
        private readonly PlacesFactory _placesFactory;
        private readonly PlaceTableFactory _placeTableFactory;
        private readonly SmartRestaurantDbContext _context;

        public PlaceService(SmartRestaurantDbContext context, PlacesFactory placesFactory, PlaceTableFactory placeTableFactory)
        {
            _placesFactory = placesFactory;
            _placeTableFactory = placeTableFactory;
            _context = context;
        }

        public void AddPlace(PlaceDto place, int userId)
        {
            var placeEntity = AddPlace(place);
            AddEmployeeToPlace(placeEntity, userId);
            AddCategoryTypesToPlace(place, placeEntity);
        }

        public void AddTableToPlace(int placeId, int tableNumber, int amountOfSeats)
        {
            var table = _placeTableFactory.CreatePlaceTable(placeId, tableNumber);
            var place = _context.Places.FirstOrDefault(x => x.Id == placeId);
            var tables = new List<PlaceTableEntity>();

            if (place.Tables != null) tables = place.Tables.ToList();

            tables.Add(table);
            place.Tables = tables;

            _context.SaveChanges();
        }

        public PlaceDto UpdatePlace(PlaceDto placeDto)
        {
            // TO REFACTOR
            var place = _context.Places.Where(x => x.Id == placeDto.Id)
                .Include(x => x.Tables)
                .Include(x => x.PlaceTypes)
                    .ThenInclude(x => x.CategoryType)
                .FirstOrDefault();

            if (place == null)
            {
                throw new ArgumentNullException();
            }
            
            place.Name = placeDto.Name;
            place.Description = placeDto.Description;
            place.ImageUrl = placeDto.ImageUrl;
            place.Location = new Point(placeDto.coordinates.Longitude, placeDto.coordinates.Latitude)
            {
                SRID = 4326
            };
            
            place.Address = placeDto.coordinates.AddressName;
            
            AddCategoryTypesToPlace(placeDto, place);
            
            var placeTablesDifference = placeDto.NumberOfTables - place.Tables.Count();
            
            for (var tableNumber = place.Tables.Count() + 1; tableNumber <= placeTablesDifference; tableNumber++)
                place.Tables = place.Tables.Append(_placeTableFactory.CreatePlaceTable(place.Id, tableNumber)).ToList();

            if (placeTablesDifference < 0)
                place.Tables = place.Tables.Where(table => table.TableNumber < placeDto.NumberOfTables).ToList();

            _context.SaveChanges();

            return place.EntityToDomain();
        }

        public IEnumerable<CheckInDto> GetPlaceCheckIns(int placeId)
        {
            var checkIns = _context.CheckIns.Where(x => x.Place.Id == placeId && x.Status==CheckInStatus.NEW).Include(x => x.Table)
                .Include(x=>x.Place)
                .Include(x => x.PartialOrders).ThenInclude(x => x.UserPartialOrders)
                .Include(x => x.PartialOrders).ThenInclude(x => x.MenuItem).Include(x => x.UsersReadyForOrder)
                .ThenInclude(x=>x.AppUserEntity)
                .Select(x => x.MapToDto());

            return checkIns;
        }

        public void DeletePlace(int placeId)
        {
            var place = _context.Places.FirstOrDefault(x=> x.Id == placeId);
            _context.Remove(place);
            _context.SaveChanges();
        }

        public IEnumerable<PlaceDto> GetPlacesAccessibleToEmployee(int employeeId)
        {
            var employeePlacesIds = GetEmployeePlaces(employeeId);
            var places = GetAllEmployeePlaces(employeePlacesIds).ToList();
            
            return places;
        }

        private PlaceEntity AddPlace(PlaceDto place)
        {
            var placeEntity = _placesFactory.CreatePlace(place);
            _context.Places.Add(placeEntity);
            _context.SaveChanges();
            placeEntity.Tables = _placeTableFactory.CreateListOfTables(place.NumberOfTables, placeEntity.Id);
            return placeEntity;
        }

        private void AddCategoryTypesToPlace(PlaceDto placeDto, PlaceEntity placeEntity)
        {
            var categoryTypes = _context.CategoryTypes.Where(x=>placeDto.PlaceTags.Select(tag=>tag.Id).Contains(x.Id));
            placeEntity.PlaceTypes = categoryTypes.Select(z=>new PlaceCategoryType(){CategoryType = z}).ToList();
            _context.SaveChanges();
        }
        
        private void AddEmployeeToPlace(PlaceEntity placeEntity, int userId)
        {
            var employee = _context.Employees.FirstOrDefault(x=>x.Id==userId); _context.SaveChanges();
            _context.PlaceEmployees.Add(new PlaceEmployee(){ Employee = employee, Place = placeEntity});
            _context.SaveChanges();
        }

        private IEnumerable<PlaceDto> GetAllEmployeePlaces(IEnumerable<int> placesIds)
        {
            var places = _context.Places.Where(x=>placesIds.Contains(x.Id))
                .Include(x => x.Tables)
                .Include(x => x.PlaceTypes)
                .ThenInclude(y=>y.CategoryType)
                .Include(x => x.PlaceMenuEntity)
                .ThenInclude(y => y.MenuItems)
                .Select(x=>x.EntityToDomain());
            return places;
        }
        
        private IEnumerable<int> GetEmployeePlaces(int employeeId)
        {
            return _context.PlaceEmployees.Where(x => x.Employee.Id == employeeId)
                .Include(x => x.Place)
                .Select(employee => employee.Place.Id).ToList();
        }
    }
}

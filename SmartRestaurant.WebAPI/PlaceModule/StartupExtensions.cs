using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PlaceModule.Interfaces;
using PlaceModule.Services;

namespace PlaceModule
{
    
    public static class PlaceModuleExtensions
    {
        public static void RegisterPlaceModuleDependencies(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddTransient<IClientPlaceService, ClientPlaceService>();
            services.AddTransient<IPlaceService, PlaceService>();
            services.AddTransient<IPlaceTypeService, PlaceTypeService>();
            services.AddSingleton<PlaceTableFactory, PlaceTableFactory>();
            services.AddSingleton<PlacesFactory, PlacesFactory>();
        }
    }
}

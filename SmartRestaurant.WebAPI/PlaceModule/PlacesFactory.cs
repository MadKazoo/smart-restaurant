using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore.Migrations.Operations;
using NetTopologySuite.Geometries;
using Places.Model.Domain;
using Places.Model.Entity;
using Shared.Helpers;
using Shared.Interfaces;

namespace PlaceModule
{
    public class PlacesFactory
    {
        private PlaceTableFactory _placeTableFactory;

        public PlacesFactory(PlaceTableFactory placeTableFactory)
        {
            _placeTableFactory = placeTableFactory;
        }
        public PlaceEntity CreatePlace(PlaceDto place)
        {
            var placeEntity = new PlaceEntity
            {
                Description = place.Description,
                PlaceMenuEntity = new PlaceMenuEntity
                {
                    MenuItems = new List<MenuItemEntity>()
                },
                Name = place.Name,
                ImageUrl = place.ImageUrl,
                Tables = new List<PlaceTableEntity>(),
                PlaceTypes = new List<PlaceCategoryType>(),
                Location = new Point(place.coordinates.Longitude, place.coordinates.Latitude)
                {
                    SRID = 4326
                }
            };

            return placeEntity;
        }
    }

    public class PlaceTableFactory
    {
        private IMd5Hashing _md5Hashing;

        public PlaceTableEntity CreatePlaceTable(int placeId, int tableNumber)
        {
            return new PlaceTableEntity
            {
                PlaceId = placeId,
                TableNumber = tableNumber,
                Hash = _md5Hashing.CreateHash((placeId + tableNumber).ToString())
            };
        }
        
        public PlaceTableFactory (IMd5Hashing md5Hashing)
        {
            _md5Hashing = md5Hashing;

        }

        public IEnumerable<PlaceTableEntity> CreateListOfTables(int amount, int placeId)
        {
            IEnumerable<PlaceTableEntity> placeTableEntities = new List<PlaceTableEntity>();
            for (int i = 1; i <= amount; i++)
            {
                placeTableEntities.Append(CreatePlaceTable(i, placeId));
            }

            return placeTableEntities;
        }
    }
}
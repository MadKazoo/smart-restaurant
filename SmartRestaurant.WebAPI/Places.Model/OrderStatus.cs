namespace Places.Model
{
    public enum OrderStatus
    {
        NEW = 0,
        ACCEPTED = 1,
        PAID = 2,
        FINISHED = 3
    }
}
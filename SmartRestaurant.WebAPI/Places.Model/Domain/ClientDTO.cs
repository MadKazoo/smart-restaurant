namespace Places.Model.Domain
{
    public class ClientDTO
    {
        public long Id { get; set; }
        public string Name { get; set; } = "";
        public string ImageIcon { get; set; }
        public bool IsReadyForOrder { get; set; }
        public decimal AmountToPay { get; set; }
    }
}
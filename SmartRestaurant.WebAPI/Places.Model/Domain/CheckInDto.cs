using System;
using System.Collections.Generic;
using Places.Model.Entity;

namespace Places.Model.Domain
{
    public class CheckInDto
    {
        public Guid Id { get; set; }
        public IEnumerable<ClientDTO> CheckedInUsers { get; set; }
        public int PlaceId { get; set; }
        public long TableId { get; set; }
        public IEnumerable<PartialOrder> PartialOrders { get; set; }
    }
}
using System;
using System.Collections.Generic;
using Places.Model.Entity;

namespace Places.Model.Domain
{
    public class OrderDto
    {
        public int Id { get; set; }
        public OrderStatus? OrderStatus { get; set; }
        public string GroupId { get; set; }
        public int PlaceId { get; set; }
        public string PlaceName { get; set; }
        public DateTime DateCreated { get; set; }
        public decimal Price { get; set; }
        public int TableNumber { get; set; }
        public IEnumerable<PartialOrder> PartialOrders { get; set; }
        public IEnumerable<UserOrderStatus> UserOrderStatuses { get; set; }
    }
}
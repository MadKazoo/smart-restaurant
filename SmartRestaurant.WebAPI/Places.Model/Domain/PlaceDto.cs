using System.Collections.Generic;
using Places.Model.Entity;

namespace Places.Model.Domain
{
    public class PlaceDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string ImageUrl { get; set; }
        public int NumberOfTables { get; set; }
        public double AvarageRating { get; set; }
        public GeoCoordinates coordinates { get; set; }
        public IEnumerable<CategoryType> PlaceTags { get; set; }
    }
}
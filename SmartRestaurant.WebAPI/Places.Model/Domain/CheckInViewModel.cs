namespace Places.Model.Domain
{
    public class CheckInViewModel
    {
        public int ClientId { get; set; }

        public string Hash { get; set; }

        public int PlaceId { get; set; }

        public int TableNumber { get; set; }
    }
}
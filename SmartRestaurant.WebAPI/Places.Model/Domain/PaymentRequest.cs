namespace Places.Model.Domain
{
    public class PaymentRequest
    {
        public long UserId { get; set; }
        public int OrderId { get; set; }
        public int PlaceId { get; set; }
        public bool IsPaid { get; set; }
    }
}
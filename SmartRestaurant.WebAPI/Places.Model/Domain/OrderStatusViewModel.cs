namespace Places.Model.Domain
{
    public class OrderStatusViewModel
    {
        public int OrderId;
        public OrderStatus OrderStatus;
    }
}
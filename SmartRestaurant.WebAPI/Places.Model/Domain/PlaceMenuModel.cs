using System.Collections.Generic;

namespace Places.Model.Domain
{
    public class PlaceMenuModel
    {
        public IEnumerable<MenuItemModel> MenuItems { get; set; }
        public string PlaceName { get; set; }
    }
}
using System.Collections.Generic;
using Places.Model.Entity;

namespace Places.Model.Domain
{
    public class PartialOrderDto
    {
        public long Id { get; set; }
        public MenuItemEntity MenuItem { get; set; }
        public IEnumerable<UserPartialOrder> UserPartialOrders { get; set; }
    }
}
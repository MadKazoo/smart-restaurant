namespace Places.Model.Domain
{
    public class RegistrationViewModel
    {
        public string Name { get; set; }
        public string Password { get; set; }
    }
}
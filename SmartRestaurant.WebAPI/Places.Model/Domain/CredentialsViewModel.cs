namespace Places.Model.Domain
{
    public class CredentialsViewModel
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
using Places.Model.Domain;
using Places.Model.Entity;

namespace Places.Model.Mapping
{
    public static class OrderMapping
    {
        public static OrderDto EntitityToDomain(this OrderEntity order)
        {
            var orderDto = new OrderDto
            {
                Id = order.Id,
                OrderStatus = order.OrderStatus,
                PlaceId = order.Place.Id,
                PlaceName = order.Place.Name,
                DateCreated = order.DateCreated,
                GroupId = order.GroupId.ToString(),
                PartialOrders = order.PartialOrders,
                TableNumber = order.Table.TableNumber,
                UserOrderStatuses = order.UserOrderStatuses,
                Price = order.Price
            };
            return orderDto;
        }
    }
}
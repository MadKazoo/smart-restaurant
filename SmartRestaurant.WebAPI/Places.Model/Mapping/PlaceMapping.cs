using System.Linq;
using GeoAPI.Geometries;
using Places.Model.Domain;
using Places.Model.Entity;

namespace Places.Model.Mapping
{
    public static class PlaceMapping
    {
        public static PlaceDto EntityToDomain(this PlaceEntity place)
        {
            var placeDomain = new PlaceDto
            {
                Id = place.Id,
                Name = place.Name,
                Description = place.Description,
                ImageUrl = place.ImageUrl,
                PlaceTags = place.PlaceTypes?.Select(x => x.CategoryType),
                coordinates = new GeoCoordinates()
                {
                    AddressName =place?.Address,
                    Longitude = place.Location!=null ? place.Location.Coordinate.X : 0,
                    Latitude = place.Location !=null ? place.Location.Coordinate.Y : 0
                },
                NumberOfTables = place.Tables?.Count() ?? 0
            };
            return placeDomain;
        }
    }
}
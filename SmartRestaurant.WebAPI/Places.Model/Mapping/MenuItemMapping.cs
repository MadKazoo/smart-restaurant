using Places.Model.Domain;
using Places.Model.Entity;

namespace Places.Model.Mapping
{
    public static class MenuItemMapping
    {
        public static MenuItemModel EntityToDomain(this MenuItemEntity menuItemEntity)
        {
            var menuItemDomain = new MenuItemModel
            {
                Id = menuItemEntity.Id,
                Name = menuItemEntity.Name,
                Description = menuItemEntity.Description,
                Price = menuItemEntity.Price,
                ImageUrl = menuItemEntity.ImageUrl
            };
            return menuItemDomain;
        }

        public static MenuItemEntity DomainToEntitity(this MenuItemModel menuItem, PlaceMenuEntity placeMenuEntity)
        {
            var menuitemEntity = new MenuItemEntity
            {
                Name = menuItem.Name,
                Price = menuItem.Price,
                ImageUrl = menuItem.ImageUrl,
                PlaceMenuId = placeMenuEntity.Id,
                Description = menuItem.Description,
                PlaceMenuEntity = placeMenuEntity
            };

            return menuitemEntity;
        }
    }
}
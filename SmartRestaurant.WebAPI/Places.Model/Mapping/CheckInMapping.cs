using System.Collections.Generic;
using System.Linq;
using Places.Model.Domain;
using Places.Model.Entity;

namespace Places.Model.Mapping
{
    public static class CheckInMapping
    {
        public static CheckInDto MapToDto(this CheckInEntity checkInEntity, IEnumerable<ClientDTO> clients)
        {
            var checkInDto = new CheckInDto
            {
                CheckedInUsers = clients.ToList(),
                Id = checkInEntity.Id,
                TableId = checkInEntity.Table.TableNumber,
                PartialOrders = checkInEntity.PartialOrders,
                PlaceId = checkInEntity.Place.Id
            };

            return checkInDto;
        }
        public static CheckInDto MapToDto(this CheckInEntity checkInEntity)
        {
            var clients = checkInEntity.UsersReadyForOrder.Select(z => z.AppUserEntity.MapToUser());
            var checkInDto = new CheckInDto
            {
                CheckedInUsers = clients.ToList(),
                Id = checkInEntity.Id,
                TableId = checkInEntity.Table.TableNumber,
                PartialOrders = checkInEntity.PartialOrders,
                PlaceId = checkInEntity.Place.Id
            };

            return checkInDto;
        }
    }
}
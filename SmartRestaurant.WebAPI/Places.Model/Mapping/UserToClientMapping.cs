using System.Collections.Generic;
using System.Linq;
using Places.Model.Domain;
using Places.Model.Entity;

namespace Places.Model.Mapping
{
    public static class UserToClientMapping
    {
        public static ClientDTO MapToUser(this AppUserEntity user)
        {
            var clientDto = new ClientDTO
            {
                Name = user.Login,
                ImageIcon = user.ImageIcon,
                Id = user.Id
            };


            return clientDto;
        }

        public static ClientDTO MapToUserWithStatus(this AppUserEntity user,
            IEnumerable<UserCheckInStatus> userOrderStatus)
        {
            var userStatus = userOrderStatus.FirstOrDefault(x => x.AppUserEntity.Id == user.Id);
            var clientDto = new ClientDTO
            {
                Name = user.Login,
                ImageIcon = user.ImageIcon,
                Id = user.Id
            };

            if (userStatus != null)
            {
                clientDto.AmountToPay = userStatus.ToPay;
                clientDto.IsReadyForOrder = userStatus.Status == UserCheckInStatusEnum.READY;
            }

            return clientDto;
        }
    }
}
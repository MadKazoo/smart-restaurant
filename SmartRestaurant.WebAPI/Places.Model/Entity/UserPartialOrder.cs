using System;

namespace Places.Model.Entity
{
    public class UserPartialOrder
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public decimal ClientPercentShare { get; set; }

        public decimal ToPayInMoney { get; private set; }

        public long AppUserId { get; set; }

        public void SetClientMenuItemPercentShare(decimal menuItemPrice, decimal value)
        {
            ClientPercentShare = Math.Round(value, 2);
            if (menuItemPrice != 0) ToPayInMoney = Math.Round(menuItemPrice * ClientPercentShare / 100, 2);
        }

        public void SetToPayInMoney(decimal menuItemPrice, decimal value)
        {
            ToPayInMoney = Math.Round(value, 2);
            if (menuItemPrice != 0) ClientPercentShare = Math.Round(ToPayInMoney / menuItemPrice * 100, 2);
        }
    }
}
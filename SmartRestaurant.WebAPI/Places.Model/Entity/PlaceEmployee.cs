namespace Places.Model.Entity
{
    public class PlaceEmployee
    {
        public int Id { get; set; }
        public Employee Employee { get; set; }
        public PlaceEntity Place { get; set; }

        public EmployeePermission EmployeePermission { get; set; }
    }

    public enum EmployeePermission
    {
        ADMIN = 0
    }
}
namespace Places.Model.Entity
{
    public enum UserCheckInStatusEnum
    {
        NEW = 0,
        READY = 1,
        FINISHED = 2
    }
}
using System.Collections.Generic;

namespace Places.Model.Entity
{
    public class PartialOrder
    {
        public long Id { get; set; }
        public MenuItemEntity MenuItem { get; set; }
        public IEnumerable<UserPartialOrder> UserPartialOrders { get; set; }
    }
}
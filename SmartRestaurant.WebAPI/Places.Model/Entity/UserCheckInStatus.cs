namespace Places.Model.Entity
{
    public class UserCheckInStatus
    {
        public long Id { get; set; }
        public AppUserEntity AppUserEntity { get; set; }
        public UserCheckInStatusEnum Status { get; set; }
        public decimal ToPay { get; set; }
    }
}
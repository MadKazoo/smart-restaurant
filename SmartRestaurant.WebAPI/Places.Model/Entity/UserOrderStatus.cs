namespace Places.Model.Entity
{
    public class UserOrderStatus
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        public bool HasPaid { get; set; }
        public decimal ToPay { get; set; }
    }
}
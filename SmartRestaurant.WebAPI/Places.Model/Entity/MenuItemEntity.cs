namespace Places.Model.Entity
{
    public class MenuItemEntity
    {
        public int Id { get; set; }
        public int PlaceMenuId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public string ImageUrl { get; set; }
        public PlaceMenuEntity PlaceMenuEntity { get; set; }
    }
}
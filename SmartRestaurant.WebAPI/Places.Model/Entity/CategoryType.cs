namespace Places.Model.Entity
{
    public class CategoryType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ImageSrc { get; set; }
    }
}
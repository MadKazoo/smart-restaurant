using System.Collections.Generic;

namespace Places.Model.Entity
{
    public class PlaceMenuEntity
    {
        public int Id { get; set; }
        public int PlaceId { get; set; }
        public IEnumerable<MenuItemEntity> MenuItems { get; set; }
    }
}
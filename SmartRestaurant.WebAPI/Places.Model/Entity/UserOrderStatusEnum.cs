namespace Places.Model.Entity
{
    public enum UserOrderStatusEnum
    {
        NEW = 0,
        ACCEPTED = 1,
        PAID = 2,
        COMPLETED = 3
    }
}
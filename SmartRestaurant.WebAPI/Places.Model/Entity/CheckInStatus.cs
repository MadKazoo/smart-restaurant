namespace Places.Model.Entity
{
    public enum CheckInStatus
    {
        NEW = 0,
        FINISHED = 1,
        CANCELED = 2
    }
}
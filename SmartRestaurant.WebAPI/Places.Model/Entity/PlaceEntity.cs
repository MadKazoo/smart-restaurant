﻿using System.Collections.Generic;
using NetTopologySuite.Geometries;

namespace Places.Model.Entity
{
    public class PlaceEntity
    {
        public PlaceEntity()
        {
        }

        public PlaceEntity(PlaceEntity placeEntity)
        {
            Id = placeEntity.Id;
            Key = placeEntity.Key;
            Name = placeEntity.Name;
            Description = placeEntity.Description;
            Location = new Point(placeEntity.Location.Coordinate);
        }

        public string Key { get; set; }

        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public IEnumerable<PlaceTableEntity> Tables { get; set; }
        
        public ICollection<PlaceCategoryType> PlaceTypes { get; set; }

        public string ImageUrl { get; set; }

        public PlaceMenuEntity PlaceMenuEntity { get; set; }
        public double AvarageRating { get; set; }
        public Point Location { get; set; }
        
        public string Address { get; set; }

        public void UpdateValueByReflection(string propertyName, object value)
        {
            GetType().GetProperty(propertyName.ToUpperInvariant()).SetValue(this, value);
        }
    }
}
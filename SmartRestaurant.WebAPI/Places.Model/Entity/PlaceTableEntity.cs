namespace Places.Model.Entity
{
    public class PlaceTableEntity
    {
        
        public long Id { get; set; }

        public string Hash { get; set; }
        public int TableNumber { get; set; }
        public int PlaceId { get; set; }


        // public CheckInEntity CurrentlyCheckedIn { get; set; }
        // public IEnumerable<CheckInEntity> CheckInHistory { get; set; }
    }
}
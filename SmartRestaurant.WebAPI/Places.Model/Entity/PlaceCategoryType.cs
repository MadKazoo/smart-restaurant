namespace Places.Model.Entity
{
    public class PlaceCategoryType
    {
        public int Id { get; set; }
        public CategoryType CategoryType { get; set; }
    }
}
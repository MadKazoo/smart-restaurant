using System;
using System.Collections.Generic;
using System.Linq;

namespace Places.Model.Entity
{
    public class CheckInEntity
    {
        public Guid Id { get; set; }
        public List<UserCheckInStatus> UsersReadyForOrder { get; set; }
        public PlaceTableEntity Table { get; set; }
        public PlaceEntity Place { get; set; }
        public IEnumerable<PartialOrder> PartialOrders { get; set; }
        public CheckInStatus Status { get; set; }
        public DateTime DateCreated { get; set; }

        public bool AreAllUsersReadyToPay()
        {
            return UsersReadyForOrder.All(x => x.Status == UserCheckInStatusEnum.READY);
        }

        public decimal GetAllMenuItemsPrice()
        {
            var sum = PartialOrders.Sum(x => x.MenuItem.Price);
            return sum;
        }
    }
}
using System;

namespace Places.Model.Entity
{
    public class CheckedInUsers
    {
        public long Id { get; set; }
        public Guid CheckInId { get; set; }
        public AppUserEntity User { get; set; }
    }
}
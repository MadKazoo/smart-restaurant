using System;
using System.Collections.Generic;

namespace Places.Model.Entity
{
    public class OrderEntity
    {
        public int Id { get; set; }
        public Guid GroupId { get; set; }
        public IEnumerable<PartialOrder> PartialOrders { get; set; }
        public IEnumerable<UserOrderStatus> UserOrderStatuses { get; set; }
        public OrderStatus OrderStatus { get; set; }
        public PlaceEntity Place { get; set; }
        public PlaceTableEntity Table { get; set; }
        public DateTime DateCreated { get; set; }
        public decimal Price { get; set; }
    }
}
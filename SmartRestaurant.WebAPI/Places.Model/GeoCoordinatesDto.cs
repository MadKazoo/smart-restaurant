﻿namespace Places.Model
{
    public class GeoCoordinates
    {
        public int Id { get; set; }
        public string AddressName { get; set; }
        public double Latitude { get; set; }

        public double Longitude { get; set; }
    }
}
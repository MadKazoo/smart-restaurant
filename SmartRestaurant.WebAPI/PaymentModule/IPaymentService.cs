using Places.Model.Entity;

namespace PaymentModule
{
    public interface IPaymentService
    {
        OrderEntity SetUserOrderPaid(long userId, int orderId);
    }
}
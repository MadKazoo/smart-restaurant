using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace PaymentModule
{
    public static class StartupExtensions
    {
        public static void RegisterPaymentModuleDependencies(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddTransient<IPaymentService, PaymentService>();
        }
    }
}
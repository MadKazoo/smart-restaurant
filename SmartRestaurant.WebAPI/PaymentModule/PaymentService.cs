using System.Linq;
using Places.Model;
using Places.Model.Entity;
using Shared.Interfaces;

namespace PaymentModule
{
    public class PaymentService : IPaymentService
    {
        private readonly IOrderRepository _orderRepository;

        public PaymentService(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public OrderEntity SetUserOrderPaid(long userId, int orderId)
        {
            var order = _orderRepository.GetOrderWithUserStatuses(orderId, userId);
            order.UserOrderStatuses.First(x => x.UserId == userId).HasPaid = true;

            if (order.UserOrderStatuses.All(x => x.HasPaid)) order.OrderStatus = OrderStatus.PAID;

            _orderRepository.Update(order);
            _orderRepository.SaveChanges();

            return order;
        }
    }
}
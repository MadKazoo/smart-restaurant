using System;
using System.Text;
using System.Threading.Tasks;
using CheckInModule.Helpers;
using Menu.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using OrderModule;
using OrderModule.Helpers;
using PaymentModule;
using Persistence;
using PlaceModule;
using Shared.Helpers;
using SmartRestaurant.WebAPI.Controllers.WebSocketControllers;
using Swashbuckle.AspNetCore.Swagger;
using UserModule;
using UserModule.Helpers;

namespace SmartRestaurant.WebAPI
{
    public class Startup
    {
        private const string _corsPolicyName = "AllowAll";
        private const string SecretKey = "iNivDmHLpUA223sqsfhqGbMRdRj1PVkH"; // todo: get this from somewhere secure

        private readonly SymmetricSecurityKey
            _signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(SecretKey));

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<SmartRestaurantDbContext>(opts => opts.UseSqlServer(Configuration["ConnectionString:master"], 
                o => o.UseNetTopologySuite()));

            services.RegisterUserModuleDependencies(Configuration);
            services.RegisterSharedModuleDependencies(Configuration);
            services.RegisterPlaceModuleDependencies(Configuration);
            services.RegisterPaymentModuleDependencies(Configuration);
            services.RegisterOrderModuleDependencies(Configuration);
            services.RegisterMenuModuleDependencies(Configuration);
            services.RegisterCheckInModuleDependencies(Configuration);
            
            services.AddSingleton<IUrlConfigProvider, UrlConfigProvider>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            AddJWTAuthIntoService(services);

            services.AddCors(options =>
            {
                options.AddPolicy(
                    _corsPolicyName,
                    x =>
                    {
                        x.AllowAnyHeader()
                            .AllowAnyMethod()
                            .SetIsOriginAllowed(_ => true)
                            .AllowCredentials();
                    });
            });

            services.AddSignalR();

            services.AddSwaggerGen(c => { c.SwaggerDoc("v1", new Info {Title = "My API", Version = "v1"}); });
        }

        public void AddJWTAuthIntoService(IServiceCollection services)
        {
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.RequireHttpsMetadata = false;
                options.SaveToken = true;
                options.Authority = "https://localhost:5001/RestaurantHub";

                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateAudience = false,
                    ValidateIssuer = false,
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(SecretKey)),
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.Zero //the default for this setting is 5 minutes
                };

                options.Events = new JwtBearerEvents
                {
                    OnAuthenticationFailed = context =>
                    {
                        if (context.Exception.GetType() == typeof(SecurityTokenExpiredException))
                        {
                            context.Response.Headers.Add("Token-Expired", "true");
                        }
                        return Task.CompletedTask;
                    }
                };
            });

        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            else
                app.UseHsts();
            
            app.UseWebSockets();
            app.UseAuthentication();

            app.UseHttpsRedirection();

            app.UseSwagger();
            app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1"); });

            app.UseCors(_corsPolicyName);

            app.UseSignalR(routes =>
            {
                routes.MapHub<PlaceOrderPreparationHub>("/RestaurantHub");
                routes.MapHub<CheckInHub>("/ClientsHub");
            });


            app.UseMvc();

            app.UseDefaultFiles();
            app.UseStaticFiles();
        }
    }
}
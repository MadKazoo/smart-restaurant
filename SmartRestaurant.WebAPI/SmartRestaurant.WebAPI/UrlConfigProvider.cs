using Microsoft.Extensions.Configuration;

namespace SmartRestaurant.WebAPI
{
    public class UrlConfigProvider : IUrlConfigProvider
    {
        public UrlConfigProvider(IConfiguration configuration)
        {
            checkInHubUrl = $"{configuration["Data:checkInHubUrl"]}";
            placeOrderPreparationHub = $"{configuration["Data:placeOrderPreparationHub"]}";
        }

        public string checkInHubUrl { get; private set;  }
        public string placeOrderPreparationHub { get; private set; }
        
        public string connectionString { get; private set; }
    }


    public interface IUrlConfigProvider
    {
        string checkInHubUrl { get; }
        string placeOrderPreparationHub { get; }
    }
}
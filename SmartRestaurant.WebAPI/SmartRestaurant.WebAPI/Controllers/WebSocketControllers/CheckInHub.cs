using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CheckInModule.Interfaces;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.SignalR.Client;
using OrderModule.Interfaces;
using Places.Model.Domain;
using Places.Model.Entity;
using Places.Model.Mapping;

namespace SmartRestaurant.WebAPI.Controllers.WebSocketControllers
{
    [EnableCors("AllowAll")]
    partial class CheckInHub : Hub
    {
        private readonly ICheckInService _checkInService;
        private readonly IClientOrderService _clientOrderService;
        private readonly IRestaurantProceedingOrderService _proceedingOrderService;
        


        public CheckInHub(ICheckInService checkInService, IClientOrderService clientOrderService,
            IRestaurantProceedingOrderService proceedingOrderService)
        {
            _checkInService = checkInService;
            _clientOrderService = clientOrderService;
            _proceedingOrderService = proceedingOrderService;
        }

        public async Task CheckIn(CheckInViewModel checkInViewModel)
        {
            // TODO transform to Chain of responsibility
            if (!string.IsNullOrEmpty(checkInViewModel.Hash))
            {
                var placeAndTableId = _checkInService.GetPlaceAndTableId(checkInViewModel.Hash);
                checkInViewModel.PlaceId = placeAndTableId.Item1;
                checkInViewModel.PlaceId = placeAndTableId.Item2;
            }

            var checkIn = _checkInService.GetCurrentCheckInForTable(checkInViewModel.TableNumber);

            if (checkIn == null)
            {
                var groupId = Guid.NewGuid();
                await Groups.AddToGroupAsync(Context.ConnectionId, groupId.ToString());
                checkIn = _checkInService.AddNewCheckIn(checkInViewModel.PlaceId, checkInViewModel.TableNumber,
                    checkInViewModel.ClientId, groupId);
            }
            else if (IsContainingClient(checkIn, checkInViewModel))
            {
                await Groups.AddToGroupAsync(Context.ConnectionId, checkIn.Id.ToString());
                _checkInService.CheckIn(checkIn, checkInViewModel.ClientId);
            }
            else
            {
                await Groups.AddToGroupAsync(Context.ConnectionId, checkIn.Id.ToString());
            }

            if (checkIn == null) throw new HubException("This error will be sent to the client!");
            SendCheckInToGroup(checkIn);
            SendCheckInToPreparationHub(checkIn);

        }

        public async Task CheckOut(string checkInId, long userId)
        {
            var g = new Guid(checkInId);
            var checkIn = _checkInService.CheckOut(g, userId);
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, checkInId);
            await SendCheckInToGroup(checkIn);
        }

        public async Task<CheckInDto> GetCurrentCheckInForUser(long userId)
        {
            var checkIn = _checkInService.GetCurrentCheckInForUser(userId);
            return checkIn;
        }

        public async Task AddItemToOrder(string checkInId, long userId, MenuItemModel menuItem)
        {
            var g = new Guid(checkInId);
            var checkIn = _clientOrderService.AddToOrder(g, userId, menuItem.Id);
            await SendCheckInToGroup(checkIn);
            SendCheckInToPreparationHub(checkIn);
        }

        public async Task JoinToPartialOrder(string checkInId, long partialOrderId, long userId, decimal moneyToPay)
        {
            var g = new Guid(checkInId);
            var checkIn = _clientOrderService.AddUserToPartialOrder(g, partialOrderId, userId, moneyToPay);
            await SendCheckInToGroup(checkIn);
            SendCheckInToPreparationHub(checkIn);
        }

        public async Task ChangePartialOrderPercentShare(string checkInId, long partialOrderId, long userId,
            decimal clientMoneyShare)
        {
            var g = new Guid(checkInId);
            var checkIn =
                _clientOrderService.SetUserPartialOrderPercentShare(g, partialOrderId, userId, clientMoneyShare);
            await SendCheckInToGroup(checkIn);
            SendCheckInToPreparationHub(checkIn);
        }

        public async Task ResignFromPartialOrder(string checkInId, long partialOrderId, long userId)
        {
            var g = new Guid(checkInId);
            var checkIn = _clientOrderService.RemoveUserFromPartialOrder(g, partialOrderId, userId);
            await SendCheckInToGroup(checkIn);
            SendCheckInToPreparationHub(checkIn);
        }

        public async Task SetUserStatusToCheckIn(string checkInId, long userId, bool isReadyToCheckOut)
        {
            var g = new Guid(checkInId);
            var checkIn = _clientOrderService.SetUserStatusToCheckIn(g, userId, isReadyToCheckOut);

            if (checkIn.AreAllUsersReadyToPay())
            {
                _checkInService.CloseCheckIn(checkIn);
                var isOrderPaid = _clientOrderService.ProceedToPayment(checkIn);
                if (!isOrderPaid) return;

                var order = _clientOrderService.CreateOrder(checkIn);

                await SendCheckInToGroup(checkIn);
                await SendOrderToGroup(checkIn.Id.ToString(), order.EntitityToDomain());
                SendOrderToOrderPreparationHub(order);
            }
            else
            {
                await SendCheckInToGroup(checkIn);
                SendCheckInToPreparationHub(checkIn);
            }
        }

        private async Task SendCheckInToGroup(CheckInEntity checkIn)
        {
            if (checkIn == null) return;

            var checkInDto = _checkInService.GetCheckInDto(checkIn);
            await Clients.Group(checkIn.Id.ToString()).SendAsync("subscribeCheckIn", checkInDto);
        }

        public async Task SendOrderToGroup(string groupId, OrderDto order)
        {
            await Clients.Group(groupId).SendAsync("subscribeToNewOrder", order);
        }

        private HubConnection _connection;

        private async Task SendOrderToOrderPreparationHub(OrderEntity orderEntity)
        {
            // await StartConnection();
          
            await _connection.InvokeAsync("notifyAboutNewOrder", orderEntity.Place.Id);
        }

        private async Task SendCheckInToPreparationHub(CheckInEntity checkIn)
        {
//            await StartConnection();

           // var hubContext = GlobalHost.ConnectionManager.GetHubContext<PlaceOrderPreparationHub>();

            var connection = new HubConnectionBuilder()
                .WithUrl(GetOrderPreparationnHubUrl())
                .Build();
            await connection.StartAsync();
            
           // await connection.InvokeAsync("notifyAboutNewCheckIn", checkIn.Place.Id);
        }
        private async Task StartConnection()
        {
            if(_connection==null)
            {
                _connection = new HubConnectionBuilder()
                    .WithUrl(GetOrderPreparationnHubUrl())
                    .Build();
                await _connection.StartAsync();
            }

            if (_connection.State == HubConnectionState.Disconnected)
            {
                await _connection.StartAsync();
            }
        }
        private string GetOrderPreparationnHubUrl()
        {
            // TODO move to config file
            return "https://localhost/RestaurantHub";
        }

        public async Task SubscribeToOrdersInRestaurant(long placeId)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, placeId.ToString());
        }

        public async Task InvokeClientOrderSubscription(string groupId)
        {
//            Guid orderGroupId = _proceedingOrderServiceedingOrderService.GetOrderGroupId(orderId);


            var orderGroupId = groupId;
            await Groups.AddToGroupAsync(Context.ConnectionId, orderGroupId);
        }

        public async Task NotifyAboutNewOrder(long placeId)
        {
            var orders = _proceedingOrderService.GetCurrentlyProceededOrders(placeId);
            SendOrderToPlace(orders, placeId);
        }

        private async Task SendOrderToPlace(IEnumerable<OrderDto> orders, long placeId)
        {
            await Clients.Group(placeId.ToString()).SendAsync("subscribeToPlaceOrders", orders);
        }

        // used by PlaceOrdersController
        public async Task SendOrderToClients(OrderDto order)
        {
            await Clients.Group(order.GroupId).SendAsync("subscribeToOrder", order);
        }

        public async Task SendOrderToClientsByOrderId(int orderId)
        {
            var order = _clientOrderService.GetOrder(orderId);
            await Clients.Group(order.GroupId.ToString()).SendAsync("subscribeToOrder", order);
        }

        private async Task SendOrderToClients(IEnumerable<OrderDto> orders, long placeId)
        {
            foreach (var order in orders) await Clients.Group(order.GroupId).SendAsync("subscribeToOrders", order);
        }

        private bool IsContainingClient(CheckInEntity checkIn, CheckInViewModel checkInViewModel)
        {
            return checkIn.UsersReadyForOrder.All(x => x.Id != checkInViewModel.ClientId);
        }
    }
}
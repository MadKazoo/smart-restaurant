using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CheckInModule.Interfaces;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.SignalR;
using OrderModule.Interfaces;
using Places.Model.Domain;

namespace SmartRestaurant.WebAPI.Controllers.WebSocketControllers
{
    partial class CheckInHub
    {

        public async Task JoinToRestaurantCheckIns(long placeId)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, placeId.ToString());
        }

        // Move to distinct service
        public async Task JoinToRestaurantOrders(long placeId)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, placeId.ToString());
        }
        
        public async Task ResignFromOrdersSubscription(int placeId)
        {
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, placeId.ToString());
        }
        
        public async Task NotifyPlacesAboutNewOrder(long placeId)
        {
            var orders = _proceedingOrderService.GetCurrentlyProceededOrders(placeId);
            SendOrderToPlaces(orders, placeId);
        }
        
        public async Task NotifyAboutNewCheckIn(int placeId)
        {
            var checkIns = _checkInService.GetPlaceCheckIns(placeId);
            SendCheckInToPlaces(checkIns, placeId);
        }

        private async Task SendOrderToPlaces(IEnumerable<OrderDto> orders, long placeId)
        {
            await Clients.Group(placeId.ToString()).SendAsync("subscribeToPlaceOrders", orders);
        }
        
        private async Task SendCheckInToPlaces(IEnumerable<CheckInDto> checkIns, long placeId)
        {
            await Clients.Group(placeId.ToString()).SendAsync("subscribeToPlaceCheckIns", checkIns);
        }
    }
}
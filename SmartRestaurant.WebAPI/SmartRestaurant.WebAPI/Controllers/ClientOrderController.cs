using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR.Client;
using OrderModule.Interfaces;
using PaymentModule;
using Places.Model;
using Places.Model.Domain;
using Places.Model.Entity;
using Places.Model.Mapping;

namespace SmartRestaurant.WebAPI.Controllers
{
    [Route("Orders")]
    public class ClientOrderController : Controller
    {
        private readonly HubConnection _clientHubConnection;
        private readonly IClientOrderService _clientOrderService;
        private readonly IPaymentService _paymentService;
        private readonly HubConnection _placeHubConnection;
        private readonly IUrlConfigProvider _urlConfigProvider;

        public ClientOrderController(IClientOrderService clientOrderService, IPaymentService paymentService,
            IUrlConfigProvider urlConfigProvider)
        {
            _clientOrderService = clientOrderService;
            _paymentService = paymentService;
            _urlConfigProvider = urlConfigProvider;

            _clientHubConnection = new HubConnectionBuilder()
                .WithUrl(GetClientCheckInHubUrl())
                .Build();

            _placeHubConnection = new HubConnectionBuilder()
                .WithUrl(GetPlaceConnectionHubUrl())
                .Build();
        }


        private string GetClientCheckInHubUrl()
        {
            return _urlConfigProvider.checkInHubUrl;
        }

        private string GetPlaceConnectionHubUrl()
        {
            return _urlConfigProvider.placeOrderPreparationHub;
        }

        [HttpGet("{id}")]
        [EnableCors("AllowAll")]
        [ProducesResponseType(typeof(OrderEntity), 200)]
        public IActionResult GetOrder(int id)
        {
            var order = _clientOrderService.GetOrder(id).EntitityToDomain();
            return Ok(order);
        }

        [HttpGet("{orderId}/payment/{appUserId}")]
        [EnableCors("AllowAll")]
        [ProducesResponseType(typeof(OrderEntity), 200)]
        public async Task<IActionResult> GetPaymentUrl(int orderId, long appUserId)
        {
            var paymentUrl = _clientOrderService.GetPaymentUrl(orderId, appUserId);
            // TODO - remove when will have proper payment service
            await SetOrderAsPaid(new PaymentRequest {UserId = appUserId, OrderId = orderId, IsPaid = true});
            return Ok(paymentUrl);
        }

        [HttpPost("{orderId}/payment/{appUserId}")]
        [EnableCors("AllowAll")]
        public async Task<IActionResult> SetOrderAsPaid(PaymentRequest paymentRequest)
        {
            var order = _paymentService.SetUserOrderPaid(paymentRequest.UserId, paymentRequest.OrderId);

            if (order == null) return BadRequest(false);
            await _clientHubConnection.StartAsync();

            await _clientHubConnection.InvokeAsync("SendOrderToClientsByOrderId", order.Id);

            await _placeHubConnection.StartAsync();
            if (order.OrderStatus == OrderStatus.PAID)
                await _placeHubConnection.InvokeAsync("NotifyAboutNewOrder", order.Place.Id);

            return Ok(true);
        }

        [HttpGet("user/{appUserId}")]
        [EnableCors("AllowAll")]
        [ProducesResponseType(typeof(OrderEntity), 200)]
        public IActionResult GetCurrentlyProceededOrders(int appUserId)
        {
            var orders = _clientOrderService.GetCurrentlyProceededOrdersByUserId(appUserId);
            return Ok(orders);
        }
    }
}
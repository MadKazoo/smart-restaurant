using System;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using PlaceModule.Interfaces;
using Places.Model.Domain;

namespace SmartRestaurant.WebAPI.Controllers
{
    [Route("Place")]
    [Produces("application/json")]
    [ApiController]
    public class PlaceController : Controller
    {
        private readonly IPlaceService _placeService;

        public PlaceController(IPlaceService placeService)
        {
            _placeService = placeService;
        }

        [HttpPost("")]
        [EnableCors("AllowAll")]
        [ProducesResponseType(200)] 
        [Authorize]
        public IActionResult AddPlace([FromBody] PlaceDto place)
        {
             int.TryParse(User.Identity.Name, out var userId);
             _placeService.AddPlace(place, userId);
            return Ok();
        }
        
        [HttpGet("{placeId}/CheckIn")]
        [EnableCors("AllowAll")]
        [ProducesResponseType(200)]
        public IActionResult GetCurrentCheckIns(int placeId)
        {
            var checkIns = _placeService.GetPlaceCheckIns(placeId);
            return Ok(checkIns);
        }

        [HttpPost("{id}/tables")]
        [EnableCors("AllowAll")]
        [ProducesResponseType(200)]
        public IActionResult AddTableToPlace(int placeId, int tableNumber, int amountOfSeats)
        {
            _placeService.AddTableToPlace(placeId, tableNumber, amountOfSeats);
            return Ok();
        }

        [HttpPut("{id}")]
        [EnableCors("AllowAll")]
        [ProducesResponseType(200)]
        public IActionResult UpdatePlace([FromBody] PlaceDto placeDto)
        {
            var place = _placeService.UpdatePlace(placeDto);
            return Ok(place);
        }

        [HttpPut("{id}/{propertyName}")]
        [EnableCors("AllowAll")]
        [ProducesResponseType(200)]
        public IActionResult UpdatePlaceProperty(int id, string propertyName, [FromBody] object value)
        {
            throw new NotImplementedException();
        }


        [HttpGet("manage/user/{id}")]
        [Authorize]
        [EnableCors("AllowAll")]
        [ProducesResponseType(200)]
        public IActionResult GetPlaces(int id)
        {
            return Ok(_placeService.GetPlacesAccessibleToEmployee(id).ToList());
        }
    }
}
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR.Client;
using OrderModule.Interfaces;
using Places.Model.Domain;
using Places.Model.Mapping;

namespace SmartRestaurant.WebAPI.Controllers
{
    [Route("Place")]
    public class PlaceOrdersController : Controller
    {
        private readonly HubConnection _clientHubConnection;
        private readonly HubConnection _placeHubConnection;
        private readonly IRestaurantProceedingOrderService _restaurantProceedingOrderService;
        private readonly IUrlConfigProvider _urlConfigProvider;

        public PlaceOrdersController(IRestaurantProceedingOrderService restaurantProceedingOrderService,
            IUrlConfigProvider urlConfigProvider)
        {
            _restaurantProceedingOrderService = restaurantProceedingOrderService;
            _urlConfigProvider = urlConfigProvider;
            _clientHubConnection = new HubConnectionBuilder()
                .WithUrl(GetClientCheckInHubUrl())
                .Build();

            _placeHubConnection = new HubConnectionBuilder()
                .WithUrl(GetPlaceConnectionHubUrl())
                .Build();
        }

        private string GetClientCheckInHubUrl()
        {
            return _urlConfigProvider.checkInHubUrl;
        }

        private string GetPlaceConnectionHubUrl()
        {
            return _urlConfigProvider.placeOrderPreparationHub;
        }

        [HttpGet("{placeId}/currentOrders")]
        [EnableCors("AllowAll")]
        public IActionResult GetCurrentlyProcessedOrders(int placeId)
        {
            var orders = _restaurantProceedingOrderService.GetCurrentlyProceededOrders(placeId);
            return Ok(orders);
        }

        [HttpPost("{placeId}/orders/status")]
        [EnableCors("AllowAll")]
        public async Task<IActionResult> SetOrderStatus(int placeId,
            [FromBody] OrderStatusViewModel orderStatusViewModel)
        {
            await _clientHubConnection.StartAsync();

            var order = _restaurantProceedingOrderService.SetOrderStatus(orderStatusViewModel.OrderId,
                orderStatusViewModel.OrderStatus);

            await _clientHubConnection.InvokeAsync("SendOrderToClients", order.EntitityToDomain());
            await _placeHubConnection.InvokeAsync("NotifyAboutNewOrder", placeId);

            return Ok(true);
        }
    }
}
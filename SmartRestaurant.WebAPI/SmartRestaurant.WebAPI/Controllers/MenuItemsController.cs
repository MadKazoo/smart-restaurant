using Menu.Services.Interfaces;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Places.Model.Domain;

namespace SmartRestaurant.WebAPI.Controllers
{
    [Route("Place")]
    [Produces("application/json")]
    [ApiController]
    public class MenuItemsController : Controller
    {
        private readonly IMenuItemsService _menuItemsService;

        public MenuItemsController(IMenuItemsService menuItemsService)
        {
            _menuItemsService = menuItemsService;
        }

        [HttpPost("{placeId}/Menu")]
        [EnableCors("AllowAll")]
        [ProducesResponseType(200)]
        public IActionResult AddMenuItem(int placeId, [FromBody] MenuItemModel menuItemModel)
        {
            _menuItemsService.AddMenuItem(placeId, menuItemModel);
            return Ok();
        }

        [HttpDelete("{placeId}/Menu/{menuItemId}")]
        [EnableCors("AllowAll")]
        [ProducesResponseType(200)]
        public IActionResult RemoveItem(int placeId, int menuItemId)
        {
            _menuItemsService.RemoveMenuItem(placeId, menuItemId);
            return Ok();
        }

        [HttpGet("{placeId}/Menu")]
        [EnableCors("AllowAll")]
        [ProducesResponseType(200)]
        public IActionResult GetMenu(int placeId)
        {
            var menuItems = _menuItemsService.GetMenu(placeId);
            return Ok(menuItems);
        }

        [HttpPut("{placeId}/Menu/{menuItemId}")]
        [EnableCors("AllowAll")]
        [ProducesResponseType(200)]
        public IActionResult UpdateItem(int placeId, int menuItemId, [FromBody] MenuItemModel menuItemModel)
        {
            _menuItemsService.UpdateMenuItem(placeId, menuItemModel);
            return Ok();
        }
    }
}
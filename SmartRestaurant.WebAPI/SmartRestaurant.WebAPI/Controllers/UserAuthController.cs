using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Places.Model.Domain;
using Places.Model.Entity;
using UserModule;
using UserModule.Interfaces;

namespace SmartRestaurant.WebAPI.Controllers
{
    [Route("auth")]
    public class UserAuthController : Controller
    {
        private const string SecretKey = "iNivDmHLpUA223sqsfhqGbMRdRj1PVkH"; // todo: get this from somewhere secure

        private readonly IEmployeeService _employeeService;
        // private readonly SymmetricSecurityKey _signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(SecretKey));

        private readonly IUserAuthService _userAuthService;

        public UserAuthController(IUserAuthService userAuthService, IEmployeeService employeeService)
        {
            _userAuthService = userAuthService;
            _employeeService = employeeService;
        }

        [HttpGet("testJWT")]
        [Authorize]
        public async Task<IActionResult> TestJwt()
        {
            return Ok(new PlaceMenuEntity());
        }

        [HttpPost("register")]
        [EnableCors("AllowAll")]
        public async Task<IActionResult> RegisterUser([FromBody] RegistrationViewModel model)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            try
            {
                _userAuthService.RegisterUser(model);
            }
            catch (Exception e)
            {
                return BadRequest();
            }

            return new OkObjectResult("Account created");
        }

        [HttpPost("login")]
        [EnableCors("AllowAll")]
        public async Task<IActionResult> Login([FromBody] CredentialsViewModel credentials)
        {
            if (credentials == null) return BadRequest("Invalid client request");

            var user = _userAuthService.Authorize(credentials);
            if (user != null)
            {
                var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(SecretKey));
                var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);

                var tokeOptions = new JwtSecurityToken(
                    "http://smartrestaurant-webapi.azurewebsites.net",
                    "http://smartrestaurant-webapi.azurewebsites.net",
                    new List<Claim>(),
                    expires: DateTime.Now.AddMinutes(30),
                    signingCredentials: signinCredentials
                );

                var tokenString = new JwtSecurityTokenHandler().WriteToken(tokeOptions);
                return Ok(new {Token = tokenString, id = user.Id});
            }

            return Unauthorized();
        }

        [HttpPost("login/employee")]
        [EnableCors("AllowAll")] // TODO - CORS restricted policy needs to be applied only for employees
        public async Task<IActionResult> LoginEmployee([FromBody] CredentialsViewModel credentials)
        {
            if (credentials == null) return BadRequest("Invalid client request");

            var user = _employeeService.Authorize(credentials);
            if (user != null)
            {
                var claim = new Claim(ClaimTypes.Name, user.Id.ToString());

                var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(SecretKey));
                var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);

                var tokeOptions = new JwtSecurityToken(
                    "http://smartrestaurant-webapi.azurewebsites.net",
                    "http://smartrestaurant-webapi.azurewebsites.net",
                    new List<Claim>(){claim},
                    expires: DateTime.Now.AddMinutes(240),
                    signingCredentials: signinCredentials
                );

                var tokenString = new JwtSecurityTokenHandler().WriteToken(tokeOptions);
                return Ok(new {Token = tokenString, id = user.Id});
            }

            return Unauthorized();
        }
    }
}
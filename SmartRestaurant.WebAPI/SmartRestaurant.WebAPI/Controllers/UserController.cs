using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR.Client;
using OrderModule.Interfaces;
using Places.Model.Entity;
using SmartRestaurant.WebAPI.Controllers.WebSocketControllers;

namespace SmartRestaurant.WebAPI.Controllers
{
    [Route("User")]
    public class UserController : Controller
    {
        private readonly IClientOrderService _clientOrderService;
        private readonly PlaceOrderPreparationHub _preparationHub;

        public UserController(IClientOrderService clientOrderService, PlaceOrderPreparationHub preparationHub)
        {
            _clientOrderService = clientOrderService;
            _preparationHub = preparationHub;
        }

        [HttpGet("{userId}/Orders")]
        [EnableCors("AllowAll")]
        [ProducesResponseType(typeof(OrderEntity), 200)]
        public IActionResult GetOrders(int userId)
        {
            var orders = _clientOrderService.GetCurrentlyProceededOrdersByUserId(userId);
            return Ok(orders);
        }
        [HttpGet("TestHubConnections")]
        [EnableCors("AllowAll")]
        public  IActionResult StartConnection()
        {
//            var _connection = new HubConnectionBuilder()
//                    .WithUrl(GetOrderPreparationnHubUrl())
//                    .Build();
//             await _connection.StartAsync();
            _preparationHub.NotifyAboutNewCheckIn(4);
            return Ok();
        }
        private string GetOrderPreparationnHubUrl()
        {
            // TODO move to config file
            return "https://localhost/ClientsHub";
        }
    }
}
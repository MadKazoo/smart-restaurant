using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using PlaceModule.Interfaces;
using Places.Model.Entity;

namespace SmartRestaurant.WebAPI.Controllers
{
    [Route("PlaceType")]
    [Produces("application/json")]
    [ApiController]
    public class PlaceTypeController : Controller
    {
        private readonly IPlaceTypeService _placeTypeService;

        public PlaceTypeController(IPlaceTypeService placeTypeService)
        {
            _placeTypeService = placeTypeService;
        }


        [HttpGet("")]
        [EnableCors("AllowAll")]
        [ProducesResponseType(200)]
        public IActionResult GetPlaceTypes()
        {
            var placeTypes = _placeTypeService.GetPlaceTypes();
            return Ok(placeTypes);
        }


        [HttpPost("")]
        [EnableCors("AllowAll")]
        [ProducesResponseType(200)]
        public IActionResult AddPlaceType([FromBody] CategoryType categoryType)
        {
            _placeTypeService.AddPlaceType(categoryType);
            return Ok();
        }
    }
}
using CheckInModule.Interfaces;
using CheckInModule.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace SmartRestaurant.WebAPI.Controllers
{
    [Route("CheckIn")]
    [Produces("application/json")]
    [ApiController]
    public class ClientCheckInController : Controller
    {
        private readonly ICheckInService _checkInService;

        public ClientCheckInController(ICheckInService checkInService)
        {
            _checkInService = checkInService;
        }

        [HttpGet("")]
        [EnableCors("AllowAll")]
        public IActionResult Test()
        {
            return Ok("Test");
        }
        
        // GET
        [HttpGet("user/{userId}")]
        [EnableCors("AllowAll")]
        public IActionResult GetCurrentlyProcessedCheckIn(int userId)
        {
            var checkIn = _checkInService.GetCurrentCheckInForUser(userId);
            return Ok(checkIn);
        }
    }
}
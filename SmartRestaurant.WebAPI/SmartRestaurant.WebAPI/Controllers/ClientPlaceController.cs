﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using PlaceModule.Interfaces;
using Places.Model.Domain;
using Places.Model.Entity;

namespace SmartRestaurant.WebAPI.Controllers
{
    [Route("Place")]
    public class ClientPlaceController : Controller
    {
        private readonly IClientPlaceService _clientPlaceService;

        public ClientPlaceController(IClientPlaceService clientPlaceService)
        {
            _clientPlaceService = clientPlaceService;
        }

        [HttpGet("{id}")]
        public PlaceEntity Get(int id)
        {
            var place = _clientPlaceService.GetPlace(id);
            return place;
        }

        [HttpGet("qr-code/{hash}")]
        [EnableCors("AllowAll")]
        public IActionResult GetByQrCode(string hash)
        {
            var place = _clientPlaceService.GetPlaceAndTableId(hash);
            return Ok(place);
        }

        [HttpGet("{idFrom}/{idTo}")]
        [EnableCors("AllowAll")]
        public IEnumerable<PlaceEntity> GetFrom(int idFrom, int idTo)
        {
            var places = _clientPlaceService.GetPlaces(idFrom, idTo);
            return places;
        }


        [HttpGet("types")]
        [EnableCors("AllowAll")]
        public IEnumerable<CategoryType> GetPlacesTypes()
        {
            var places = _clientPlaceService.GetPlaceTypes();
            return places;
        }

        [HttpGet("")]
        [EnableCors("AllowAll")]
        public ActionResult<IEnumerable<PlaceDto>> Get()
        {
            var places = _clientPlaceService.GetPlaces();
            var p = places.ToList();
            return Ok(p);
        }

        [HttpGet("{lat}/{lon}/{distanceInMeters}")]
        [EnableCors("AllowAll")]
        public ActionResult<IEnumerable<PlaceDto>> GetNearbyPlaces(double lat, double lon, int distanceInMeters)
        {
            try
            {
                var places = _clientPlaceService.GetNearbyPlaces(lat, lon, distanceInMeters);
                return Ok(places);
            }
            catch (Exception e)
            {
                return BadRequest("Invalid data");
            }
        }
        
        [HttpGet("nearest/{lat}/{lon}/{amount}")]
        [EnableCors("AllowAll")]
        public ActionResult<IEnumerable<PlaceDto>> GetFewNearest(double lat, double lon, int amount)
        {
            try
            {
                var places = _clientPlaceService.GetNearest(lat, lon, amount);
                return Ok(places);
            }
            catch (Exception e)
            {
                return BadRequest("Invalid data");
            }
        }
    }
}
using System.Collections.Generic;
using Places.Model.Domain;

namespace Menu.Services.Interfaces
{
    public interface IMenuItemsService
    {
        IEnumerable<MenuItemModel> GetMenu(int placeId);
        void AddMenuItem(int placeId, MenuItemModel menuItemModel);

        void RemoveMenuItem(int placeId, int menuItemId);

        void UpdateMenuItem(int placeId, MenuItemModel menuItemModel);
    }
}
using Menu.Services.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Shared.Interfaces;
using Shared.Repositories;

namespace Menu.Services
{
    public static class StartupExtensions
    {
        public static void RegisterMenuModuleDependencies(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddTransient<IMenuItemsService, MenuItemsService>();
        }
    }  
    }
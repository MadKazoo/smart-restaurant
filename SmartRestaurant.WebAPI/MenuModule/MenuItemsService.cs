using System.Collections.Generic;
using System.Linq;
using Menu.Services.Interfaces;
using Places.Model.Domain;
using Places.Model.Entity;
using Places.Model.Mapping;
using Shared.Interfaces;

namespace Menu.Services
{
    public class MenuItemsService : IMenuItemsService
    {
        private readonly IPlaceRepository _placeRepository;

        public MenuItemsService(IPlaceRepository placeRepository)
        {
            _placeRepository = placeRepository;
        }

        public IEnumerable<MenuItemModel> GetMenu(int placeId)
        {
            var placeMenuEntity = _placeRepository.GetWithMenu(x => x.Id == placeId)?.PlaceMenuEntity;
            return placeMenuEntity?.MenuItems.Select(x => x.EntityToDomain());
        }

        public void AddMenuItem(int placeId, MenuItemModel menuItemModel)
        {
            var place = _placeRepository.GetWithMenu(x => x.Id == placeId);
            var menuItems = place.PlaceMenuEntity.MenuItems;
            if (menuItems == null) menuItems = new List<MenuItemEntity>();

            var menuItem = menuItemModel.DomainToEntitity(place.PlaceMenuEntity);
            var list = menuItems.ToList();
            list.Add(menuItem);
            menuItems = list;
            place.PlaceMenuEntity.MenuItems = menuItems.ToList();
            _placeRepository.SaveChanges();
        }

        public void RemoveMenuItem(int placeId, int menuItemId)
        {
            var place = _placeRepository.GetWithMenu(x => x.Id == placeId);
            var menuItems = place.PlaceMenuEntity.MenuItems;
            menuItems = menuItems.Where(x => x.Id != menuItemId);
            place.PlaceMenuEntity.MenuItems = menuItems;
            _placeRepository.SaveChanges();
        }

        public void UpdateMenuItem(int placeId, MenuItemModel menuItemModel)
        {
            var place = _placeRepository.GetWithMenu(x => x.Id == placeId);
            var menuItems = place.PlaceMenuEntity.MenuItems;
            var domainToEntitity = menuItems.FirstOrDefault(x => x.Id == menuItemModel.Id);
            domainToEntitity = menuItemModel.DomainToEntitity(place.PlaceMenuEntity);
            place.PlaceMenuEntity.MenuItems = menuItems;
            _placeRepository.SaveChanges();
        }
    }
}
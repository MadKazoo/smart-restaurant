using System;
using System.Collections.Generic;
using Places.Model.Entity;

namespace CheckInModule.Interfaces
{
    public interface IJoiningCheckInLogic
    {
        CheckInEntity AddNewCheckIn(PlaceEntity place, PlaceTableEntity tableEntity, AppUserEntity user,
            Guid groupId);

        void JoinToExistingCheckIn(long userId, IEnumerable<AppUserEntity> users, CheckInEntity checkIn);
    }
}
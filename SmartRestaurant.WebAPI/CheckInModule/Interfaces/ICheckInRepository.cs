using Places.Model.Entity;
using Shared.Repository;

namespace CheckInModule.Interfaces
{
    public interface ICheckInRepository : IRepositoryBase<CheckInEntity>
    {
    }
}
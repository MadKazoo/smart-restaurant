using Places.Model.Entity;

namespace CheckInModule.Interfaces
{
    public interface IClientCheckInService
    {
        AppUserEntity GetUser(long userId);
    }
}
using System;
using System.Collections.Generic;
using Places.Model.Domain;
using Places.Model.Entity;

namespace CheckInModule.Interfaces
{
    public interface ICheckInService
    {
        void CheckIn(CheckInEntity checkIn, long user);

        CheckInEntity AddNewCheckIn(int placeId, long tableId, long user, Guid GroupId);

        Tuple<int, int> GetPlaceAndTableId(string tableHash);

        CheckInEntity GetCurrentCheckInForTable(int tableNumber);

        IEnumerable<CheckInDto> GetPlaceCheckIns(int placeId);

        CheckInEntity CheckOut(Guid checkInId, long user);

        CheckInDto GetCurrentCheckInForUser(long userId);

        CheckInDto GetCheckInDto(CheckInEntity checkIn);

        void CloseCheckIn(CheckInEntity checkIn);
    }
}
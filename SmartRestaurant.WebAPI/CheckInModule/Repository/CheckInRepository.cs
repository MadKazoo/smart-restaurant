using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using CheckInModule.Interfaces;
using Microsoft.EntityFrameworkCore;
using Persistence;
using Places.Model.Entity;
using Shared.Repository;

namespace CheckInModule.Repository
{
    public class CheckInRepository : RepositoryBase<CheckInEntity>, ICheckInRepository
    {
        public CheckInRepository(SmartRestaurantDbContext databaseContext) : base(databaseContext)
        {
        }

        CheckInEntity IRepositoryBase<CheckInEntity>.Get(Expression<Func<CheckInEntity, bool>> where)
        {
            return _dbSet.Where(where)
                .Include(x => x.PartialOrders)
                .ThenInclude(z => z.UserPartialOrders)
                .Include(x => x.PartialOrders)
                .ThenInclude(z => z.MenuItem)
                .Include(z => z.UsersReadyForOrder)
                .ThenInclude(x => x.AppUserEntity)
                .Include(y => y.Place)
                .Include(x => x.Table)
                .FirstOrDefault();
        }
        
        IEnumerable<CheckInEntity> IRepositoryBase<CheckInEntity>.GetMany(Expression<Func<CheckInEntity, bool>> where)
        {
            return _dbSet.Where(where)
                .Include(x => x.PartialOrders)
                .ThenInclude(z => z.UserPartialOrders)
                .Include(x => x.PartialOrders)
                .ThenInclude(z => z.MenuItem)
                .Include(z => z.UsersReadyForOrder)
                .ThenInclude(x => x.AppUserEntity)
                .Include(y => y.Place)
                .Include(x => x.Table);
        }

        CheckInEntity IRepositoryBase<CheckInEntity>.GetById(Guid id)
        {
            return _dbSet
                .Include(x => x.Table)
                .Include(x => x.PartialOrders)
                .ThenInclude(z => z.UserPartialOrders)
                .Include(x => x.PartialOrders)
                .ThenInclude(z => z.MenuItem)
                .Include(z => z.UsersReadyForOrder)
                .ThenInclude(x => x.AppUserEntity)
                .Include(x => x.Place)
                .FirstOrDefault(x => x.Id == id);
        }
    }
}
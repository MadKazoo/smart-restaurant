using CheckInModule.BusinessLogic;
using CheckInModule.Interfaces;
using CheckInModule.Repository;
using CheckInModule.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CheckInModule.Helpers
{
    public static class StartupExtensions
    {
        public static void RegisterCheckInModuleDependencies(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddTransient<ICheckInRepository, CheckInRepository>();
            services.AddTransient<ICheckInService, CheckInService>();
            services.AddTransient<IClientCheckInService, ClientCheckInService>();
            services.AddTransient<IJoiningCheckInLogic, JoiningCheckInLogic>();

        }
    }
}
using CheckInModule.Interfaces;
using Places.Model.Entity;
using UserModule.Interfaces;

namespace CheckInModule.Services
{
    public class ClientCheckInService : IClientCheckInService
    {
        private readonly IUserAuthRepository _userAuthRepository;

        public ClientCheckInService(IUserAuthRepository userAuthRepository)
        {
            _userAuthRepository = userAuthRepository;
        }

        public AppUserEntity GetUser(long userId)
        {
            return _userAuthRepository.Get(x => x.Id == userId);
        }
    }
}
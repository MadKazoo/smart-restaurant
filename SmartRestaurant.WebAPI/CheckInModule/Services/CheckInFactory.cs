using System;
using System.Collections.Generic;
using Places.Model.Entity;

namespace CheckInModule.Services
{
    public static class CheckInFactory
    {
        public static CheckInEntity CreateNewCheckInEntity(PlaceEntity place, AppUserEntity user,
            PlaceTableEntity tableEntity, Guid groupId)
        {
            var checkIn = new CheckInEntity
            {
                Id = groupId,
                Table = tableEntity,
                Place = place,
                Status = CheckInStatus.NEW,
                PartialOrders = new List<PartialOrder>(),
                DateCreated = DateTime.UtcNow,
                UsersReadyForOrder = new List<UserCheckInStatus>
                {
                    new UserCheckInStatus
                    {
                        Status = UserCheckInStatusEnum.NEW, AppUserEntity = user, ToPay = 0
                    }
                }
            };

            return checkIn;
        }
    }
}
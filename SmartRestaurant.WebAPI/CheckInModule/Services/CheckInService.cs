using System;
using System.Collections.Generic;
using System.Linq;
using CheckInModule.BusinessLogic;
using CheckInModule.Interfaces;
using CheckInModule.Repository;
using Persistence;
using PlaceModule.Interfaces;
using Places.Model.Domain;
using Places.Model.Entity;
using Places.Model.Mapping;
using Shared.Interfaces;
using Shared.PartialOrder;

namespace CheckInModule.Services
{
    public class CheckInService : ICheckInService
    {
        private readonly IPartialOrderLogic _checkInPartialOrderLogic;
        private readonly SmartRestaurantDbContext _context;
        private readonly ICheckInRepository _checkInRepository;
        private readonly IClientCheckInService _clientCheckInService;
        private readonly IJoiningCheckInLogic _joiningCheckInLogic;
        private readonly IPlaceRepository _placeRepository;

        public CheckInService(SmartRestaurantDbContext context, ICheckInRepository checkInRepository, IClientCheckInService clientCheckInService
            , IJoiningCheckInLogic joiningCheckInLogic, IPlaceRepository placeRepository,
            IPartialOrderLogic checkInPartialOrderLogic)
        {
            _context = context;
            _checkInRepository = checkInRepository;
            _clientCheckInService = clientCheckInService;
            _joiningCheckInLogic = joiningCheckInLogic;
            _placeRepository = placeRepository;
            _checkInPartialOrderLogic = checkInPartialOrderLogic;
        }

        public void CheckIn(CheckInEntity checkIn, long userId)
        {
            var users = AddUserToList(userId, checkIn).ToList();
            _joiningCheckInLogic.JoinToExistingCheckIn(userId, users, checkIn);
            _checkInRepository.SaveChanges();
        }

        public Tuple<int, int> GetPlaceAndTableId(string tableHash)
        {
            var table = _context.PlaceTables.FirstOrDefault(x => x.Hash == tableHash);
            if (table.PlaceId == 0 || table.TableNumber == 0) throw new ArgumentException("Invalid QR code");
            return new Tuple<int, int>(table.PlaceId, table.TableNumber);
        }

        public CheckInEntity AddNewCheckIn(int placeId, long tableNumber, long userId, Guid groupId)
        {
            var user = _clientCheckInService.GetUser(userId);
            var place = _placeRepository.GetWithTables(placeId);

            if (place == null || !IsPlaceContainingTable(place, tableNumber))
            {
                throw new ArgumentNullException("Place does not exist or does not contain tables");
            }

            var table = place.Tables.First(x => x.TableNumber == tableNumber);
            var checkIn = _joiningCheckInLogic.AddNewCheckIn(place, table, user, groupId);

            SaveChanges(checkIn);
            return checkIn;
        }

        public CheckInEntity GetCurrentCheckInForTable(int tableNumber)
        {
            var checkIn = _checkInRepository
                .Get(x => x.Table.TableNumber == tableNumber && x.Status != CheckInStatus.CANCELED &&
                          x.Status != CheckInStatus.FINISHED);
            return checkIn;
        }

        public IEnumerable<CheckInDto> GetPlaceCheckIns(int placeId)
        {
          return  _checkInRepository.GetMany(x => x.Place.Id == placeId).Select(x=>x.MapToDto());
        }

        public CheckInEntity CheckOut(Guid checkInId, long userId)
        {
            var checkIn = _checkInRepository.GetById(checkInId);

            if (checkIn == null) return null;

            if (!checkIn.UsersReadyForOrder.Any())
            {
                ResetCheckIn(checkIn);
                return checkIn;
            }

            checkIn.UsersReadyForOrder = checkIn.UsersReadyForOrder.Where(x => x.AppUserEntity.Id != userId).ToList();

            foreach (var checkInPartialOrder in checkIn.PartialOrders.Where(x =>
                x.UserPartialOrders.Select(y => y.AppUserId).Contains(userId)))
            {
                _checkInPartialOrderLogic.RemoveUserFromPartialOrder(checkInPartialOrder, checkIn, userId);
            }

            if (!checkIn.UsersReadyForOrder.Any())
            {
                ResetCheckIn(checkIn);
            }

            _checkInRepository.SaveChanges();

            return checkIn;
        }

        public CheckInDto GetCurrentCheckInForUser(long userId)
        {
            var checkIn = _checkInRepository.Get(x => x.Status == CheckInStatus.NEW && x.UsersReadyForOrder
                                                          .Any(checkedInUser =>
                                                              checkedInUser.AppUserEntity.Id == userId));
            return GetCheckInDto(checkIn);
        }

        public CheckInDto GetCheckInDto(CheckInEntity checkIn)
        {
            var clients =
                checkIn?.UsersReadyForOrder.Select(x =>
                    x.AppUserEntity.MapToUserWithStatus(checkIn.UsersReadyForOrder));

            return checkIn?.MapToDto(clients);
        }

        public void CloseCheckIn(CheckInEntity checkIn)
        {
            checkIn.Status = CheckInStatus.FINISHED;
            _checkInRepository.SaveChanges();
        }

        private bool IsPlaceContainingTable(PlaceEntity place, long tableId)
        {
            return place.Tables.Any(x => x.TableNumber == tableId);
        }

        private void SaveChanges(CheckInEntity checkIn)
        {
            _checkInRepository.Add(checkIn);
            _checkInRepository.SaveChanges();
        }

        private IEnumerable<AppUserEntity> AddUserToList(long userId, CheckInEntity checkIn)
        {
            var user = _clientCheckInService.GetUser(userId);
            var checkedInUsers = checkIn.UsersReadyForOrder.ToList();
            checkedInUsers.Add(new UserCheckInStatus {AppUserEntity = user});
            return checkedInUsers.Select(x => x.AppUserEntity);
        }

        private void ResetCheckIn(CheckInEntity checkIn)
        {
            _checkInRepository.Delete(checkIn);
        }
    }
}
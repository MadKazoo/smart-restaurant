using System;
using System.Collections.Generic;
using System.Linq;
using CheckInModule.Interfaces;
using CheckInModule.Services;
using Places.Model.Entity;

namespace CheckInModule.BusinessLogic
{
    public class JoiningCheckInLogic : IJoiningCheckInLogic
    {
        public CheckInEntity AddNewCheckIn(PlaceEntity place, PlaceTableEntity tableEntity,
            AppUserEntity user, Guid groupId)
        {
            var checkIn = CheckInFactory.CreateNewCheckInEntity(place, user, tableEntity, groupId);

            return checkIn;
        }

        public void JoinToExistingCheckIn(long userId, IEnumerable<AppUserEntity> users, CheckInEntity checkIn)
        {
            if (checkIn.UsersReadyForOrder.Any(x => x.AppUserEntity.Id == userId)) return;

            var usersStatuses = checkIn.UsersReadyForOrder.ToList();
            var user = users.FirstOrDefault(x => x.Id == userId);

            usersStatuses.Add(new UserCheckInStatus
            {
                Status = UserCheckInStatusEnum.NEW, AppUserEntity = user, ToPay = 0
            });

            users.ToList().Add(user);

            var checkedInUsers = checkIn.UsersReadyForOrder.ToList();
            checkedInUsers.Add(new UserCheckInStatus {AppUserEntity = user});

            checkIn.UsersReadyForOrder = checkedInUsers;
            checkIn.UsersReadyForOrder = usersStatuses;
        }
    }
}
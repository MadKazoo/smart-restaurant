using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OrderModule.Interfaces;
using OrderModule.OrderLogic;
using OrderModule.Services;

namespace OrderModule.Helpers
{
    public static class StartupExtensions
    {
        public static void RegisterOrderModuleDependencies(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddTransient<IClientOrderService, ClientOrderService>();
            services.AddTransient<IOrderProceedingLogic, OrderProceedingLogic>();
            services.AddTransient<IRestaurantProceedingOrderService, RestaurantProceedingOrderService>();
            
        }
    }
}
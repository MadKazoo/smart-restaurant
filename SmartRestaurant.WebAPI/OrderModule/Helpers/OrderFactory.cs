using System;
using System.Collections.Generic;
using System.Linq;
using Places.Model.Entity;

namespace OrderModule.Helpers
{
    public static class OrderFactory
    {
        public static OrderEntity CreateOrderEntity(CheckInEntity checkInEntity)
        {
            var UserOrderStatuses = new List<UserOrderStatus>();
            foreach (var checkedInUser in checkInEntity.UsersReadyForOrder)
            {
                var userOrders = checkInEntity.PartialOrders.SelectMany(x =>
                    x.UserPartialOrders.Where(z => z.AppUserId == checkedInUser.AppUserEntity.Id));
                var status = CreateUserOrderStatus(checkedInUser, userOrders);
                UserOrderStatuses.Add(status);
            }

            var orderEntity = new OrderEntity
            {
                Place = checkInEntity.Place,
                Price = checkInEntity.GetAllMenuItemsPrice(),
                PartialOrders = checkInEntity.PartialOrders,
                GroupId = checkInEntity.Id,
                Table = checkInEntity.Table,
                DateCreated = DateTime.Now,
                UserOrderStatuses = UserOrderStatuses
            };

            return orderEntity;
        }

        private static UserOrderStatus CreateUserOrderStatus(UserCheckInStatus checkedInUser,
            IEnumerable<UserPartialOrder> userPartialOrders)
        {
            var userOrderStatus = new UserOrderStatus
            {
                UserId = checkedInUser.AppUserEntity.Id,
                HasPaid = false,
                ToPay = userPartialOrders.Sum(x => x.ToPayInMoney)
            };

            return userOrderStatus;
        }
    }
}
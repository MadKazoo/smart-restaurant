using System;
using System.Collections.Generic;
using System.Linq;
using CheckInModule.Interfaces;
using OrderModule.Helpers;
using OrderModule.Interfaces;
using OrderModule.OrderLogic;
using Places.Model.Domain;
using Places.Model.Entity;
using Places.Model.Mapping;
using Shared.Interfaces;

namespace OrderModule.Services
{
    public class ClientOrderService : IClientOrderService
    {
        private readonly ICheckInRepository _checkInRepository;
        private readonly IMenuItemRepository _menuItemRepository;
        private readonly IOrderProceedingLogic _orderProceedingLogic;
        private readonly IOrderRepository _orderRepository;
        private readonly IPartialOrderLogic _partialOrderLogic;

        public ClientOrderService(IOrderRepository orderRepository, ICheckInRepository checkInRepository,
            IMenuItemRepository menuItemRepository, IPartialOrderLogic partialOrderLogic,
            IOrderProceedingLogic orderProceedingLogic)
        {
            _orderRepository = orderRepository;
            _menuItemRepository = menuItemRepository;
            _checkInRepository = checkInRepository;
            _partialOrderLogic = partialOrderLogic;
            _orderProceedingLogic = orderProceedingLogic;
        }

        public OrderEntity GetOrder(int id)
        {
            return _orderRepository.GetOrdersWithFilter(x => x.Id == id).First();
        }

        public IEnumerable<OrderDto> GetCurrentlyProceededOrdersByUserId(int userId)
        {
            var orders =
                _orderRepository.GetOrdersWithFilter(_orderProceedingLogic.GetCurrentlyProceedOrdersForUser(userId));
            return orders?.ToList()
                .Select(x => x.EntitityToDomain());
        }


        public CheckInEntity AddToOrder(Guid checkInId, long userId, int menuItemId)
        {
            var menuItem = _menuItemRepository.GetById(menuItemId);
            var checkIn = _checkInRepository.GetById(checkInId);
            IEnumerable<UserCheckInStatus> users = checkIn.UsersReadyForOrder;
            var partialOrder = PartialOrderFactory.CreatePartialOrder(userId, menuItem, users);

            _partialOrderLogic.AddItemToPartialOrder(checkIn, partialOrder);

            _checkInRepository.SaveChanges();
            return checkIn;
        }

        public CheckInEntity AddUserToPartialOrder(Guid checkInId, long partialOrderId, long userId, decimal moneyToPay)
        {
            var checkIn = _checkInRepository.GetById(checkInId);

            var partialOrder = checkIn.PartialOrders
                .FirstOrDefault(x => x.Id.Equals(partialOrderId));

            if (partialOrder == null || IsPartialOrderContainingUser(partialOrder, userId)) return checkIn;

            _partialOrderLogic.AddUserToPartialOrder(partialOrder, userId, moneyToPay);

            _checkInRepository.SaveChanges();
            return checkIn;
        }


        public CheckInEntity SetUserPartialOrderPercentShare(Guid checkInId, long partialOrderId, long userId,
            decimal clientMoneyShare)
        {
            var checkIn = _checkInRepository.GetById(checkInId);
            var partialOrder = checkIn.PartialOrders
                .FirstOrDefault(x => x.Id.Equals(partialOrderId));

            var userToSetPercentShare = partialOrder?.UserPartialOrders.First(x => x.AppUserId == userId);

            if (partialOrder == null || userToSetPercentShare == null)
            {
                return checkIn;
            }
            
            _partialOrderLogic.SetUsersPercentShareInOrder(partialOrder, userId, clientMoneyShare);

            _checkInRepository.SaveChanges();
            return checkIn;
        }


        public CheckInEntity RemoveUserFromPartialOrder(Guid checkInId, long partialOrderId, long userId)
        {
            var checkIn = _checkInRepository.GetById(checkInId);
            var partialOrder = checkIn.PartialOrders
                .FirstOrDefault(x => x.Id.Equals(partialOrderId));

            if (partialOrder == null || partialOrder.UserPartialOrders.All(x => x.AppUserId != userId)) return checkIn;

            if (partialOrder.UserPartialOrders.Count() == 1)
                _partialOrderLogic.DeletePartialOrder(checkIn, partialOrder);
            else
                _partialOrderLogic.RemoveUserFromPartialOrder(partialOrder, userId);

            _checkInRepository.SaveChanges();
            return checkIn;
        }

        // Move in some check in service
        public CheckInEntity SetUserStatusToCheckIn(Guid checkInId, long clientId, bool isClientReady)
        {
            var checkIn = _checkInRepository.GetById(checkInId);
            var user = checkIn.UsersReadyForOrder.FirstOrDefault(x => x.AppUserEntity.Id == clientId);

            if (user == null) return null;

            _partialOrderLogic.SetCheckInPartialOrderStatus(checkIn, clientId, isClientReady);

            _checkInRepository.SaveChanges();
            return checkIn;
        }

        public bool HasAlreadyPaidForOrder(int userId, int orderId)
        {
            throw new NotImplementedException();
        }

        public OrderEntity CreateOrder(CheckInEntity checkInEntity)
        {
            var order = OrderFactory.CreateOrderEntity(checkInEntity);
            _orderRepository.Add(order);
            _orderRepository.SaveChanges();

            return order;
        }

        public string GetPaymentUrl(long orderId, long userId)
        {
            // TODO - do it with payment service
            return "www.google.com";
        }

        public bool ProceedToPayment(CheckInEntity checkInEntity)
        {
            // TODO - do it by external service
            return true;
        }

        public IEnumerable<OrderEntity> GetAll()
        {
            return _orderRepository.GetAll();
        }

        private bool IsPartialOrderContainingUser(PartialOrder partialOrder, long userId)
        {
            return partialOrder == null || partialOrder.UserPartialOrders.Any(x => x.AppUserId == userId);
        }
    }
}
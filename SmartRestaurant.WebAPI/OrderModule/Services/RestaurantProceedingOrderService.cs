using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using OrderModule.Interfaces;
using Places.Model;
using Places.Model.Domain;
using Places.Model.Entity;
using Places.Model.Mapping;
using Shared.Interfaces;

namespace OrderModule.Services
{
    public class RestaurantProceedingOrderService : IRestaurantProceedingOrderService
    {
        private readonly IOrderRepository _orderRepository;

        public RestaurantProceedingOrderService(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public OrderEntity SetOrderStatus(int orderId, OrderStatus status)
        {
            var order = GetOrder(orderId);
            order.OrderStatus = status;
            _orderRepository.SaveChanges();
            return order;
        }

        public IEnumerable<OrderDto> GetCurrentlyProceededOrders(long placeId)
        {
            var orders = _orderRepository
                .GetManyAsQueryable(x => x.Place.Id == placeId && x.OrderStatus != OrderStatus.FINISHED)
                .Include(z => z.Place)
                .Include(z => z.PartialOrders)
                .ThenInclude(z => z.UserPartialOrders)
                .Include(z => z.PartialOrders)
                .ThenInclude(z => z.MenuItem)
                .Include(z => z.Table)
                .Include(z => z.UserOrderStatuses)
                .Select(y => y.EntitityToDomain());
            return orders.ToList();
        }

        public Guid GetOrderGroupId(int orderId)
        {
            return _orderRepository.Get(x => x.Id == orderId).GroupId;
        }

        public OrderEntity GetOrder(int orderId)
        {
            return _orderRepository.GetManyAsQueryable(x => x.Id == orderId)
                .Include(z => z.Place)
                .Include(z => z.PartialOrders)
                .ThenInclude(z => z.UserPartialOrders)
                .Include(z => z.PartialOrders)
                .ThenInclude(z => z.MenuItem)
                .Include(z => z.UserOrderStatuses)
                .Include(z => z.Table)
                .First();
        }
    }
}
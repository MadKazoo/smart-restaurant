using System.Collections.Generic;
using System.Linq;
using Places.Model.Entity;

namespace OrderModule.OrderLogic
{
    public static class PartialOrderFactory
    {
        public static PartialOrder CreatePartialOrder(long userId, MenuItemEntity menuItem,
            IEnumerable<UserCheckInStatus> users)
        {
            var hundredPercentShare = 100;

            var userPartialOrder = CreateUserPartialOrder(userId, users, hundredPercentShare, menuItem.Price);

            var partialOrder = CreatePartialOrder(menuItem, userPartialOrder);
            return partialOrder;
        }

        private static PartialOrder CreatePartialOrder(MenuItemEntity menuItemEntity, UserPartialOrder userPartialOrder)
        {
            var partialOrder = new PartialOrder
            {
                MenuItem = menuItemEntity,
                UserPartialOrders = new List<UserPartialOrder> {userPartialOrder}
            };
            return partialOrder;
        }

        private static UserPartialOrder CreateUserPartialOrder(long userId, IEnumerable<UserCheckInStatus> users,
            decimal toPay, decimal menuItemPrice)
        {
            var userPartialOrder = new UserPartialOrder
            {
                AppUserId = userId, Name = users.First(x => x.AppUserEntity.Id == userId).AppUserEntity.Login
            };

            userPartialOrder.SetClientMenuItemPercentShare(menuItemPrice, toPay);

            return userPartialOrder;
        }
    }
}
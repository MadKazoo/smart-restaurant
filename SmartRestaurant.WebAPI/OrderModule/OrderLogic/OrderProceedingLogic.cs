using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using OrderModule.Interfaces;
using Places.Model;
using Places.Model.Entity;

namespace OrderModule.OrderLogic
{
    public class OrderProceedingLogic : IOrderProceedingLogic
    {
        public Expression<Func<OrderEntity, bool>> GetCurrentlyProceedOrdersForUser(long userId)
        {
            return x => x.UserOrderStatuses.Any(y => y.UserId == userId) &&
                        (x.OrderStatus == OrderStatus.NEW || x.OrderStatus == OrderStatus.ACCEPTED ||
                         x.OrderStatus == OrderStatus.PAID);
        }

        public IEnumerable<OrderEntity> GetCurrentlyProceededOrdersByPlaceId(IQueryable<OrderEntity> orders,
            int placeId)
        {
            return orders.Where(x =>
                (x.OrderStatus == OrderStatus.NEW || x.OrderStatus == OrderStatus.ACCEPTED ||
                 x.OrderStatus == OrderStatus.PAID) && x.Place.Id == placeId);
        }
    }
}
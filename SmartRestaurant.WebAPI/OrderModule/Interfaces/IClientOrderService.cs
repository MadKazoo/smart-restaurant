using System;
using System.Collections.Generic;
using Places.Model.Domain;
using Places.Model.Entity;

namespace OrderModule.Interfaces
{
    public interface IClientOrderService
    {
        OrderEntity GetOrder(int id);

        // void AddOrder(OrderModel orderModel);
        IEnumerable<OrderDto> GetCurrentlyProceededOrdersByUserId(int userId);
        CheckInEntity AddToOrder(Guid checkInId, long userId, int menuItemId);

        CheckInEntity AddUserToPartialOrder(Guid checkInId, long partialOrderId, long userId,
            decimal clientPercentShare);

        CheckInEntity SetUserPartialOrderPercentShare(Guid checkInId, long partialOrderId, long userId,
            decimal clientPercentShare);

        CheckInEntity RemoveUserFromPartialOrder(Guid checkInId, long partialOrderId, long userId);

        CheckInEntity SetUserStatusToCheckIn(Guid checkInId, long userId,
            bool isReadyToCheckOut);

        bool HasAlreadyPaidForOrder(int userId, int orderId);
        OrderEntity CreateOrder(CheckInEntity checkInEntity);
        string GetPaymentUrl(long orderId, long userId);
        bool ProceedToPayment(CheckInEntity checkInEntity);
    }
}
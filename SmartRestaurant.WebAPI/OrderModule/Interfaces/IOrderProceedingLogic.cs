using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Places.Model.Entity;

namespace OrderModule.Interfaces
{
    public interface IOrderProceedingLogic
    {
        Expression<Func<OrderEntity, bool>> GetCurrentlyProceedOrdersForUser(long userId);
        IEnumerable<OrderEntity> GetCurrentlyProceededOrdersByPlaceId(IQueryable<OrderEntity> orders, int placeId);
    }
}
using System;
using System.Collections.Generic;
using Places.Model;
using Places.Model.Domain;
using Places.Model.Entity;

namespace OrderModule.Interfaces
{
    public interface IRestaurantProceedingOrderService
    {
        IEnumerable<OrderDto> GetCurrentlyProceededOrders(long placeId);
        OrderEntity SetOrderStatus(int orderId, OrderStatus status);
        Guid GetOrderGroupId(int orderId);
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using CheckInModule.BusinessLogic;
using CheckInModule.Interfaces;
using CheckInModule.Repository;
using CheckInModule.Services;
using NSubstitute;
using NUnit.Framework;
using Persistence;
using PlaceModule;
using PlaceModule.Interfaces;
using Places.Model.Domain;
using Places.Model.Entity;
using Shared.Helpers;
using Shared.Interfaces;
using UserModule.Helpers;

namespace Places.UnitTest
{
    [TestFixture]
    public class CheckInServiceTests
    {
        [Test]
        public void AddNewCheckIn_CreatedCheckIn_ShouldHaveAssignedUsersAndPlace()
        {
           /* var userId = 1;
            var placeId = 1;
            var tableId = 1;
            var tableNumber = 1;
            
            var user = new AppUserEntity()
            {
                Id = userId,
                Login = "Test"
            };
            
            var place = new PlaceEntity(){
             Id   = placeId,
             Tables =  new List<PlaceTableEntity>()
             {
                 new PlaceTableEntity(){Id = tableId, TableNumber = tableNumber}
             }
            };
            
            var checkInPartialOrderLogic = Substitute.For<IPartialOrderLogic>();
            var clienCheckIntService = Substitute.For<IClientCheckInService>();
            var joiningCheckInLogic = new JoiningCheckInLogic();
            var placeRepository = Substitute.For<IPlaceRepository>();
            var checkInRepository = Substitute.For<ICheckInRepository>();
            clienCheckIntService.GetUser(userId).Returns(user);
            placeRepository.GetWithTables(placeId).Returns(place);
            ICheckInService checkInService = new CheckInService(checkInRepository, clienCheckIntService, 
                joiningCheckInLogic, placeRepository, checkInPartialOrderLogic);
            
            
            var checkIn = checkInService.AddNewCheckIn(placeId, tableId, userId, Guid.NewGuid());
            
            Assert.True(checkIn!=null);
            Assert.True(checkIn.Place == place);
            Assert.True(checkIn.UsersReadyForOrder.Select(x => x.AppUserEntity).Contains(user));
            */
        }
    }
}
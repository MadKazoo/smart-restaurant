using System.Collections.Generic;
using NUnit.Framework;
using OrderModule.Interfaces;
using OrderModule.OrderLogic;
using Places.Model;
using Places.Model.Entity;

namespace Places.UnitTest
{
    [TestFixture]
    public class CurrentlyProceedOrdersTests
    {
        private readonly IOrderProceedingLogic _orderProceedingLogic;

        public CurrentlyProceedOrdersTests()
        {
            _orderProceedingLogic = new OrderProceedingLogic();
        }

        [Test]
        public void FindOneOrderForUser()
        {
            var givenUser = new AppUserEntity {Id = 1};
            var userPartialOrder = new UserPartialOrder
            {
                Id = 1, AppUserId = givenUser.Id
            };
            var partialOrder = new PartialOrder
            {
                Id = 1, UserPartialOrders = new List<UserPartialOrder> {userPartialOrder}
            };

            var entities = new List<OrderEntity>
            {
                new OrderEntity
                {
                    PartialOrders = new List<PartialOrder> {partialOrder}
                }
            };

            var orderEntity = new OrderEntity
            {
                OrderStatus = OrderStatus.PAID,
                PartialOrders = new List<PartialOrder> {partialOrder}
            };

            var orders = new List<OrderEntity> {orderEntity};

            var ord = _orderProceedingLogic.GetCurrentlyProceedOrdersForUser(givenUser.Id);

            // Assert.True(ord.Count() == 1);
        }
    }
}
using System.Linq;
using Microsoft.EntityFrameworkCore;
using NetTopologySuite.Geometries;
using NUnit.Framework;
using Persistence;
using PlaceModule.Services;
using Places.Model.Entity;

namespace Places.UnitTest
{
    [TestFixture]
    public class PlacesServiceTests
    {
        private SmartRestaurantDbContext _context;

        public PlacesServiceTests()
        {
            var options = new DbContextOptionsBuilder<SmartRestaurantDbContext>()
                .UseInMemoryDatabase(databaseName: "Products Test")
                .Options;
            _context = new SmartRestaurantDbContext(options);

            var place = new PlaceEntity()
            {
                Location = new Point( 21.007813, 52.221434) // PW
                {
                    SRID = 4326
                }
            };

            _context.Places.Add(place);
                _context.SaveChanges();
        }

        [TestCase( 20.988382, 52.211257, 10000, true)]
        [TestCase( 20.988382, 52.211257, 5000, false)]
        [TestCase( 20.59190, 52.12405, 50, false)]
        [TestCase( 21.06029, 52.14337,5000, true)]
        [TestCase( 21.06029, 52.14337,50000, true)]


        public void GetNearbyPlaces_PlacesAreWithinExpectedMetersDistance(double longitude, double latitude, double distance, bool isNearby) {

            var point = new Point(longitude, latitude)
            {
                SRID = 4326
            };
            
                var service = new ClientPlaceService(_context);
                var nearbyPlaces = service.GetNearbyPlaces(lat:latitude, lon:longitude, distance);
                Assert.AreEqual(nearbyPlaces.Any(), isNearby);
                
            }

        [OneTimeTearDown]
        public void Cleanup()
        {
            _context.Dispose();
        }


        }
    }
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Places.Model.Entity;
using Shared.Helpers;
using Shared.Interfaces;

namespace Places.UnitTest
{
    [TestFixture]
    public class BillSplittingLogicTest
    {
        private readonly IBillSplittingLogic _billSplittingLogic;

        public BillSplittingLogicTest()
        {
            _billSplittingLogic = new BillSplittingLogic();
        }

        public class UserOrder : UserPartialOrder
        {
        }

        [TestCase(50, 50, 50, 50)]
        [TestCase(50, 50, 40, 60)]
        [TestCase(75, 25, 20, 80)]
        [TestCase(90, 10, 80, 20)]
        [TestCase(80, 20, 3, 97)]
        public void When_User_Changes_Percent_Share_In_Order_It_Should_Be_Splitted(decimal firstclientValue,
            decimal secondClientValue, decimal changedTo, decimal output)
        {
            var userPartialOrderList = new List<UserPartialOrder>();
            var firstUserPartialOrder = new UserOrder
            {
                Id = 1, AppUserId = 1
            };
            var itemPrice = 100;

            firstUserPartialOrder.SetClientMenuItemPercentShare(itemPrice, firstclientValue);
            var secondUserPartialOrder = new UserOrder
            {
                Id = 2, AppUserId = 2
            };
            secondUserPartialOrder.SetClientMenuItemPercentShare(itemPrice, secondClientValue);


            userPartialOrderList.Add(firstUserPartialOrder);
            userPartialOrderList.Add(secondUserPartialOrder);
        
            _billSplittingLogic.SetUserPercentShareInOrder(userPartialOrderList, secondUserPartialOrder, changedTo, itemPrice);

            var firstClientPercentShare = userPartialOrderList.First();


            Assert.AreEqual(firstClientPercentShare.ClientPercentShare, output);
        }
    }
}